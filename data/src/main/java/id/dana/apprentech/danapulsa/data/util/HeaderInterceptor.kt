package id.dana.apprentech.danapulsa.data.util

import android.util.Log
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 04/06/20
 */

class HeaderInterceptor @Inject
constructor(private val preferencesManager: PreferencesManager) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        if (request.header("No-Auth") == null) {
            val cookie = preferencesManager.getUserSession()

            if (!cookie.isNullOrEmpty()) {
                request = request.newBuilder()
                    .addHeader("Cookie", cookie)
                    .build()
            }
        }
        return chain.proceed(request)
    }
}