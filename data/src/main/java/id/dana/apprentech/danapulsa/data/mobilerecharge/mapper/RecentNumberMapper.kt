package id.dana.apprentech.danapulsa.data.mobilerecharge.mapper

import id.dana.apprentech.danapulsa.data.mobilerecharge.entity.RecentNumberEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Provider
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.RecentNumber
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

class RecentNumberMapper {
    fun transform(response: Response<BaseResponse<List<RecentNumberEntity>>>): DataResult<BaseResponse<List<RecentNumber>>> {
        return if (response.isSuccessful) {
            with(response.body()!!) { handleApiSuccess(code, transformEntity(data), message) }
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(entity: List<RecentNumberEntity>): List<RecentNumber> {
        val recentNumbers = mutableListOf<RecentNumber>()
        entity.forEach {
            recentNumbers.add(
                RecentNumber(
                    it.number,
                    Provider(it.provider.providerId, it.provider.name, it.provider.image),
                    it.date
                )
            )
        }
        return recentNumbers
    }
}