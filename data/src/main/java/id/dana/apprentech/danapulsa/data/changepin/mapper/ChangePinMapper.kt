package id.dana.apprentech.danapulsa.data.changepin.mapper

import id.dana.apprentech.danapulsa.data.changepin.entity.OtpEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.changepin.model.Otp
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

class ChangePinMapper {
    fun transformOtpEntity(
        response: Response<BaseResponse<OtpEntity>>
    ): DataResult<BaseResponse<Otp>> {
        return if (response.isSuccessful) {
            with(response.body()!!) { handleApiSuccess(code, transformEntity(data), message) }
        } else {
            handleApiError(response)
        }
    }

    fun transformNonBody(
        response: Response<NonBodyResponse>
    ): DataResult<NonBodyResponse> {
        return if (response.isSuccessful){
            with(response.body()!!){ handleApiSuccess(code, message)}
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(otpEntity: OtpEntity): Otp {
        return Otp(
            otpEntity.id,
            otpEntity.userId,
            otpEntity.code
        )
    }
}