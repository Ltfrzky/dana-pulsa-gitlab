package id.dana.apprentech.danapulsa.data.profile

import id.dana.apprentech.danapulsa.data.login.mapper.UserProfileMapper
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.data.profile.mapper.BalanceMapper
import id.dana.apprentech.danapulsa.data.profile.mapper.LogoutMapper
import id.dana.apprentech.danapulsa.data.profile.network.ProfileApi
import id.dana.apprentech.danapulsa.data.util.PreferencesKey
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.profile.model.Balance
import id.dana.apprentech.danapulsa.domain.profile.repository.ProfileRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

class ProfileRepositoryImpl @Inject constructor(
    private val profileApi: ProfileApi,
    private val preferencesManager: PreferencesManager
) : ProfileRepository {

    override fun getUserProfile(): Single<DataResult<BaseResponse<UserProfile>>> {
        return profileApi.getUserProfile().map { UserProfileMapper().transform(it) }
            .doAfterSuccess {
                when (it) {
                    is DataResult.Success -> preferencesManager.saveUserProfile(it.successData.data)
                }
            }
    }

    override fun getUserBalance(): Single<DataResult<BaseResponse<Int>>> {
        return profileApi.getUserBalance()
            .map { BalanceMapper().transform(it) }
            .doAfterSuccess {
                when (it) {
                    is DataResult.Success -> preferencesManager.saveUserBalance(it.successData.data)
                }
            }
    }

    override fun getUserProfileLocal(): Single<UserProfile> {
        return Single.just(preferencesManager.getUserProfile())
    }

    override fun loggingOut(): Single<DataResult<NonBodyResponse>> {
        return profileApi.loggingOut().map { LogoutMapper().transform(it) }
            .doAfterSuccess{
                when (it) {
                    is DataResult.Success -> preferencesManager.clearPreferences()
                }
            }
    }
}