package id.dana.apprentech.danapulsa.data.mobilerecharge.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

data class CatalogEntity(
    @SerializedName("id")
    val catalogId: Int,

    @SerializedName("value")
    val value: Int,

    @SerializedName("price")
    val price: Int
)