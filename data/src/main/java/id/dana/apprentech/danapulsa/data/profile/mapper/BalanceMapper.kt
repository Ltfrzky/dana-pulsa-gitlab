package id.dana.apprentech.danapulsa.data.profile.mapper

import id.dana.apprentech.danapulsa.data.profile.entity.BalanceEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.profile.model.Balance
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

class BalanceMapper {
    fun transform(response: Response<BaseResponse<String>>): DataResult<BaseResponse<Int>> {
        return if (response.isSuccessful) {
            with(response.body()!!) { handleApiSuccess(code, data.toInt(), message) }
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(entity: BalanceEntity): Balance {
        return Balance(entity.balance)
    }
}