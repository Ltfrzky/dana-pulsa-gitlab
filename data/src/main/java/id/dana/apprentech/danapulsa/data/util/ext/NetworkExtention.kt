package id.dana.apprentech.danapulsa.data.util.ext

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.data.util.ApiErrorUtil
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

fun isOnline(context: Context): Boolean {
    var result = false
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        connectivityManager?.run {
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.run {
                result = when {
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_VPN) -> true
                    else -> false
                }
            }
        }
    } else {
        connectivityManager?.run {
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
            return activeNetwork?.isConnected == true
        }
    }
    return result
}

fun <T : Any> handleApiError(response: Response<T>): DataResult.Error {
    val error = ApiErrorUtil.parseError(response)
    if(error.code.isEmpty()){
        return DataResult.Error(Exception(error.error), error.status.toInt())
    }
    return DataResult.Error(Exception(error.message), error.code.toInt())
}

fun <T: Any> handleApiSuccess(code: Int, data: T, message: String) : DataResult.Success<BaseResponse<T>>{
    return DataResult.Success(BaseResponse(code, data, message))
}

fun handleApiSuccess(code: Int, message: String) : DataResult.Success<NonBodyResponse>{
    return DataResult.Success(NonBodyResponse(code, message))
}

fun handleSubscriberError(message: String?) : DataResult.Error{
    return DataResult.Error(Exception(message))
}

fun noNetworkConnectivityError(): DataResult.Error {
    return DataResult.Error(Exception("Connection Error"))
}