package id.dana.apprentech.danapulsa.data.register.entity

import com.google.gson.annotations.SerializedName

data class UserProfileEntity(
    @SerializedName("id")
    val id: String?,

    @SerializedName("name")
    val name: String?,

    @SerializedName("email")
    val email: String?,

    @SerializedName("username")
    val phone: String?
) {
}