package id.dana.apprentech.danapulsa.data.login.mapper

import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

class VerifyPinMapper {
    fun transform(response: Response<BaseResponse<Empty>>): DataResult<BaseResponse<Empty>> {
        return if (response.isSuccessful) {
            with(response.body()!!) { handleApiSuccess(code, Empty(), message) }
        } else {
            handleApiError(response)
        }
    }
}