package id.dana.apprentech.danapulsa.data.sample

import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.data.sample.mapper.SampleMapper
import id.dana.apprentech.danapulsa.data.sample.network.SampleApi
import id.dana.apprentech.danapulsa.data.sample.network.request.LoginRequestSample
import id.dana.apprentech.danapulsa.data.util.PreferencesKey
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.sample.repository.SampleRepository
import id.dana.apprentech.danapulsa.domain.sample.model.UserSample
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable
import retrofit2.Response
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 04/06/20
 */

class SampleRepositoryImpl @Inject constructor(
    private val sampleApi: SampleApi,
    private val preferencesManager: PreferencesManager
) : SampleRepository {

    override fun loginSample(
        username: String,
        password: String
    ): Flowable<DataResult<BaseResponse<UserSample>>> {
        return sampleApi.login(LoginRequestSample(username, password))
            .map { SampleMapper().transform(it) }
            .doOnNext {
                when (it) {
                    is DataResult.Success -> saveUserData(it.successData.data)
                }
            }
    }

    override fun getUserData(): Flowable<UserSample> {
        return Flowable.just(userDataPreferences())
    }

    private fun saveUserData(user: UserSample) {
        preferencesManager.saveString(PreferencesKey.KEY_USER_NAME, user.name)
        preferencesManager.saveString(PreferencesKey.KEY_USER_EMAIL, user.email)
        preferencesManager.saveString(PreferencesKey.KEY_USER_PHONE, user.phone)
        preferencesManager.saveFloat(PreferencesKey.KEY_USER_BALANCE, user.balance)
        preferencesManager.saveString(PreferencesKey.KEY_USER_TOKEN, user.token)
    }

    private fun userDataPreferences(): UserSample {
        return UserSample(
            preferencesManager.getString(PreferencesKey.KEY_USER_NAME),
            preferencesManager.getString(PreferencesKey.KEY_USER_EMAIL),
            preferencesManager.getString(PreferencesKey.KEY_USER_EMAIL),
            preferencesManager.getFloat(PreferencesKey.KEY_USER_BALANCE),
            preferencesManager.getString(PreferencesKey.KEY_USER_EMAIL)
        )

    }
}