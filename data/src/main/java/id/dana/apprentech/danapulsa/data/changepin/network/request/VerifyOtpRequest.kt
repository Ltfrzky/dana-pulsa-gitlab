package id.dana.apprentech.danapulsa.data.changepin.network.request

import com.google.gson.annotations.SerializedName

data class VerifyOtpRequest(
    @SerializedName("id")
    val userId: Int,

    @SerializedName("code")
    val code: Int
) {
}