package id.dana.apprentech.danapulsa.data.sample.mapper

import id.dana.apprentech.danapulsa.data.sample.entity.UserEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.sample.model.UserSample
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

class SampleMapper {
    fun transform(
        response: Response<BaseResponse<UserEntity>>
    ): DataResult<BaseResponse<UserSample>> {
        return if (response.isSuccessful) {
            val user = transformEntity(response.body()!!.data)
            DataResult.Success(BaseResponse(response.code(), user, response.message()))
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(userEntity: UserEntity): UserSample {
        return UserSample(
            userEntity.name,
            userEntity.email,
            userEntity.phone,
            userEntity.balance,
            userEntity.token
        )
    }
}