package id.dana.apprentech.danapulsa.data.mobilerecharge.mapper

import id.dana.apprentech.danapulsa.data.mobilerecharge.entity.OrderEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.CatalogOrder
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Order
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Provider
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

class OrderMapper {
    fun transform(response: Response<BaseResponse<OrderEntity>>): DataResult<BaseResponse<Order>> {
        return if (response.isSuccessful) {
            with(response.body()!!){ handleApiSuccess(code, transformEntity(data), message) }
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(entity: OrderEntity): Order {
        return Order(
            entity.orderId,
            entity.phoneNumber,
            CatalogOrder(
                entity.catalog.catalogId,
                entity.catalog.value,
                entity.catalog.price,
                Provider(
                    entity.catalog.provider.providerId,
                    entity.catalog.provider.name,
                    entity.catalog.provider.image
                )
            )
        )
    }
}