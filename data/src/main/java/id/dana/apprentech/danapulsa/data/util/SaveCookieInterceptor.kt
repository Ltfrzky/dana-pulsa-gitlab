package id.dana.apprentech.danapulsa.data.util

import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

class SaveCookieInterceptor @Inject
constructor(private val preferencesManager: PreferencesManager) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

        if(response.isSuccessful){
            if(response.headers("Set-Cookie").isNotEmpty()){
                val cookie = response.headers["Set-Cookie"]
                cookie?.let { preferencesManager.saveUserSession(it) }
            }
        }
        return response
    }
}