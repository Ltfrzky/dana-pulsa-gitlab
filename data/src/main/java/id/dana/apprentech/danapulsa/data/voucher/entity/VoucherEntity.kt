package id.dana.apprentech.danapulsa.data.voucher.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

data class VoucherEntity(
    @SerializedName("id")
    val voucherId: Int,

    @SerializedName("name")
    val name: String,

    @SerializedName("voucherTypeName")
    val voucherTypeName: String,

    @SerializedName("discount")
    val discount: Int,

    @SerializedName("value")
    val value: Int,

    @SerializedName("maxDeduction")
    val maxDeduction: Int,

    @SerializedName("minPurchase")
    val minPurchase: Int,

    @SerializedName("filePath")
    val filePath: String,

    @SerializedName("expiryDate")
    val expiryDate: String,

    @SerializedName("paymentMethodName")
    val paymentMethod: String?
)