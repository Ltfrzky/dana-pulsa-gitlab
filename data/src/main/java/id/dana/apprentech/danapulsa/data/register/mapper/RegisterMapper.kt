package id.dana.apprentech.danapulsa.data.register.mapper

import id.dana.apprentech.danapulsa.data.register.entity.UserProfileEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.register.model.User
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

class RegisterMapper {
    fun transform(
        response: Response<NonBodyResponse>
    ): DataResult<NonBodyResponse> {
        return if (response.isSuccessful) {
            with(response.body()!!) {
                handleApiSuccess(this.code, message)
            }
        } else {
            handleApiError(response)
        }
    }
}