package id.dana.apprentech.danapulsa.data.history

import id.dana.apprentech.danapulsa.data.history.mapper.HistoryMapper
import id.dana.apprentech.danapulsa.data.history.network.HistoryApi
import id.dana.apprentech.danapulsa.data.mobilerecharge.mapper.TransactionMapper
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.history.model.History
import id.dana.apprentech.danapulsa.domain.history.repository.HistoryRepository
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 18/06/20
 */

class HistoryRepositoryImpl @Inject constructor(
    private val historyApi: HistoryApi
) : HistoryRepository {

    override fun getHistoryInProgress(page: Int): Flowable<DataResult<BaseResponse<List<History>>>> {
        return historyApi.getHistoryInProgress(page)
            .map { HistoryMapper().transform(it) }
    }

    override fun getHistoryCompleted(page: Int): Flowable<DataResult<BaseResponse<List<History>>>> {
        return historyApi.getHistoryCompleted(page)
            .map { HistoryMapper().transform(it) }
    }

    override fun getHistoryDetail(transactionId: String): Single<DataResult<BaseResponse<Transaction>>> {
        return historyApi.getHistoryDetail(transactionId)
            .map { TransactionMapper().transform(it) }
    }
}