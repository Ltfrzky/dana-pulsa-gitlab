package id.dana.apprentech.danapulsa.data.mobilerecharge.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

data class ProviderEntity(
    @SerializedName("id")
    val providerId: Int,

    @SerializedName("name")
    val name: String,

    @SerializedName("image")
    val image: String
)