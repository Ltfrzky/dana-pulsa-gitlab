package id.dana.apprentech.danapulsa.data.changepin

import id.dana.apprentech.danapulsa.data.changepin.mapper.ChangePinMapper
import id.dana.apprentech.danapulsa.data.changepin.network.ChangePinApi
import id.dana.apprentech.danapulsa.data.changepin.network.request.IdRequest
import id.dana.apprentech.danapulsa.data.changepin.network.request.NewPinRequest
import id.dana.apprentech.danapulsa.data.changepin.network.request.VerifyOtpRequest
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.changepin.repository.ChangePinRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ChangePinRepositoryImpl @Inject constructor(
    private val changePinApi: ChangePinApi,
    private val preferencesManager: PreferencesManager
) : ChangePinRepository {

    override fun getChangePinOtp(): Single<DataResult<NonBodyResponse>> {
        return changePinApi.getChangePinOtp()
            .map { ChangePinMapper().transformNonBody(it) }
    }

    override fun getForgotPinOtp(userId: Int): Single<DataResult<NonBodyResponse>> {
        return changePinApi.getForgotPinOtp(IdRequest(userId))
            .map { ChangePinMapper().transformNonBody(it) }
    }

    override fun verifyOtp(userId: Int, code: Int): Single<DataResult<NonBodyResponse>> {
        return changePinApi.verifyOtp(VerifyOtpRequest(userId, code))
            .doOnSuccess {response ->
                if (response.isSuccessful){
                    if (preferencesManager.getUserSession().isNullOrEmpty()) {
                        response.headers()["Set-Cookie"]
                            ?.let { session -> preferencesManager.saveUserSession(session) }
                    }
                }
            }
            .map { ChangePinMapper().transformNonBody(it) }
    }

    override fun resendOtp(userId: String): Single<DataResult<NonBodyResponse>> {
        return changePinApi.getOtp(userId)
            .map { ChangePinMapper().transformNonBody(it) }
    }

    override fun changePin(userPin: Int): Single<DataResult<NonBodyResponse>> {
        return changePinApi.changePin(NewPinRequest(userPin))
            .map { ChangePinMapper().transformNonBody(it) }
    }
}