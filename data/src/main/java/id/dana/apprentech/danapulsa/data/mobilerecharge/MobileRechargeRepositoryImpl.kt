package id.dana.apprentech.danapulsa.data.mobilerecharge

import id.dana.apprentech.danapulsa.data.mobilerecharge.mapper.*
import id.dana.apprentech.danapulsa.data.mobilerecharge.network.MobileRechargeApi
import id.dana.apprentech.danapulsa.data.mobilerecharge.network.request.OrderPulsaRequest
import id.dana.apprentech.danapulsa.data.mobilerecharge.network.request.PayOrderRequest
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.*
import id.dana.apprentech.danapulsa.domain.mobilerecharge.repository.MobileRechargeRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

class MobileRechargeRepositoryImpl @Inject constructor(
    private val mobileRechargeApi: MobileRechargeApi,
    private val preferencesManager: PreferencesManager
) : MobileRechargeRepository {

    override fun getRecentNumbers(): Flowable<DataResult<BaseResponse<List<RecentNumber>>>> {
        return mobileRechargeApi.getRecentNumbers()
            .map { RecentNumberMapper().transform(it) }
    }

    override fun getProviderCatalog(phoneNumber: String): Flowable<DataResult<BaseResponse<ProviderCatalog>>> {
        return mobileRechargeApi.getCatalog(phoneNumber)
            .map { ProviderCatalogMapper().transform(it) }
    }

    override fun orderPulsa(
        phoneNumber: String,
        catalogId: String
    ): Single<DataResult<BaseResponse<Order>>> {
        return mobileRechargeApi.orderPulsa(OrderPulsaRequest(phoneNumber, catalogId))
            .map { OrderMapper().transform(it) }
    }

    override fun payOrder(
        transactionId: String,
        methodId: String,
        voucherId: String
    ): Single<DataResult<BaseResponse<Payment>>> {
        return mobileRechargeApi.payOrder(PayOrderRequest(transactionId, methodId, voucherId))
            .map { PaymentMapper().transform(it) }
            .doAfterSuccess{
                when(it){
                    is DataResult.Success -> preferencesManager.saveUserBalance(it.successData.data.balance)
                }
            }
    }

    override fun cancelOrder(transactionId: String): Single<DataResult<BaseResponse<Transaction>>> {
        return mobileRechargeApi.cancelOrder(transactionId)
            .map { TransactionMapper().transform(it) }
    }

    override fun getVoucherRecommendation(transactionId: String): Flowable<DataResult<BaseResponse<List<Voucher>>>> {
        return mobileRechargeApi.getVoucherRecommendation(transactionId)
            .map { VoucherMapper().transform(it) }
    }

    override fun getOrderDetail(transactionId: String): Single<DataResult<BaseResponse<Order>>> {
        return mobileRechargeApi.getTransactionDetail(transactionId)
            .map { OrderMapper().transform(it) }
    }
}