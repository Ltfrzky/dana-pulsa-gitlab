package id.dana.apprentech.danapulsa.data.login.entity

import com.google.gson.annotations.SerializedName

data class UserProfileEntity(
    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String,

    @SerializedName("email")
    val email: String,

    @SerializedName("username")
    val phone: String
)