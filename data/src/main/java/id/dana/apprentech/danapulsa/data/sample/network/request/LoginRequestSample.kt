package id.dana.apprentech.danapulsa.data.sample.network.request

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

data class LoginRequestSample(
    @SerializedName("emailOrPhone")
    var username: String,

    @SerializedName("password")
    var password: String
)