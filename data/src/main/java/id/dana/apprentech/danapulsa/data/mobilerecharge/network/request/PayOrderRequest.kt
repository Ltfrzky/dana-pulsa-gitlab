package id.dana.apprentech.danapulsa.data.mobilerecharge.network.request

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

data class PayOrderRequest(
    @SerializedName("transactionId")
    val transactionId: String,

    @SerializedName("methodId")
    val methodId: String,

    @SerializedName("voucherId")
    val voucherId: String
)