package id.dana.apprentech.danapulsa.data.login.mapper

import id.dana.apprentech.danapulsa.data.login.entity.UserProfileEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

class UserProfileMapper {
    fun transform(
        response: Response<BaseResponse<UserProfileEntity>>
    ): DataResult<BaseResponse<UserProfile>>{
        return if (response.isSuccessful){
            with(response.body()!!){ handleApiSuccess(code, transformEntity(data), message) }
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(userProfileEntity: UserProfileEntity): UserProfile{
        return UserProfile(
            userProfileEntity.id,
            userProfileEntity.name,
            userProfileEntity.email,
            userProfileEntity.phone
        )
    }
}