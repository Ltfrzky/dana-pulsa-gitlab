package id.dana.apprentech.danapulsa.data.preference

import android.content.Context
import android.content.SharedPreferences
import id.dana.apprentech.danapulsa.data.util.PreferencesKey
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.profile.model.Balance
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 04/06/20
 */

class PreferencesManagerImpl @Inject constructor(context: Context) : PreferencesManager {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(PreferencesKey.KEY_PREFERENCE_NAME, Context.MODE_PRIVATE)

    override fun getBoolean(key: String, default: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, default)
    }

    override fun saveBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    override fun getInt(key: String, default: Int): Int {
        return sharedPreferences.getInt(key, default)
    }

    override fun saveInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }

    override fun getString(key: String, default: String): String {
        return sharedPreferences.getString(key, default)!!
    }

    override fun saveString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    override fun getLong(key: String, default: Long): Long {
        return sharedPreferences.getLong(key, default)
    }

    override fun saveLong(key: String, value: Long) {
        sharedPreferences.edit().putLong(key, value).apply()
    }

    override fun getFloat(key: String, default: Float): Float {
        return sharedPreferences.getFloat(key, default)
    }

    override fun saveFloat(key: String, value: Float) {
        sharedPreferences.edit().putFloat(key, value).apply()
    }

    override fun saveUserProfile(userProfile: UserProfile) {
        sharedPreferences.edit()
            .putInt(PreferencesKey.KEY_USER_ID, userProfile.id)
            .putString(PreferencesKey.KEY_USER_NAME, userProfile.name)
            .putString(PreferencesKey.KEY_USER_EMAIL, userProfile.email)
            .putString(PreferencesKey.KEY_USER_PHONE, userProfile.phone)
            .apply()
    }

    override fun getUserProfile(): UserProfile {
        return UserProfile(
            sharedPreferences.getInt(PreferencesKey.KEY_USER_ID, 0),
            sharedPreferences.getString(PreferencesKey.KEY_USER_NAME, ""),
            sharedPreferences.getString(PreferencesKey.KEY_USER_EMAIL, ""),
            sharedPreferences.getString(PreferencesKey.KEY_USER_PHONE, "")
        )
    }

    override fun saveUserSession(session: String) {
        sharedPreferences.edit().putString(PreferencesKey.KEY_USER_SESSION, session).apply()
    }

    override fun getUserSession(): String? {
        return sharedPreferences.getString(PreferencesKey.KEY_USER_SESSION, "")
    }

    override fun saveUserBalance(balance: Int) {
        sharedPreferences.edit().putInt(PreferencesKey.KEY_USER_BALANCE, balance).apply()
    }

    override fun getUserBalance(): Int {
        return sharedPreferences.getInt(PreferencesKey.KEY_USER_BALANCE, 0)
    }

    override fun clearPreferences() {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }
}