package id.dana.apprentech.danapulsa.data.sample.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

data class UserEntity(
    @SerializedName("name")
    var name: String,
    @SerializedName("email")
    var email: String,
    @SerializedName("phone")
    var phone: String,
    @SerializedName("balance")
    var balance: Float,
    @SerializedName("token")
    var token: String
)