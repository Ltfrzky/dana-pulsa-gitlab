package id.dana.apprentech.danapulsa.data.mobilerecharge.mapper

import id.dana.apprentech.danapulsa.data.mobilerecharge.entity.ProviderCatalogEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Catalog
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Provider
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.ProviderCatalog
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

class ProviderCatalogMapper {
    fun transform(response: Response<BaseResponse<ProviderCatalogEntity>>): DataResult<BaseResponse<ProviderCatalog>> {
        return if (response.isSuccessful) {
            with(response.body()!!) { handleApiSuccess(code, transformEntity(data), message) }
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(entity: ProviderCatalogEntity): ProviderCatalog {
        val catalogs = mutableListOf<Catalog>()
        entity.catalog.forEach {
            catalogs.add(Catalog(it.catalogId, it.value, it.price))
        }

        return ProviderCatalog(
            Provider(
                entity.provider.providerId,
                entity.provider.name,
                entity.provider.image
            ), catalogs
        )
    }
}