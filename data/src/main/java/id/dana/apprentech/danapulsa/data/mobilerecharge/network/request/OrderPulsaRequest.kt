package id.dana.apprentech.danapulsa.data.mobilerecharge.network.request

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

data class OrderPulsaRequest(
    @SerializedName("phoneNumber")
    val phoneNumber: String,

    @SerializedName("catalogId")
    val catalogId: String
)