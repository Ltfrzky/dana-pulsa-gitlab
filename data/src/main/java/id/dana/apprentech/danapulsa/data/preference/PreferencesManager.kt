package id.dana.apprentech.danapulsa.data.preference

import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.profile.model.Balance

/**
 *created by Lutfi Rizky Ramadhan on 04/06/20
 */

interface PreferencesManager {
    fun getBoolean(key: String, default: Boolean = false): Boolean
    fun saveBoolean(key: String, value: Boolean)

    fun getInt(key: String, default: Int = 0): Int
    fun saveInt(key: String, value: Int)

    fun getString(key: String, default: String = ""): String
    fun saveString(key: String, value: String)

    fun getLong(key: String, default: Long = 0): Long
    fun saveLong(key: String, value: Long)

    fun getFloat(key: String, default: Float = 0f): Float
    fun saveFloat(key: String, value: Float)

    fun saveUserProfile(userProfile: UserProfile)
    fun getUserProfile() : UserProfile

    fun saveUserSession(session: String)
    fun getUserSession() : String?

    fun saveUserBalance(balance: Int)
    fun getUserBalance() : Int

    fun clearPreferences()
}