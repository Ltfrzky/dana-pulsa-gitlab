package id.dana.apprentech.danapulsa.data.voucher.entity

import com.google.gson.annotations.SerializedName

data class PromotionEntity (
    @SerializedName("id")
    val id: Int,

    @SerializedName("name")
    val name: String,

    @SerializedName("voucherTypeName")
    val voucherTypeName: String,

    @SerializedName("filePath")
    val filePath: String,

    @SerializedName("expiryDate")
    val expiryDate: String
)