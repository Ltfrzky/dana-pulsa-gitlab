package id.dana.apprentech.danapulsa.data.voucher.network

import id.dana.apprentech.danapulsa.data.voucher.entity.PromotionEntity
import id.dana.apprentech.danapulsa.data.voucher.entity.VoucherEntity
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface VoucherApi {
    @GET("vouchers/promotion/{page}")
    fun getPromotion(@Path("page") page: String): Flowable<Response<BaseResponse<List<PromotionEntity>>>>

    @GET("vouchers/details/{voucherId}")
    fun getVoucherDetail(@Path("voucherId") voucherId: String): Single<Response<BaseResponse<VoucherEntity>>>
}