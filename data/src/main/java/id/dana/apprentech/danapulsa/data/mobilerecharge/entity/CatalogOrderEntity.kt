package id.dana.apprentech.danapulsa.data.mobilerecharge.entity

import com.google.gson.annotations.SerializedName
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Provider

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

data class CatalogOrderEntity(
    @SerializedName("id")
    val catalogId: Int,

    @SerializedName("provider")
    val provider: ProviderEntity,

    @SerializedName("value")
    val value: Int,

    @SerializedName("price")
    val price: Int
)