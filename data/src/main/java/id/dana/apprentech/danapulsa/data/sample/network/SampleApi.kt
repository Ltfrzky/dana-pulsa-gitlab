package id.dana.apprentech.danapulsa.data.sample.network

import id.dana.apprentech.danapulsa.data.sample.entity.UserEntity
import id.dana.apprentech.danapulsa.data.sample.network.request.LoginRequestSample
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import io.reactivex.rxjava3.core.Flowable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 *created by Lutfi Rizky Ramadhan on 04/06/20
 */

interface SampleApi {
    @POST("api/login")
    @Headers("No-Auth: true")
    fun login(@Body loginRequest: LoginRequestSample) : Flowable<Response<BaseResponse<UserEntity>>>
}