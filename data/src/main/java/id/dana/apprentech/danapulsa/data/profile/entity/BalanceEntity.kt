package id.dana.apprentech.danapulsa.data.profile.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

data class BalanceEntity(
    @SerializedName("balance")
    val balance: Int
)