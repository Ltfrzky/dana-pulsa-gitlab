package id.dana.apprentech.danapulsa.data.changepin.network

import id.dana.apprentech.danapulsa.data.changepin.network.request.IdRequest
import id.dana.apprentech.danapulsa.data.changepin.network.request.NewPinRequest
import id.dana.apprentech.danapulsa.data.changepin.network.request.VerifyOtpRequest
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.*

interface ChangePinApi {
    @POST("forgotpin-otp")
    @Headers("No-Auth: true")
    fun getForgotPinOtp(@Body idRequest: IdRequest): Single<Response<NonBodyResponse>>

    @POST("changepin-otp")
    fun getChangePinOtp(): Single<Response<NonBodyResponse>>

    @GET("otp/{id}")
    @Headers("No-Auth: true")
    fun getOtp(@Path("id") id: String): Single<Response<NonBodyResponse>>

    @POST("verify-otp")
    @Headers("No-Auth: true")
    fun verifyOtp(@Body verifyOtpRequest: VerifyOtpRequest): Single<Response<NonBodyResponse>>

    @PUT("change-pin")
    fun changePin(@Body newPinRequest: NewPinRequest): Single<Response<NonBodyResponse>>
}