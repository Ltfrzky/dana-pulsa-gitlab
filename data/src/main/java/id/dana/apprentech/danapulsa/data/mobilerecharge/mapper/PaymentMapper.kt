package id.dana.apprentech.danapulsa.data.mobilerecharge.mapper

import id.dana.apprentech.danapulsa.data.mobilerecharge.entity.PaymentEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.*
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class PaymentMapper {
    fun transform(response: Response<BaseResponse<PaymentEntity>>): DataResult<BaseResponse<Payment>> {
        return if (response.isSuccessful) {
            with(response.body()!!) { handleApiSuccess(code, transformEntity(data), message) }
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(entity: PaymentEntity): Payment {
        val catalog = entity.transaction.catalog.let {
            CatalogOrder(
                it.catalogId,
                it.value,
                it.price,
                Provider(it.provider.providerId, it.provider.name, it.provider.image)
            )
        }
        val voucher = entity.transaction.voucher?.let {
            VoucherTransaction(
                it.voucherId,
                it.name,
                it.deduction,
                it.maxDeduction
            )
        }
        val transaction =
            entity.transaction.let {
                Transaction(
                    it.transactionId,
                    it.method,
                    it.phoneNumber,
                    catalog,
                    voucher, it.status, it.createdAt, it.updatedAt
                )
            }
        return Payment(entity.balance, entity.rewardVoucher, transaction)
    }
}