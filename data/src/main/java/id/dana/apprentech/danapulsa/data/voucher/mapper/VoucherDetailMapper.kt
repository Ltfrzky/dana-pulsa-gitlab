package id.dana.apprentech.danapulsa.data.voucher.mapper

import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.data.voucher.entity.VoucherEntity
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import retrofit2.Response

class VoucherDetailMapper {
    fun transform(response: Response<BaseResponse<VoucherEntity>>): DataResult<BaseResponse<Voucher>> {
        return if (response.isSuccessful) {
            with(response.body()!!){ handleApiSuccess(code, transformEntity(data), message)}
        } else {
            handleApiError(response)
        }
    }

    fun transformEntity(entity: VoucherEntity): Voucher {
        return Voucher(
            entity.voucherId,
            entity.name,
            entity.voucherTypeName,
            entity.discount,
            entity.value,
            entity.maxDeduction,
            entity.minPurchase,
            entity.filePath,
            entity.expiryDate,
            entity.paymentMethod
        )
    }
}