package id.dana.apprentech.danapulsa.data.voucher.mapper

import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.data.voucher.entity.PromotionEntity
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Promotion
import retrofit2.Response

class PromotionMapper {
    fun transform(
        response: Response<BaseResponse<List<PromotionEntity>>>
    ): DataResult<BaseResponse<List<Promotion>>> {
        return if (response.isSuccessful) {
            with(response.body()!!) { handleApiSuccess(code, transformEntity(data), message) }
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(entity: List<PromotionEntity>): List<Promotion> {
        val promotions = mutableListOf<Promotion>()
        entity.forEach {
            promotions.add(
                Promotion(
                    it.id,
                    it.name,
                    it.voucherTypeName,
                    it.filePath,
                    it.expiryDate
                )
            )
        }
        return promotions
    }
}