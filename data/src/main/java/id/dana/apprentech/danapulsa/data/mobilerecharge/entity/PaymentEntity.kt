package id.dana.apprentech.danapulsa.data.mobilerecharge.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

data class PaymentEntity(
    @SerializedName("balance")
    val balance: Int,

    @SerializedName("rewardVoucher")
    val rewardVoucher: Boolean,

    @SerializedName("transaction")
    val transaction: TransactionEntity
)