package id.dana.apprentech.danapulsa.data.mobilerecharge.mapper

import id.dana.apprentech.danapulsa.data.mobilerecharge.entity.TransactionEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.*
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class TransactionMapper {
    fun transform(response: Response<BaseResponse<TransactionEntity>>): DataResult<BaseResponse<Transaction>> {
        return if (response.isSuccessful) {
            with(response.body()!!) { handleApiSuccess(code, transformEntity(data), message) }
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(entity: TransactionEntity): Transaction {
        val catalog =
            entity.catalog.let {
                CatalogOrder(
                    it.catalogId, it.value, it.price,
                    Provider(
                        it.provider.providerId,
                        it.provider.name,
                        it.provider.image
                    )
                )
            }
        val voucher = entity.voucher?.let { VoucherTransaction(it.voucherId, it.name, it.deduction, it.maxDeduction) }
        return Transaction(
            entity.transactionId,
            entity.method,
            entity.phoneNumber,
            catalog,
            voucher,
            entity.status,
            entity.createdAt,
            entity.updatedAt
        )
    }
}