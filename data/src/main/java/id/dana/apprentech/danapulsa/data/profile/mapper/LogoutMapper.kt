package id.dana.apprentech.danapulsa.data.profile.mapper

import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

class LogoutMapper {
    fun transform(
        response: Response<NonBodyResponse>
    ): DataResult<NonBodyResponse>{
        return if (response.isSuccessful){
            with(response.body()!!){
                handleApiSuccess(code, message)
            }
        }else{
            handleApiError(response)
        }
    }
}