package id.dana.apprentech.danapulsa.data.util

import android.util.Log
import com.google.gson.GsonBuilder
import retrofit2.Response
import java.io.IOException

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

object ApiErrorUtil {

    fun parseError(response: Response<*>): ApiError {

        val gson = GsonBuilder().create()
        val error: ApiError

        try {
            error = gson.fromJson(response.errorBody()?.string(), ApiError::class.java)
        } catch (e: IOException) {
            return ApiError()
        }
        return error
    }
}

data class ApiError(val code: String, val message: String, val status: String, val error: String) {
    constructor() : this("", "", "", "")
}