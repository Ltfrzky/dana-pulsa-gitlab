package id.dana.apprentech.danapulsa.data.mobilerecharge.entity

import com.google.gson.annotations.SerializedName
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.CatalogOrder

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

data class OrderEntity(
    @SerializedName("id")
    val orderId: Int,

    @SerializedName("phoneNumber")
    val phoneNumber: String,

    @SerializedName("catalog")
    val catalog: CatalogOrderEntity
)