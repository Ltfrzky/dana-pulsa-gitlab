package id.dana.apprentech.danapulsa.data.register

import id.dana.apprentech.danapulsa.data.register.mapper.RegisterMapper
import id.dana.apprentech.danapulsa.data.register.network.RegisterApi
import id.dana.apprentech.danapulsa.data.register.network.request.RegisterRequest
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.register.model.User
import id.dana.apprentech.danapulsa.domain.register.repository.RegisterRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class RegisterRepositoryImpl @Inject constructor(
    private val registerApi: RegisterApi
):RegisterRepository {

    override fun registerUser(
        name: String,
        email: String,
        phone: String,
        pin: Int
    ): Single<DataResult<NonBodyResponse>> {
        return registerApi.register(RegisterRequest(name, email, phone, pin))
            .map { RegisterMapper().transform(it) }
    }
}