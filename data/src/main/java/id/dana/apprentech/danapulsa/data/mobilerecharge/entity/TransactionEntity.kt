package id.dana.apprentech.danapulsa.data.mobilerecharge.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

data class TransactionEntity(
    @SerializedName("id")
    val transactionId: Int,

    @SerializedName("method")
    val method: String,

    @SerializedName("phoneNumber")
    val phoneNumber: String,

    @SerializedName("catalog")
    val catalog: CatalogOrderEntity,

    @SerializedName("voucher")
    val voucher: VoucherTransactionEntity?,

    @SerializedName("status")
    val status: String,

    @SerializedName("createdAt")
    val createdAt: String,

    @SerializedName("updatedAt")
    val updatedAt: String
)