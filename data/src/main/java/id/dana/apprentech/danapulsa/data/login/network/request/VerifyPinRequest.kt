package id.dana.apprentech.danapulsa.data.login.network.request

import com.google.gson.annotations.SerializedName

data class VerifyPinRequest(
    @SerializedName("id")
    val id: Int,

    @SerializedName("pin")
    val pin: Int
)