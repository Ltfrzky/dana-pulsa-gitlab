package id.dana.apprentech.danapulsa.data.mobilerecharge.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

data class VoucherTransactionEntity(
    @SerializedName("id")
    val voucherId: Int,

    @SerializedName("name")
    val name: String,

    @SerializedName("deduction")
    val deduction: Int,

    @SerializedName("maxDeduction")
    val maxDeduction: Int
)