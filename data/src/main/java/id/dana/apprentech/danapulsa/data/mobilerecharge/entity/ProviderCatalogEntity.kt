package id.dana.apprentech.danapulsa.data.mobilerecharge.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

data class ProviderCatalogEntity(
    @SerializedName("provider")
    val provider: ProviderEntity,

    @SerializedName("catalog")
    val catalog: List<CatalogEntity>
)