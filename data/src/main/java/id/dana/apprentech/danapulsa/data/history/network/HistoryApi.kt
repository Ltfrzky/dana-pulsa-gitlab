package id.dana.apprentech.danapulsa.data.history.network

import id.dana.apprentech.danapulsa.data.history.entity.HistoryEntity
import id.dana.apprentech.danapulsa.data.mobilerecharge.entity.TransactionEntity
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 *created by Lutfi Rizky Ramadhan on 18/06/20
 */

interface HistoryApi {
    @GET("transaction/in-progress/{page}")
    fun getHistoryInProgress(@Path("page") page: Int) : Flowable<Response<BaseResponse<List<HistoryEntity>>>>

    @GET("transaction/completed/{page}")
    fun getHistoryCompleted(@Path("page") page: Int) : Flowable<Response<BaseResponse<List<HistoryEntity>>>>

    @GET("transaction/details/{transactionId}")
    fun getHistoryDetail(@Path("transactionId") transactionId: String) : Single<Response<BaseResponse<TransactionEntity>>>
}