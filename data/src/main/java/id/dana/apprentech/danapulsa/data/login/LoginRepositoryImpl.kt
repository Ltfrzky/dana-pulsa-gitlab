package id.dana.apprentech.danapulsa.data.login

import id.dana.apprentech.danapulsa.data.login.mapper.UserProfileMapper
import id.dana.apprentech.danapulsa.data.login.mapper.VerifyPinMapper
import id.dana.apprentech.danapulsa.data.login.network.LoginApi
import id.dana.apprentech.danapulsa.data.login.network.request.VerifyPinRequest
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.data.util.PreferencesKey
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.login.repository.LoginRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class LoginRepositoryImpl @Inject constructor(
    private val loginApi: LoginApi,
    private val preferencesManager: PreferencesManager
) : LoginRepository {

    override fun login(username: String): Single<DataResult<BaseResponse<UserProfile>>> {
        return loginApi.login(username)
            .map { UserProfileMapper().transform(it) }
            .doOnSuccess {
                when (it) {
                    is DataResult.Success -> preferencesManager.saveUserProfile(it.successData.data)
                }
            }
//            .doOnNext {
//                when (it) {
//                    is DataResult.Success -> preferencesManager.saveUserProfile(it.successData.data)
//                }
//            }
    }

    override fun verifyPin(
        userId: Int,
        userPin: Int
    ): Single<DataResult<BaseResponse<Empty>>> {
        return loginApi.verifyPin(VerifyPinRequest(userId, userPin))
            .doOnSuccess {
                if (it.isSuccessful){
                    it.headers()["Set-Cookie"]
                        ?.let {session->
                            preferencesManager.saveUserSession(session)
                        }
                }
            }
            .map { VerifyPinMapper().transform(it) }
    }

    override fun getUserProfile(userId: Int): Single<UserProfile> {
        return Single.just(preferencesManager.getUserProfile())
    }

}