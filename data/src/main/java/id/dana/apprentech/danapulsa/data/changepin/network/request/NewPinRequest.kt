package id.dana.apprentech.danapulsa.data.changepin.network.request

import com.google.gson.annotations.SerializedName

data class NewPinRequest(
    @SerializedName("pin")
    val pin: Int
) {
}