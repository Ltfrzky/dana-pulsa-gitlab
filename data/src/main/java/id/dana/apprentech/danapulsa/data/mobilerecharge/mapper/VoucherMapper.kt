package id.dana.apprentech.danapulsa.data.mobilerecharge.mapper

import id.dana.apprentech.danapulsa.data.voucher.entity.VoucherEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class VoucherMapper {
    fun transform(response: Response<BaseResponse<List<VoucherEntity>>>): DataResult<BaseResponse<List<Voucher>>> {
        return if (response.isSuccessful) {
            with(response.body()!!) { handleApiSuccess(code, transformEntity(data), message) }
        } else {
            handleApiError(response)
        }
    }

    private fun transformEntity(entity: List<VoucherEntity>): List<Voucher> {
        val vouchers = mutableListOf<Voucher>()
        entity.forEach {
            vouchers.add(
                Voucher(
                    it.voucherId,
                    it.name,
                    it.voucherTypeName,
                    it.discount,
                    it.value,
                    it.maxDeduction,
                    it.minPurchase,
                    it.filePath,
                    it.expiryDate,
                    it.paymentMethod
                )
            )
        }
        return vouchers
    }
}