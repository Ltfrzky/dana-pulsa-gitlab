package id.dana.apprentech.danapulsa.data.changepin.entity

import com.google.gson.annotations.SerializedName

data class OtpEntity(
    @SerializedName("id")
    val id: Int,

    @SerializedName("userId")
    val userId: Int,

    @SerializedName("code")
    val code: Int
) {
}