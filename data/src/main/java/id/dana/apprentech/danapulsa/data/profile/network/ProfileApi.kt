package id.dana.apprentech.danapulsa.data.profile.network

import id.dana.apprentech.danapulsa.data.login.entity.UserProfileEntity
import id.dana.apprentech.danapulsa.data.profile.entity.BalanceEntity
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.DELETE
import retrofit2.http.GET

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface ProfileApi {
    @GET("profile")
    fun getUserProfile() : Single<Response<BaseResponse<UserProfileEntity>>>

    @GET("balance")
    fun getUserBalance() : Single<Response<BaseResponse<String>>>

    @DELETE("logout")
    fun loggingOut() : Single<Response<NonBodyResponse>>
}