package id.dana.apprentech.danapulsa.data.register.network

import id.dana.apprentech.danapulsa.data.register.entity.UserProfileEntity
import id.dana.apprentech.danapulsa.data.register.network.request.RegisterRequest
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface RegisterApi {
    @POST("register")
    @Headers("No-Auth: true")
    fun register(@Body registerRequest: RegisterRequest): Single<Response<NonBodyResponse>>
}