package id.dana.apprentech.danapulsa.data.util

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

object PreferencesKey {
    const val KEY_PREFERENCE_NAME = "DANA_PULSA_PREFERENCES"

    const val KEY_USER_NAME = "USER_NAME"

    const val KEY_USER_EMAIL = "USER_EMAIL"

    const val KEY_USER_PHONE = "USER_PHONE"

    const val KEY_USER_BALANCE = "USER_BALANCE"

    const val KEY_USER_TOKEN = "USER_TOKEN"

    const val KEY_USER_SESSION = "USER_SESSION"

    const val KEY_USER_ID = "USER_ID"
}