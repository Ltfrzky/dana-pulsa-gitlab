package id.dana.apprentech.danapulsa.data.history.entity

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 18/06/20
 */

data class HistoryEntity(
    @SerializedName("id")
    val id: Int,

    @SerializedName("phoneNumber")
    val phoneNumber: String,

    @SerializedName("price")
    val price: Int,

    @SerializedName("voucher")
    val voucher: Int,

    @SerializedName("status")
    val status: String,

    @SerializedName("createdAt")
    val createdAt: String
)