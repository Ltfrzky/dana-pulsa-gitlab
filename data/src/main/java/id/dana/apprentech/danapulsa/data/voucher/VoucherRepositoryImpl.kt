package id.dana.apprentech.danapulsa.data.voucher

import id.dana.apprentech.danapulsa.data.mobilerecharge.mapper.VoucherMapper
import id.dana.apprentech.danapulsa.data.voucher.mapper.PromotionMapper
import id.dana.apprentech.danapulsa.data.voucher.mapper.VoucherDetailMapper
import id.dana.apprentech.danapulsa.data.voucher.network.VoucherApi
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Promotion
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import id.dana.apprentech.danapulsa.domain.voucher.repository.VoucherRepository
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class VoucherRepositoryImpl @Inject constructor(
    private val voucherApi: VoucherApi
): VoucherRepository {
    override fun getPromotion(page: String): Flowable<DataResult<BaseResponse<List<Promotion>>>> {
        return voucherApi.getPromotion(page)
            .map { PromotionMapper().transform(it) }
    }

    override fun getVoucherDetail(voucherId: String): Single<DataResult<BaseResponse<Voucher>>> {
        return voucherApi.getVoucherDetail(voucherId)
            .map { VoucherDetailMapper().transform(it) }
    }
}