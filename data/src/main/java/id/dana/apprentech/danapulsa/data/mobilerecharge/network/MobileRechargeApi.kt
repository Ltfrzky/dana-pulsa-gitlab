package id.dana.apprentech.danapulsa.data.mobilerecharge.network

import id.dana.apprentech.danapulsa.data.mobilerecharge.entity.*
import id.dana.apprentech.danapulsa.data.mobilerecharge.network.request.OrderPulsaRequest
import id.dana.apprentech.danapulsa.data.mobilerecharge.network.request.PayOrderRequest
import id.dana.apprentech.danapulsa.data.voucher.entity.VoucherEntity
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.*

/**
 *created by Lutfi Rizky Ramadhan on 14/06/20
 */

interface MobileRechargeApi {
    @GET("recent-number")
    fun getRecentNumbers(): Flowable<Response<BaseResponse<List<RecentNumberEntity>>>>

    @GET("catalog/{phone}")
    fun getCatalog(@Path("phone") phone: String): Flowable<Response<BaseResponse<ProviderCatalogEntity>>>

    @POST("order")
    fun orderPulsa(@Body request: OrderPulsaRequest): Single<Response<BaseResponse<OrderEntity>>>

    @POST("pay")
    fun payOrder(@Body request: PayOrderRequest): Single<Response<BaseResponse<PaymentEntity>>>

    @DELETE("transaction/cancel/{id}")
    fun cancelOrder(@Path("id") id: String): Single<Response<BaseResponse<TransactionEntity>>>

    @GET("vouchers/recommendation/{transactionId}")
    fun getVoucherRecommendation(@Path("transactionId") transactionId: String): Flowable<Response<BaseResponse<List<VoucherEntity>>>>

    @GET("transaction/details/{transactionId}")
    fun getTransactionDetail(@Path("transactionId") transactionId: String): Single<Response<BaseResponse<OrderEntity>>>
}