package id.dana.apprentech.danapulsa.data.login.network

import id.dana.apprentech.danapulsa.data.login.entity.UserProfileEntity
import id.dana.apprentech.danapulsa.data.login.network.request.VerifyPinRequest
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.Empty
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.*

interface LoginApi {
    @GET("login/{phone}")
    @Headers("No-Auth: true")
    fun login(@Path("phone") phone: String): Single<Response<BaseResponse<UserProfileEntity>>>

    @POST("verifypin-login")
    @Headers("No-Auth: true")
    fun verifyPin(@Body verifyPinRequest: VerifyPinRequest): Single<Response<BaseResponse<Empty>>>
}