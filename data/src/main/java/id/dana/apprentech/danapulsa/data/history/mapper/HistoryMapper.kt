package id.dana.apprentech.danapulsa.data.history.mapper

import id.dana.apprentech.danapulsa.data.history.entity.HistoryEntity
import id.dana.apprentech.danapulsa.data.util.ext.handleApiError
import id.dana.apprentech.danapulsa.data.util.ext.handleApiSuccess
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.history.model.History
import id.dana.apprentech.danapulsa.domain.util.DataResult
import retrofit2.Response

/**
 *created by Lutfi Rizky Ramadhan on 18/06/20
 */

class HistoryMapper {
    fun transform(response: Response<BaseResponse<List<HistoryEntity>>>): DataResult<BaseResponse<List<History>>> {
        return if(response.isSuccessful){
            with(response.body()!!) { handleApiSuccess(code, transformEntity(data), message)}
        }else{
            handleApiError(response)
        }
    }

    private fun transformEntity(entity: List<HistoryEntity>): List<History> {
        val histories = mutableListOf<History>()
        entity.forEach {
            histories.add(
                History(
                    it.id,
                    it.phoneNumber,
                    it.price,
                    it.voucher,
                    it.status,
                    it.createdAt
                )
            )
        }
        return histories
    }
}