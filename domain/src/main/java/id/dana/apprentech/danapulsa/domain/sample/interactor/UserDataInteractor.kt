package id.dana.apprentech.danapulsa.domain.sample.interactor

import id.dana.apprentech.danapulsa.domain.UseCase
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.sample.model.UserSample
import id.dana.apprentech.danapulsa.domain.sample.repository.SampleRepository
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

class UserDataInteractor @Inject constructor(
    private val sampleRepository: SampleRepository,
    compositeDisposable: CompositeDisposable
) : UseCase<UserSample, Empty>(compositeDisposable) {

    override fun buildUseCaseFlowable(request: Empty): Flowable<UserSample> {
        return sampleRepository.getUserData()
    }
}