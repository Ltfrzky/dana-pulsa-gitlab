package id.dana.apprentech.danapulsa.domain.login.interactors

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.login.repository.LoginRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

class Login @Inject constructor(
    private val repository: LoginRepository,
    compositeDisposable: CompositeDisposable
) : SingleUseCase<DataResult<BaseResponse<UserProfile>>,Login.Request>(compositeDisposable) {

    override fun buildUseCaseSingle(request: Request): Single<DataResult<BaseResponse<UserProfile>>> {
        return repository.login(request.username)
    }

    data class Request(val username: String)
}