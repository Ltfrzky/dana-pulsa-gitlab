package id.dana.apprentech.danapulsa.domain

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleObserver
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subscribers.ResourceSubscriber

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

abstract class UseCase<T, Request>(private val compositeDisposable: CompositeDisposable) {

    protected abstract fun buildUseCaseFlowable(request: Request): Flowable<T>

    fun executeFlowable(subscriber: ResourceSubscriber<T>, request: Request): CompositeDisposable {
        return addSubscription(
            executeAsFlowable(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(subscriber)
        )
    }

    private fun executeAsFlowable(request: Request): Flowable<T> = buildUseCaseFlowable(request)

    private fun addSubscription(subscription: ResourceSubscriber<T>): CompositeDisposable {
        if (compositeDisposable.size() > 0) {
            compositeDisposable.remove(subscription)
        }

        compositeDisposable.add(subscription)
        return compositeDisposable
    }

    fun clearSubscription() {
        compositeDisposable.clear()
    }
}