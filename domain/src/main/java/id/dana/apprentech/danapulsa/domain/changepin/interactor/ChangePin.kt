package id.dana.apprentech.danapulsa.domain.changepin.interactor

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.changepin.repository.ChangePinRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class ChangePin @Inject constructor(
    private val repository: ChangePinRepository,
    compositeDisposable: CompositeDisposable
) : SingleUseCase<DataResult<NonBodyResponse>, ChangePin.Request>(compositeDisposable){

    override fun buildUseCaseSingle(request: Request): Single<DataResult<NonBodyResponse>> {
        return repository.changePin(request.userPin)
    }

    inner class Request (val userPin: Int)
}