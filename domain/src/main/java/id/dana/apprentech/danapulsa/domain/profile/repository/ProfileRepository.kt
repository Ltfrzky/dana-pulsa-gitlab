package id.dana.apprentech.danapulsa.domain.profile.repository

import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface ProfileRepository {
    fun getUserProfile() : Single<DataResult<BaseResponse<UserProfile>>>

    fun getUserBalance() : Single<DataResult<BaseResponse<Int>>>

    fun getUserProfileLocal() : Single<UserProfile>

    fun loggingOut(): Single<DataResult<NonBodyResponse>>
}