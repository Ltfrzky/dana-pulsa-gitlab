package id.dana.apprentech.danapulsa.domain.sample.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PromotionSample(
    val id: Int? = null,
    val imageLink: String? = null,
    val promotionTitle: String? = null,
    val promotionInfo: String? = null,
    val promotionType: String? = null
): Parcelable