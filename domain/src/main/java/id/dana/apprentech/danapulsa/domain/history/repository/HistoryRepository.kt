package id.dana.apprentech.danapulsa.domain.history.repository

import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.history.model.History
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single

/**
 *created by Lutfi Rizky Ramadhan on 13/06/20
 */

interface HistoryRepository {
    fun getHistoryInProgress(page: Int) : Flowable<DataResult<BaseResponse<List<History>>>>

    fun getHistoryCompleted(page: Int) : Flowable<DataResult<BaseResponse<List<History>>>>

    fun getHistoryDetail(transactionId: String) : Single<DataResult<BaseResponse<Transaction>>>
}