package id.dana.apprentech.danapulsa.domain.sample.model

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

data class UserSample(
    var name: String,

    var email: String,

    var phone: String,

    var balance: Float,

    var token: String
)