package id.dana.apprentech.danapulsa.domain.mobilerecharge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *created by Lutfi Rizky Ramadhan on 13/06/20
 */
@Parcelize
data class Transaction(
    val transactionId: Int,
    val method: String,
    val phoneNumber: String,
    val catalog: CatalogOrder,
    val voucher: VoucherTransaction?,
    val status: String,
    val createdAt: String,
    val updatedAt: String
): Parcelable