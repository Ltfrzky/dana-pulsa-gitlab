package id.dana.apprentech.danapulsa.domain.profile.interactors

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.profile.repository.ProfileRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class DeleteUserSession @Inject constructor(
    private val repository: ProfileRepository,
    compositeDisposable: CompositeDisposable
): SingleUseCase<DataResult<NonBodyResponse>, Empty>(compositeDisposable){

    override fun buildUseCaseSingle(request: Empty): Single<DataResult<NonBodyResponse>> {
        return repository.loggingOut()
    }
}