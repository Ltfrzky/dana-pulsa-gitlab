package id.dana.apprentech.danapulsa.domain.base

/**
 *created by Lutfi Rizky Ramadhan on 11/06/20
 * Used for interactor that doesn't need any request params or response that doesn't have data (only code and message)
 * e.g get user data from preference, verify pin
 */

data class Empty(val empty: String = "empty")