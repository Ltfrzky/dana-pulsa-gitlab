package id.dana.apprentech.danapulsa.domain.login.repository

import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single

interface LoginRepository {
    fun login(username: String): Single<DataResult<BaseResponse<UserProfile>>>

    fun verifyPin(userId: Int, userPin: Int): Single<DataResult<BaseResponse<Empty>>>

    fun getUserProfile(userId: Int): Single<UserProfile>
}