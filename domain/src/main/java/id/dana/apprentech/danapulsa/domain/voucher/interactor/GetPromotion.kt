package id.dana.apprentech.danapulsa.domain.voucher.interactor

import id.dana.apprentech.danapulsa.domain.UseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Promotion
import id.dana.apprentech.danapulsa.domain.voucher.repository.VoucherRepository
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class GetPromotion @Inject constructor(
    private val repository: VoucherRepository,
    compositeDisposable: CompositeDisposable
) : UseCase<DataResult<BaseResponse<List<Promotion>>>, GetPromotion.Request>(compositeDisposable) {

    override fun buildUseCaseFlowable(request: Request): Flowable<DataResult<BaseResponse<List<Promotion>>>> {
        return repository.getPromotion(request.page)
    }

    inner class Request(val page: String)
}