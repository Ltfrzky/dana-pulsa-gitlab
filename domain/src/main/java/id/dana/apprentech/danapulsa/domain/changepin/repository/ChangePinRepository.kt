package id.dana.apprentech.danapulsa.domain.changepin.repository

import android.provider.ContactsContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.changepin.model.Otp
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single

interface ChangePinRepository {
    fun getChangePinOtp(): Single<DataResult<NonBodyResponse>>

    fun getForgotPinOtp(userId: Int): Single<DataResult<NonBodyResponse>>

    fun verifyOtp(userId: Int, code: Int): Single<DataResult<NonBodyResponse>>

    fun resendOtp(userId: String): Single<DataResult<NonBodyResponse>>

    fun changePin(userPin: Int): Single<DataResult<NonBodyResponse>>

}