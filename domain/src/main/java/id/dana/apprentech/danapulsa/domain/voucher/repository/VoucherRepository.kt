package id.dana.apprentech.danapulsa.domain.voucher.repository

import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.voucher.model.Promotion
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single

interface VoucherRepository {
    fun getPromotion(page: String): Flowable<DataResult<BaseResponse<List<Promotion>>>>

    fun getVoucherDetail(voucherId: String): Single<DataResult<BaseResponse<Voucher>>>
}