package id.dana.apprentech.danapulsa.domain.mobilerecharge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */
@Parcelize
data class CatalogOrder(
    val catalogId: Int,
    val value: Int,
    val price: Int,
    val provider: Provider
) : Parcelable