package id.dana.apprentech.danapulsa.domain.history.interactor

import id.dana.apprentech.danapulsa.domain.UseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.history.model.History
import id.dana.apprentech.danapulsa.domain.history.repository.HistoryRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 18/06/20
 */

class GetHistoryCompleted @Inject constructor(
    private val repository: HistoryRepository,
    compositeDisposable: CompositeDisposable
) : UseCase<DataResult<BaseResponse<List<History>>>, GetHistoryCompleted.Request>(
    compositeDisposable
) {

    override fun buildUseCaseFlowable(request: Request): Flowable<DataResult<BaseResponse<List<History>>>> {
        return repository.getHistoryCompleted(request.page)
    }

    data class Request(val page: Int)
}