package id.dana.apprentech.danapulsa.domain.voucher.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Promotion (
    val id: Int,
    val name: String,
    val voucherTypeName: String,
    val filePath: String,
    val expiryDate: String
): Parcelable