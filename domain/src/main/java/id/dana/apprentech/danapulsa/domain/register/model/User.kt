package id.dana.apprentech.danapulsa.domain.register.model

data class User(
    val id: String?,
    val name: String?,
    val email: String?,
    val phone: String?
) {
}