package id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor

import id.dana.apprentech.danapulsa.domain.UseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.RecentNumber
import id.dana.apprentech.danapulsa.domain.mobilerecharge.repository.MobileRechargeRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class GetRecentNumbers @Inject
constructor(
    private val repository: MobileRechargeRepository,
    compositeDisposable: CompositeDisposable
) : UseCase<DataResult<BaseResponse<List<RecentNumber>>>,Empty>(compositeDisposable) {
    override fun buildUseCaseFlowable(request: Empty): Flowable<DataResult<BaseResponse<List<RecentNumber>>>> {
        return repository.getRecentNumbers()
    }
}