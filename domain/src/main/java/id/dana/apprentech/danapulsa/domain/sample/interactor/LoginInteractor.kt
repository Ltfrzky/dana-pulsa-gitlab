package id.dana.apprentech.danapulsa.domain.sample.interactor

import id.dana.apprentech.danapulsa.domain.UseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.sample.model.UserSample
import id.dana.apprentech.danapulsa.domain.sample.repository.SampleRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

class LoginInteractor @Inject constructor(
    private val repository: SampleRepository,
    compositeDisposable: CompositeDisposable
) : UseCase<DataResult<BaseResponse<UserSample>>, LoginInteractor.Request>(compositeDisposable) {

    override fun buildUseCaseFlowable(request: Request): Flowable<DataResult<BaseResponse<UserSample>>> {
        return repository.loginSample(request.username, request.password)
    }

    data class Request(val username: String, val password: String)
}