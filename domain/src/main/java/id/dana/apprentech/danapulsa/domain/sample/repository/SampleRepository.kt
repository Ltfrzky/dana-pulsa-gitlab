package id.dana.apprentech.danapulsa.domain.sample.repository

import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.sample.model.UserSample
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable

/**
 *created by Lutfi Rizky Ramadhan on 04/06/20
 */

interface SampleRepository {

    fun loginSample(username: String, password: String) : Flowable<DataResult<BaseResponse<UserSample>>>

    fun getUserData() : Flowable<UserSample>
}