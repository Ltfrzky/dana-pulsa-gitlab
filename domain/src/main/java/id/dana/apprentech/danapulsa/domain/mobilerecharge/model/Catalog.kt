package id.dana.apprentech.danapulsa.domain.mobilerecharge.model

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

data class Catalog(
    val catalogId: Int,
    val value: Int,
    val price: Int
)