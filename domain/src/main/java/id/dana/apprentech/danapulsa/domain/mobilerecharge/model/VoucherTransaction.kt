package id.dana.apprentech.danapulsa.domain.mobilerecharge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *created by Lutfi Rizky Ramadhan on 13/06/20
 */
@Parcelize
data class VoucherTransaction(
    val voucherId: Int?,
    val name: String?,
    val deduction: Int?,
    val maxDeduction: Int?
) : Parcelable