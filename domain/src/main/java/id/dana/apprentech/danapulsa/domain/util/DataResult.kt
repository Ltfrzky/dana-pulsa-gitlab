package id.dana.apprentech.danapulsa.domain.util

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

sealed class DataResult<out T> {

    data class Success<out T>(val successData: T) : DataResult<T>()

    class Error(
        val exception: Exception,
        val code: Int = 999,
        val message: String = exception.localizedMessage
    ) : DataResult<Nothing>()

}