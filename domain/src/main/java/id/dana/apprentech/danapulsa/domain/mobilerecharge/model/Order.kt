package id.dana.apprentech.danapulsa.domain.mobilerecharge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */
@Parcelize
data class Order(
    val orderId: Int,
    val phoneNumber: String,
    val catalog: CatalogOrder
) : Parcelable