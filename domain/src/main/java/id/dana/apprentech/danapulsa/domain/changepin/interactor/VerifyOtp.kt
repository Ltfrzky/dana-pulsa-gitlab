package id.dana.apprentech.danapulsa.domain.changepin.interactor

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.changepin.repository.ChangePinRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class VerifyOtp @Inject constructor(
    private val repository: ChangePinRepository,
    compositeDisposable: CompositeDisposable
) : SingleUseCase<DataResult<NonBodyResponse>, VerifyOtp.Request>(compositeDisposable){

    override fun buildUseCaseSingle(request: Request): Single<DataResult<NonBodyResponse>> {
        return repository.verifyOtp(request.userId, request.code)
    }

    inner class Request(
        val userId: Int,
        val code: Int
    )
}