package id.dana.apprentech.danapulsa.domain.base

/**
 *created by Lutfi Rizky Ramadhan on 04/06/20
 */

data class BaseResponse<T>(
    var code: Int,

    var data: T,

    var message: String
)