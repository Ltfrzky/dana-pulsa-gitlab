package id.dana.apprentech.danapulsa.domain.history.model

/**
 *created by Lutfi Rizky Ramadhan on 13/06/20
 */

data class History(
    val id: Int,
    val phoneNumber: String,
    val price: Int,
    val voucher: Int,
    val status: String,
    val createdAt: String
)