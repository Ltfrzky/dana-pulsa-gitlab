package id.dana.apprentech.danapulsa.domain.base

import io.reactivex.rxjava3.observers.DisposableSingleObserver

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

open class BaseSingleSubscriber<T> : DisposableSingleObserver<T>() {
    override fun onSuccess(response: T) {}

    override fun onError(e: Throwable?) {}

}