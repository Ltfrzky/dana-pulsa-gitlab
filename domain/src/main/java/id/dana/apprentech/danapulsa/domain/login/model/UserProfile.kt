package id.dana.apprentech.danapulsa.domain.login.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserProfile(
    val id: Int,
    val name: String?,
    val email: String?,
    val phone: String?
) : Parcelable