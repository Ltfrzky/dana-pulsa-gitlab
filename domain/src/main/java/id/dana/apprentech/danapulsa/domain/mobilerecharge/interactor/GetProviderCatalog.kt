package id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor

import id.dana.apprentech.danapulsa.domain.UseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.ProviderCatalog
import id.dana.apprentech.danapulsa.domain.mobilerecharge.repository.MobileRechargeRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class GetProviderCatalog @Inject
constructor(
    private val repository: MobileRechargeRepository,
    compositeDisposable: CompositeDisposable
) : UseCase<DataResult<BaseResponse<ProviderCatalog>>, GetProviderCatalog.Request>(compositeDisposable) {

    override fun buildUseCaseFlowable(request: Request): Flowable<DataResult<BaseResponse<ProviderCatalog>>> {
        return repository.getProviderCatalog(request.phoneNumber)
    }

    data class Request(val phoneNumber: String)
}