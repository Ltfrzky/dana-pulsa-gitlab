package id.dana.apprentech.danapulsa.domain.mobilerecharge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */
@Parcelize
data class Payment(
    val balance: Int,
    val rewardVoucher: Boolean,
    val transaction: Transaction
) : Parcelable