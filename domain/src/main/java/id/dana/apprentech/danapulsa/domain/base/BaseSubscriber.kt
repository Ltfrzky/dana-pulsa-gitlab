package id.dana.apprentech.danapulsa.domain.base

import io.reactivex.rxjava3.subscribers.ResourceSubscriber

/**
 *created by Lutfi Rizky Ramadhan on 08/06/20
 */

open class BaseSubscriber<T> : ResourceSubscriber<T>() {

    override fun onComplete() {

    }

    override fun onNext(response: T) {

    }

    override fun onError(e: Throwable?) {

    }
}