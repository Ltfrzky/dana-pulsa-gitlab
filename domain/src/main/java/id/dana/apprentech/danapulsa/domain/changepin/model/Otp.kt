package id.dana.apprentech.danapulsa.domain.changepin.model

data class Otp(
    val id: Int,
    val userId: Int,
    val code: Int
) {
}