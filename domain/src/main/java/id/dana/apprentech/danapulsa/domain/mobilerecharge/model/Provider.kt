package id.dana.apprentech.danapulsa.domain.mobilerecharge.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */
@Parcelize
data class Provider(
    val providerId: Int,
    val name: String,
    val image: String
) : Parcelable