package id.dana.apprentech.danapulsa.domain.mobilerecharge.repository

import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.*
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

interface MobileRechargeRepository {
    fun getRecentNumbers(): Flowable<DataResult<BaseResponse<List<RecentNumber>>>>

    fun getProviderCatalog(phoneNumber: String) : Flowable<DataResult<BaseResponse<ProviderCatalog>>>

    fun orderPulsa(phoneNumber: String, catalogId: String) : Single<DataResult<BaseResponse<Order>>>

    fun payOrder(transactionId : String, methodId:String, voucherId: String) : Single<DataResult<BaseResponse<Payment>>>

    fun cancelOrder(transactionId: String) : Single<DataResult<BaseResponse<Transaction>>>

    fun getVoucherRecommendation(transactionId: String) : Flowable<DataResult<BaseResponse<List<Voucher>>>>

    fun getOrderDetail(transactionId: String) : Single<DataResult<BaseResponse<Order>>>
}