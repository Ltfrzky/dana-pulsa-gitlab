package id.dana.apprentech.danapulsa.domain.register.interactor

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.register.model.User
import id.dana.apprentech.danapulsa.domain.register.repository.RegisterRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class RegisterInteractor @Inject constructor(
    private val repository: RegisterRepository,
    compositeDisposable: CompositeDisposable
) : SingleUseCase<DataResult<NonBodyResponse>, RegisterInteractor.Request>(compositeDisposable) {

    override fun buildUseCaseSingle(request: Request): Single<DataResult<NonBodyResponse>> {
        return repository.registerUser(name = request.name, email = request.email, phone = request.phone, pin = request.pin)
    }

    data class Request(val name: String, val email: String, val phone: String, val pin: Int)
}