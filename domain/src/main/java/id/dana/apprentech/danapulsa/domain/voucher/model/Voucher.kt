package id.dana.apprentech.danapulsa.domain.voucher.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */
@Parcelize
data class Voucher(
    val voucherId: Int,
    val name: String,
    val voucherTypeName: String,
    val discount: Int,
    val value: Int,
    val maxDeduction: Int,
    val minPurchase: Int,
    val filePath: String,
    val expiryDate: String,
    val paymentMethod: String?
): Parcelable