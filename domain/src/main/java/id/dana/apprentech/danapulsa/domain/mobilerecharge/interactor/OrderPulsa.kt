package id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Order
import id.dana.apprentech.danapulsa.domain.mobilerecharge.repository.MobileRechargeRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class OrderPulsa @Inject constructor(
    private val repository: MobileRechargeRepository,
    compositeDisposable: CompositeDisposable
) : SingleUseCase<DataResult<BaseResponse<Order>>, OrderPulsa.Request>(compositeDisposable) {

    override fun buildUseCaseSingle(request: Request): Single<DataResult<BaseResponse<Order>>> {
        return repository.orderPulsa(request.phoneNumber, request.catalogId)
    }

    data class Request(val phoneNumber: String, val catalogId: String)
}