package id.dana.apprentech.danapulsa.domain.base

data class NonBodyResponse(
    val code: Int,
    val message: String
) {
}