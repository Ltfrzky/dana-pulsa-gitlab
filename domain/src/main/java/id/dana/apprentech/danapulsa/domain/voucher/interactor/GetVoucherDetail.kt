package id.dana.apprentech.danapulsa.domain.voucher.interactor

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import id.dana.apprentech.danapulsa.domain.voucher.repository.VoucherRepository
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

class GetVoucherDetail @Inject constructor(
    private val repository: VoucherRepository,
    compositeDisposable: CompositeDisposable
): SingleUseCase<DataResult<BaseResponse<Voucher>>, GetVoucherDetail.Request>(compositeDisposable){
    override fun buildUseCaseSingle(request: Request): Single<DataResult<BaseResponse<Voucher>>> {
        return repository.getVoucherDetail(request.voucherId)
    }

    class Request(val voucherId: String)
}