package id.dana.apprentech.danapulsa.domain.history.interactor

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.history.repository.HistoryRepository
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 18/06/20
 */

class GetHistoryDetail @Inject constructor(
    private val repository: HistoryRepository,
    compositeDisposable: CompositeDisposable
) : SingleUseCase<DataResult<BaseResponse<Transaction>>, GetHistoryDetail.Request>(
    compositeDisposable
) {

    override fun buildUseCaseSingle(request: Request): Single<DataResult<BaseResponse<Transaction>>> {
        return repository.getHistoryDetail(request.transactionId)
    }

    data class Request(val transactionId: String)

}