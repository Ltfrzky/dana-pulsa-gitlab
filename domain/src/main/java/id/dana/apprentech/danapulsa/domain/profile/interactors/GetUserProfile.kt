package id.dana.apprentech.danapulsa.domain.profile.interactors

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.profile.repository.ProfileRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

class GetUserProfile @Inject constructor(
    private val repository: ProfileRepository,
    compositeDisposable: CompositeDisposable
) : SingleUseCase<DataResult<BaseResponse<UserProfile>>,Empty>(compositeDisposable){

    override fun buildUseCaseSingle(request: Empty): Single<DataResult<BaseResponse<UserProfile>>> {
        return repository.getUserProfile()
    }
}