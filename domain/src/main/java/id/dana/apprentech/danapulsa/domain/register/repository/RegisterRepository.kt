package id.dana.apprentech.danapulsa.domain.register.repository

import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.register.model.User
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single

interface RegisterRepository {
    fun registerUser(name: String, email: String, phone: String, pin: Int): Single<DataResult<NonBodyResponse>>
}