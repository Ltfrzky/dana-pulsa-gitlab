package id.dana.apprentech.danapulsa.domain

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subscribers.ResourceSubscriber

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

abstract class SingleUseCase<T, Request>(private val compositeDisposable: CompositeDisposable) {

    protected abstract fun buildUseCaseSingle(request: Request): Single<T>

    fun executeSingle(subscriber: DisposableSingleObserver<T>, request: Request): CompositeDisposable {
        return addSubscription(executeAsSingle(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(subscriber))
    }

    private fun executeAsSingle(request: Request): Single<T> = buildUseCaseSingle(request)

    private fun addSubscription(subscription: DisposableSingleObserver<T>): CompositeDisposable {
        if (compositeDisposable.size() > 0) {
            compositeDisposable.remove(subscription)
        }

        compositeDisposable.add(subscription)
        return compositeDisposable
    }

    fun clearSubscription() {
        compositeDisposable.clear()
    }
}