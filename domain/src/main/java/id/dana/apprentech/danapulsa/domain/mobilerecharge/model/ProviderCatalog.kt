package id.dana.apprentech.danapulsa.domain.mobilerecharge.model

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

data class ProviderCatalog(
    val provider: Provider,
    val catalog: List<Catalog>
)