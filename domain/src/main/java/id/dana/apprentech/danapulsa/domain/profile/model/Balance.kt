package id.dana.apprentech.danapulsa.domain.profile.model

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

data class Balance(
    val balance: Int
)