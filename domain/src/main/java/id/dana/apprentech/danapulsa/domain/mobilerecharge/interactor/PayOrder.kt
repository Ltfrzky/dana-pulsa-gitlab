package id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor

import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Payment
import id.dana.apprentech.danapulsa.domain.mobilerecharge.repository.MobileRechargeRepository
import id.dana.apprentech.danapulsa.domain.util.DataResult
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class PayOrder @Inject constructor(
    private val repository: MobileRechargeRepository,
    compositeDisposable: CompositeDisposable
) : SingleUseCase<DataResult<BaseResponse<Payment>>, PayOrder.Request>(compositeDisposable) {

    override fun buildUseCaseSingle(request: Request): Single<DataResult<BaseResponse<Payment>>> {
        return repository.payOrder(request.transactionId, request.methodId, request.voucherId)
    }

    data class Request(val transactionId: String, val methodId: String, val voucherId: String)
}