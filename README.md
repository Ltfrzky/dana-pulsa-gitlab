# DANA PULSA APPRENTECH ANDROID #

## Mentor ##
- Hilmi Ilyas
- Perdi Mardhothillah

## Apprentech ##
- Lutfi Rizky
- Ramadhan Chairan

## Dependencies ##
- Dagger 2
- RxJava 3 / RxAndroid 3
- Retrofit 2
- Okhttp 3
- Glide

## Architecture ##
- MVP Clean Architecture

## Project Structure ##
- App → Main android module, presentation/UI layer, have dependencies with data and domain layer
- Data → android module where all data is retrieved. have dependencies with domain layer
- Domain → business rules, contains usecases/interactor, repository interface. have no dependencies with any layer

## Package Organization ##
Implemented package by feature e.g :

- App (Module)
    - UI
        - Sample (Feature)
            - Activity
            - Contract
            - Presenter

- Domain (Module)
    - Sample (Feature)
        - Interactors
        - Model
        - Repository