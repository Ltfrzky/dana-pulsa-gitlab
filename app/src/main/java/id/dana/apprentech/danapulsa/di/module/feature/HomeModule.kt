package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.data.profile.ProfileRepositoryImpl
import id.dana.apprentech.danapulsa.data.profile.network.ProfileApi
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserBalance
import id.dana.apprentech.danapulsa.domain.profile.repository.ProfileRepository
import id.dana.apprentech.danapulsa.ui.home.HomeContract
import id.dana.apprentech.danapulsa.ui.home.HomePresenter
import id.dana.apprentech.danapulsa.ui.home.HomePresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */
@Module
class HomeModule {

    @Provides
    fun provideGetUserBalance(
        repository: ProfileRepository,
        compositeDisposable: CompositeDisposable
    ): GetUserBalance = GetUserBalance(repository, compositeDisposable)

    @Provides
    fun provideHomePresenter(
        context: Context,
        getUserBalance: GetUserBalance
    ): HomePresenterContract<HomeContract> = HomePresenter(context, getUserBalance)
}