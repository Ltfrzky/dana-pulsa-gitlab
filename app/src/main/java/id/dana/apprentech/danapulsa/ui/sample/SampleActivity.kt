package id.dana.apprentech.danapulsa.ui.sample

import android.content.Context
import android.content.Intent
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.sample.model.UserSample
import id.dana.apprentech.danapulsa.util.MessageUtils
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import kotlinx.android.synthetic.main.activity_sample.*
import javax.inject.Inject

class SampleActivity : BaseActivity(),
    SampleContract {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, SampleActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Inject
    lateinit var presenter: SamplePresenterContract<SampleContract>

    override fun getLayoutResource(): Int = R.layout.activity_sample

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)

    }

    override fun setupUI() {

    }

    override fun setupAction() {
        btnSignIn.setOnClickListener {
            presenter.postLogin(
                tilEmailOrPhone.editText?.text.toString(),
                tilPassword.editText?.text.toString()
            )
        }
    }

    override fun setupProcess() {
        presenter.getUserData()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }


    override fun loginSuccess(response: BaseResponse<UserSample>) {
        MessageUtils.showMessage(this, response.data.name)
    }

    override fun loginFailed(response: DataResult.Error) {
        MessageUtils.showMessage(this, "${response.code} - ${response.message}")
    }

    override fun loginLoading() {
        MessageUtils.showMessage(this, "Loading")
    }

    override fun getUserDataSuccess(user: UserSample) {
        if (user.name.isNotBlank()) {
            MessageUtils.showMessage(this, user.name)
        }
    }
}