package id.dana.apprentech.danapulsa.util

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

object BundleKeys {
    const val KEY_SESSION = "KEY_SESSION"

    const val KEY_ORDER = "ORDER"

    const val KEY_TOKEN = "key_token"
    const val KEY_NAME = "fullname"
    const val KEY_PHONE = "phone"
    const val KEY_EMAIL = "email"
    const val KEY_PIN = "pin"

    const val KEY_USER_ID = "USER_ID"

    const val KEY_USER_PROFILE = "USER_PROFILE"

    const val KEY_VERIFY_PIN_DESTINATION = "VERIFY_PIN_DESTINATION"

    const val KEY_TRANSACTION_ID = "TRANSACTION_ID"

    const val KEY_TRANSACTION = "TRANSACTION"

    const val KEY_PAYMENT = "PAYMENT"

    const val KEY_VOUCHER_LIST = "VOUCHER_LIST"

    const val KEY_PICKED_VOUCHER = "PICKED_VOUCHER"

    const val KEY_CHANGE_PIN = "CHANGE_PIN"

    const val KEY_VOUCHER_ID = "VOUCHER_ID"

    const val KEY_HISTORY_TYPE = "HISTORY_TYPE"
}