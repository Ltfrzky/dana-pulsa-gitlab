package id.dana.apprentech.danapulsa.base

import androidx.lifecycle.Lifecycle

/**
 *created by Lutfi Rizky Ramadhan on 04/06/20
 */

interface BasePresenterContract<V> {

    fun onAttach(view: V?, lifecycle: Lifecycle)

    fun onDetach()

    fun getView(): V?
}