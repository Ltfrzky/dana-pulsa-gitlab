package id.dana.apprentech.danapulsa.ui.verifypin

import id.dana.apprentech.danapulsa.base.BasePresenterContract

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface VerifyPinPresenterContract<V: VerifyPinContract> : BasePresenterContract<V> {
    fun verifyPin(userId: Int, pin: Int)
}