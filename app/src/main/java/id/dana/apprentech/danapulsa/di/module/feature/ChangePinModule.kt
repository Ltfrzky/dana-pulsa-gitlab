package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.changepin.ChangePinRepositoryImpl
import id.dana.apprentech.danapulsa.data.changepin.network.ChangePinApi
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.domain.changepin.interactor.*
import id.dana.apprentech.danapulsa.domain.changepin.repository.ChangePinRepository
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfileLocal
import id.dana.apprentech.danapulsa.ui.changepin.ChangePinContract
import id.dana.apprentech.danapulsa.ui.changepin.ChangePinPresenter
import id.dana.apprentech.danapulsa.ui.changepin.ChangePinPresenterContract
import id.dana.apprentech.danapulsa.ui.otp.OtpContract
import id.dana.apprentech.danapulsa.ui.otp.OtpPresenter
import id.dana.apprentech.danapulsa.ui.otp.OtpPresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ChangePinModule {
    @Provides
    @Singleton
    fun provideChangePinApi(retrofit: Retrofit): ChangePinApi =
        retrofit.create(ChangePinApi::class.java)

    @Provides
    @Singleton
    fun provideChangePinRepository(
        changePinApi: ChangePinApi,
        preferencesManager: PreferencesManager
    ): ChangePinRepository =
        ChangePinRepositoryImpl(changePinApi, preferencesManager)

    @Provides
    @Singleton
    fun provideChangePinInteractor(
        repository: ChangePinRepository,
        compositeDisposable: CompositeDisposable
    ): ChangePin = ChangePin(repository, compositeDisposable)

    @Provides
    @Singleton
    fun provideGetChangePinOtpInteractor(
        repository: ChangePinRepository,
        compositeDisposable: CompositeDisposable
    ): GetChangePinOtp = GetChangePinOtp(repository, compositeDisposable)

    @Provides
    @Singleton
    fun provideGetForgotPinOtpInteractor(
        repository: ChangePinRepository,
        compositeDisposable: CompositeDisposable
    ): GetForgotPinOtp = GetForgotPinOtp(repository, compositeDisposable)

    @Provides
    @Singleton
    fun provideResendOtpInteractor(
        repository: ChangePinRepository,
        compositeDisposable: CompositeDisposable
    ): ResendOtp = ResendOtp(repository, compositeDisposable)

    @Provides
    @Singleton
    fun provideVerifyOtpInteractor(
        repository: ChangePinRepository,
        compositeDisposable: CompositeDisposable
    ): VerifyOtp = VerifyOtp(repository, compositeDisposable)

    @Provides
    @Singleton
    fun provideOtpPresenter(
        context: Context,
        getUserProfileLocal: GetUserProfileLocal,
        getChangePinOtp: GetChangePinOtp,
        getForgotPinOtp: GetForgotPinOtp,
        resendOtp: ResendOtp,
        verifyOtp: VerifyOtp
    ): OtpPresenterContract<OtpContract> =
        OtpPresenter(
            context,
            getUserProfileLocal,
            getChangePinOtp,
            getForgotPinOtp,
            resendOtp,
            verifyOtp
        )

    @Provides
    @Singleton
    fun provideChangePinPresenter(
        context: Context,
        changePin: ChangePin
    ): ChangePinPresenterContract<ChangePinContract> = ChangePinPresenter(context, changePin)
}