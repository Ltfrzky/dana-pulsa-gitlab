package id.dana.apprentech.danapulsa.custom

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import androidx.cardview.widget.CardView
import id.dana.apprentech.danapulsa.R
import kotlinx.android.synthetic.main.transaction_info_view.view.*

class TransactionInfoView(context: Context, attrs: AttributeSet) :
    CardView(context, attrs) {
    init {
        View.inflate(context, R.layout.transaction_info_view, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.TransactionInfoView)
        tvTransactionIdValue.text =
            attributes.getString(R.styleable.TransactionInfoView_transactionId)
        tvTransactionProductValue.text =
            attributes.getString(R.styleable.TransactionInfoView_transactionProduct)
        tvTransactionPhoneValue.text =
            attributes.getString(R.styleable.TransactionInfoView_transactionPhone)
        tvTransactionDateValue.text =
            attributes.getString(R.styleable.TransactionInfoView_transactionDate)
        tvTransactionStatusValue.text =
            attributes.getString(R.styleable.TransactionInfoView_transactionStatus)
        tvTransactionStatusValue.setTextColor(
            attributes.getColor(
                R.styleable.TransactionInfoView_transactionStatusColor,
                Color.parseColor("#212121")
            )
        )

        attributes.recycle()
    }

    fun setIdText(value: String?){
        if(value != null){
            tvTransactionIdValue.text = value
        }
    }

    fun setProductText(value: String?){
        tvTransactionProductValue.text = value
    }

    fun setPhoneText(value: String?){
        tvTransactionPhoneValue.text = value
    }

    fun setDateText(value: String?){
        tvTransactionDateValue.text = value
    }

    fun setStatusText(value: String?){
        tvTransactionStatusValue.text = value
    }

    fun setStatusTextColor(@ColorInt color: Int){
        tvTransactionStatusValue.setTextColor(color)
    }
}