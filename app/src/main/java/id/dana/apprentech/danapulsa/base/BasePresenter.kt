package id.dana.apprentech.danapulsa.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

/**
 *created by Lutfi Rizky Ramadhan on 04/06/20
 */

open class BasePresenter<V : BaseContract> : BasePresenterContract<V>, LifecycleObserver {

    private var view: V? = null
    private val isViewAttached: Boolean get() = view != null
    private var viewLifecycle: Lifecycle? = null

    override fun onAttach(view: V?, lifecycle: Lifecycle) {
        this.view = view
        viewLifecycle = lifecycle
        lifecycle.addObserver(this)
    }

    override fun onDetach() {
        view = null
    }

    override fun getView(): V? = view

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onViewDestroy() {
        onDetach()
        viewLifecycle = null
    }
}