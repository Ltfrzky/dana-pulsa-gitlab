package id.dana.apprentech.danapulsa.ui.mobilerecharge

import id.dana.apprentech.danapulsa.base.BasePresenterContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Order
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.ProviderCatalog
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.RecentNumber
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

interface MobileRechargePresenterContract<V: MobileRechargeContract> : BasePresenterContract<V> {
    fun getRecentNumbers()

    fun getProviderCatalog(phoneNumber: String)

    fun orderPulsa(phoneNumber: String, catalogId: String)
}