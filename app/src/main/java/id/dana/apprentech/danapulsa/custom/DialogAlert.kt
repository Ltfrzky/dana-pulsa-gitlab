package id.dana.apprentech.danapulsa.custom

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import id.dana.apprentech.danapulsa.R
import kotlinx.android.synthetic.main.fragment_dialog_ok.*

class DialogAlert(private val positiveAction: DialogListener?): DialogFragment() {

    companion object{
        const val DIALOG_TITLE = "title"
        const val DIALOG_CONTENT = "content"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dialog_ok, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvDialogTitle.text = arguments?.getString(DIALOG_TITLE)
        tvDialogContent.text = arguments?.getString(DIALOG_CONTENT)
        btnDialogPositive.setOnClickListener {
            positiveAction?.onPositiveButtonClick()
            dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        val params = dialog?.window?.attributes
        params?.width = ViewGroup.LayoutParams.MATCH_PARENT
        params?.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.attributes = params
    }

    interface DialogListener{
        fun onPositiveButtonClick()
    }
}