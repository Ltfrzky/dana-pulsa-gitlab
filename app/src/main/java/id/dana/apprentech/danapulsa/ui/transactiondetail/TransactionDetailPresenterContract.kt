package id.dana.apprentech.danapulsa.ui.transactiondetail

import id.dana.apprentech.danapulsa.base.BasePresenterContract

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

interface TransactionDetailPresenterContract<V: TransactionDetailContract> : BasePresenterContract<V> {
    fun getTransactionDetail(transactionId: String)
}