package id.dana.apprentech.danapulsa.ui.pickvoucher

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.base.adapter.BaseRecyclerAdapter
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import id.dana.apprentech.danapulsa.ui.adapter.VoucherAdapter
import id.dana.apprentech.danapulsa.util.BundleKeys
import kotlinx.android.synthetic.main.activity_pick_voucher.*
import kotlinx.android.synthetic.main.base_toolbar.*

class PickVoucherActivity : BaseActivity() {

    companion object {
        @JvmStatic
        fun start(context: Context, vouchers: List<Voucher>) : Intent {
            val starter = Intent(context, PickVoucherActivity::class.java)
            starter.putParcelableArrayListExtra(BundleKeys.KEY_VOUCHER_LIST, ArrayList(vouchers))
            return starter
        }
    }

    lateinit var vouchers: List<Voucher>

    lateinit var voucherAdapter: VoucherAdapter

    override fun getLayoutResource(): Int = R.layout.activity_pick_voucher

    override fun setupLib() {}

    override fun setupIntent() {
        vouchers = intent.getParcelableArrayListExtra(BundleKeys.KEY_VOUCHER_LIST)
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, getString(R.string.activity_pick_voucher), true)

        voucherAdapter = VoucherAdapter(this, ArrayList())
        rvVoucher.layoutManager = LinearLayoutManager(this)
        rvVoucher.setHasFixedSize(true)
        rvVoucher.adapter = voucherAdapter

        voucherAdapter.addItem(vouchers)
    }

    override fun setupAction() {
        voucherAdapter.setOnItemClickListener(object : BaseRecyclerAdapter.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                setPickedVoucher(voucherAdapter.data[position])
            }
        })

        btnSelectWithoutVoucher.setOnClickListener {
            setPickedVoucher(null)
        }
    }

    override fun setupProcess() {}

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun setPickedVoucher(voucher: Voucher?) {
        val result = Intent().putExtra(BundleKeys.KEY_PICKED_VOUCHER, voucher)
        setResult(Activity.RESULT_OK, result)
        finish()
    }
}