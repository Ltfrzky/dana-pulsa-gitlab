package id.dana.apprentech.danapulsa.ui.profile

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult

interface ProfileContract: BaseContract {
    fun getProfileSuccess(response: BaseResponse<UserProfile>)

    fun getProfileSuccess(response: UserProfile)

    fun getProfileFailed(response: DataResult.Error)

    fun getProfileLoading()

    fun onFailedLogout(response: DataResult.Error)

    fun onLogout()

    fun changePin()
}