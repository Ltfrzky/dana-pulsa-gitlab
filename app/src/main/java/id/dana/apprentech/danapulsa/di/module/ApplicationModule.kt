package id.dana.apprentech.danapulsa.di.module

import android.app.Application
import android.content.Context
import id.dana.apprentech.danapulsa.BuildConfig
import id.dana.apprentech.danapulsa.BaseApplication
import id.dana.apprentech.danapulsa.data.util.HeaderInterceptor
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.data.preference.PreferencesManagerImpl
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.util.SaveCookieInterceptor
import io.reactivex.rxjava3.disposables.CompositeDisposable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

@Module
class ApplicationModule(private val baseApplication: BaseApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = baseApplication

    @Provides
    @Singleton
    fun provideApplicationContext(application: Application): Context = application

    @Provides
    @Singleton
    fun providePreferenceManager(context: Context): PreferencesManager =
        PreferencesManagerImpl(context)

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
        headerInterceptor: HeaderInterceptor,
        saveCookieInterceptor: SaveCookieInterceptor
    ): OkHttpClient {
        val timeout: Long = 120
        return OkHttpClient().newBuilder()
            .readTimeout(timeout, TimeUnit.SECONDS)
            .connectTimeout(timeout, TimeUnit.SECONDS)
            .addInterceptor(headerInterceptor)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(saveCookieInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideHeaderInterceptor(preferencesManager: PreferencesManager): HeaderInterceptor =
        HeaderInterceptor(preferencesManager)


    @Provides
    @Singleton
    fun provideSaveCookieInterceptor(preferencesManager: PreferencesManager): SaveCookieInterceptor =
        SaveCookieInterceptor(preferencesManager)

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        val logger = HttpLoggingInterceptor()
        logger.level = HttpLoggingInterceptor.Level.BODY
        return logger
    }

    @Provides
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()
}