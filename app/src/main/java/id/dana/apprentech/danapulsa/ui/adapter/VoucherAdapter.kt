package id.dana.apprentech.danapulsa.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.adapter.BaseRecyclerAdapter
import id.dana.apprentech.danapulsa.base.adapter.BaseViewHolder
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import id.dana.apprentech.danapulsa.util.CommonUtils
import kotlinx.android.synthetic.main.item_voucher.view.*

/**
 *created by Lutfi Rizky Ramadhan on 17/06/20
 */

class VoucherAdapter(context: Context, data: MutableList<Voucher>) :
    BaseRecyclerAdapter<Voucher, VoucherAdapter.VoucherViewHolder>(context, data) {

    override fun getItemResourceLayout(viewType: Int): Int = R.layout.item_voucher

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VoucherViewHolder {
        return VoucherViewHolder(
            context,
            getView(parent, viewType),
            itemClickListener,
            longItemClickListener
        )
    }

    override fun onBindViewHolder(holder: VoucherViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.bind(data[position])
    }

    class VoucherViewHolder(
        context: Context,
        itemView: View,
        itemClickListener: OnItemClickListener?,
        itemLongItemClickListener: OnLongItemClickListener?
    ) : BaseViewHolder<Voucher>(context, itemView, itemClickListener, itemLongItemClickListener) {

        override fun bind(data: Voucher) {
            if (data.discount > 0) {
                itemView.tvVoucherName.text = data.name
                itemView.tvVoucherAmount.text = context.getString(
                    R.string.item_voucher_max_deduction,
                    CommonUtils.formatCurrency(data.maxDeduction)
                )
                itemView.tvVoucherInfo.text = context.getString(
                    R.string.item_voucher_minimum_purchase,
                    CommonUtils.formatCurrency(data.minPurchase)
                )
                itemView.tvVoucherExpireDate.text = context.getString(
                    R.string.item_voucher_valid_until,
                    CommonUtils.getHumanReadableDateTime(data.expiryDate)
                )
            } else {
                itemView.tvVoucherName.text = data.name
                itemView.tvVoucherAmount.text =
                    context.getString(
                        R.string.item_voucher_cashback_amount,
                        CommonUtils.formatCurrency(data.value)
                    )
                itemView.tvVoucherInfo.text = context.getString(
                    R.string.item_voucher_minimum_purchase,
                    CommonUtils.formatCurrency(data.minPurchase)
                )
                itemView.tvVoucherExpireDate.text = context.getString(
                    R.string.item_voucher_valid_until,
                    CommonUtils.getHumanReadableDateTime(data.expiryDate)
                )
            }
        }
    }
}