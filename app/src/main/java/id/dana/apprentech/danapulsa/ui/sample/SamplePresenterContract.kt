package id.dana.apprentech.danapulsa.ui.sample

import id.dana.apprentech.danapulsa.base.BasePresenterContract

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

interface SamplePresenterContract<V : SampleContract> :
    BasePresenterContract<V> {
    fun postLogin(username: String, password: String)

    fun getUserData()
}