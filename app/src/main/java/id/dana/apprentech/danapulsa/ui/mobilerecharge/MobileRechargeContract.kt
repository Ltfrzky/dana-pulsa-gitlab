package id.dana.apprentech.danapulsa.ui.mobilerecharge

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Order
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.ProviderCatalog
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.RecentNumber
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

interface MobileRechargeContract : BaseContract {
    fun getRecentNumberSuccess(result: BaseResponse<List<RecentNumber>>)

    fun getRecentNumberFailed(result: DataResult.Error)

    fun getRecentNumberFinished()

    fun getCatalogSuccess(result: BaseResponse<ProviderCatalog>)

    fun getCatalogFailed(result: DataResult.Error)

    fun getCatalogFinished()

    fun orderPulsaSuccess(result: BaseResponse<Order>)

    fun orderPulsaFailed(result: DataResult.Error)

    fun orderPulsaFinished()

    fun showLoading()
}