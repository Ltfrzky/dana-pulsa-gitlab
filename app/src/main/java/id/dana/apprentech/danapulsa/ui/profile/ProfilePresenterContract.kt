package id.dana.apprentech.danapulsa.ui.profile

import id.dana.apprentech.danapulsa.base.BasePresenterContract

interface ProfilePresenterContract<V: ProfileContract>: BasePresenterContract<V> {
    fun getProfile()
    fun getLocalProfile()
    fun logOut()
}