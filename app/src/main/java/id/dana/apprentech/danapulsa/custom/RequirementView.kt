package id.dana.apprentech.danapulsa.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import id.dana.apprentech.danapulsa.R
import kotlinx.android.synthetic.main.requirement_view.view.*

class RequirementView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    init {
        View.inflate(context, R.layout.requirement_view, this)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.RequirementView)

        ivRequirementIcon.setImageDrawable(attributes.getDrawable(R.styleable.RequirementView_icon))
        tvRequirementLabel.text = attributes.getString(R.styleable.RequirementView_title)
        tvRequirementValue.text = attributes.getString(R.styleable.RequirementView_value)

        attributes.recycle()
    }

    fun setTitleText(value: String?){
        if(value != null){
            tvRequirementLabel.text = value
        }
    }

    fun setValueText(value: String?){
        if(value != null){
            tvRequirementValue.text = value
        }
    }
}