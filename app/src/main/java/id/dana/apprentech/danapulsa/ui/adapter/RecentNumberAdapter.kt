package id.dana.apprentech.danapulsa.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.adapter.BaseRecyclerAdapter
import id.dana.apprentech.danapulsa.base.adapter.BaseViewHolder
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.RecentNumber
import id.dana.apprentech.danapulsa.util.CommonUtils
import kotlinx.android.synthetic.main.item_phone_number.view.*

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class RecentNumberAdapter(context: Context, data: MutableList<RecentNumber>) :
    BaseRecyclerAdapter<RecentNumber, RecentNumberAdapter.RecentNumberViewHolder>(context, data) {

    override fun getItemResourceLayout(viewType: Int): Int = R.layout.item_phone_number

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentNumberViewHolder {
        return RecentNumberViewHolder(
            context,
            getView(parent, viewType),
            itemClickListener,
            longItemClickListener
        )
    }

    override fun onBindViewHolder(holder: RecentNumberViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.bind(data[position])
    }

    class RecentNumberViewHolder(
        context: Context,
        itemView: View,
        itemClickListener: OnItemClickListener?,
        longItemClickListener: OnLongItemClickListener?
    ) : BaseViewHolder<RecentNumber>(context, itemView, itemClickListener, longItemClickListener) {

        override fun bind(data: RecentNumber) {
            itemView.tvRecentNumberPhone.text = CommonUtils.formatPhoneNumber(data.number)
            itemView.tvRecentNumberDate.text = CommonUtils.getHumanReadableDateTime(data.date)
            Glide.with(itemView).load(data.provider.image).into(itemView.ivRecentNumberProvider)
        }
    }


}