package id.dana.apprentech.danapulsa.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.adapter.BaseRecyclerAdapter
import id.dana.apprentech.danapulsa.base.adapter.BaseViewHolder
import id.dana.apprentech.danapulsa.domain.voucher.model.Promotion
import id.dana.apprentech.danapulsa.util.CommonUtils
import kotlinx.android.synthetic.main.item_data_loading.view.*
import kotlinx.android.synthetic.main.item_promotion.view.*

class PromotionAdapter(context: Context, data: MutableList<Promotion>) :
    BaseRecyclerAdapter<Promotion, BaseViewHolder<Promotion>>(context, data) {

    override fun getItemResourceLayout(viewType: Int): Int = R.layout.item_promotion

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<Promotion> {
        return PromotionViewHolder(
            context,
            getView(parent, viewType),
            itemClickListener,
            longItemClickListener
        )
    }


    inner class PromotionViewHolder(
        context: Context,
        itemView: View,
        itemClickListener: OnItemClickListener?,
        longItemClickListener: OnLongItemClickListener?
    ) : BaseViewHolder<Promotion>(context, itemView, itemClickListener, longItemClickListener) {

        override fun bind(data: Promotion) {
            Glide
                .with(itemView)
                .load(data.filePath)
                .into(itemView.ivPromotionItemImage)
            itemView.tvPromotionType.text = data.voucherTypeName
            itemView.tvPromotionItemTitle.text = data.name
            itemView.tvPromotionItemValid.text =
                context.getString(
                    R.string.item_voucher_valid_until,
                    CommonUtils.getHumanReadableDateTime(data.expiryDate)
                )
        }
    }
}