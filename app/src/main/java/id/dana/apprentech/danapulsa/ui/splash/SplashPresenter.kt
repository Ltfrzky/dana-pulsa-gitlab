package id.dana.apprentech.danapulsa.ui.splash

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfile
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfileLocal
import id.dana.apprentech.danapulsa.domain.util.DataResult
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

class SplashPresenter<V : SplashContract> @Inject constructor(
    private val context: Context,
    private val getUserProfile: GetUserProfile,
    private val getUserProfileLocal: GetUserProfileLocal
) : BasePresenter<V>(), SplashPresenterContract<V> {

    override fun getUserProfileLocal() {
        getUserProfileLocal.executeSingle(object : BaseSingleSubscriber<UserProfile>() {
            override fun onSuccess(response: UserProfile) {
                super.onSuccess(response)
                getView()?.getUserProfile(response)
            }
        }, Empty())
    }

    override fun checkSession() {
        if (isOnline(context)) {
            getUserProfile.executeSingle(CheckSessionSubscriber(), Empty())
        } else {
            getView()?.checkSessionFailed(noNetworkConnectivityError())
        }
    }

    inner class CheckSessionSubscriber<T : DataResult<BaseResponse<UserProfile>>> :
        BaseSingleSubscriber<T>() {

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.checkSessionSuccess(response.successData as BaseResponse<UserProfile>)
                }

                is DataResult.Error -> {
                    getView()?.checkSessionFailed(response)
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.checkSessionFailed(noNetworkConnectivityError())
        }
    }
}