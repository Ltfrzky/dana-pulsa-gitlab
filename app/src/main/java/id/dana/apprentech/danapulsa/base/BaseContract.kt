package id.dana.apprentech.danapulsa.base

import androidx.appcompat.widget.Toolbar

/**
 *created by Lutfi Rizky Ramadhan on 03/06/20
 */

interface BaseContract {

    fun setupToolbar(toolbar: Toolbar, isBack: Boolean)

    fun setupToolbar(toolbar: Toolbar, title: String, isBack: Boolean)

    fun hideKeyboard()
}