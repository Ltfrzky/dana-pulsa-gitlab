package id.dana.apprentech.danapulsa.ui.history

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.history.model.History
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

interface HistoryContract : BaseContract {
    fun getHistorySuccess(result: BaseResponse<List<History>>)

    fun getHistoryFailed(result: DataResult.Error)

    fun getHistoryLoading()

    fun getHistoryLoadingFinished()
}