package id.dana.apprentech.danapulsa.ui.mobilerecharge

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.handleSubscriberError
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.BaseSubscriber
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.GetProviderCatalog
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.GetRecentNumbers
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.OrderPulsa
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Order
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.ProviderCatalog
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.RecentNumber
import id.dana.apprentech.danapulsa.domain.util.DataResult
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class MobileRechargePresenter<V : MobileRechargeContract> @Inject constructor(
    private val context: Context,
    private val getRecentNumbers: GetRecentNumbers,
    private val getProviderCatalog: GetProviderCatalog,
    private val orderPulsa: OrderPulsa
) : BasePresenter<V>(), MobileRechargePresenterContract<V> {
    override fun getRecentNumbers() {
        if (isOnline(context)) {
            getRecentNumbers.executeFlowable(RecentNumberSubscriber(), Empty())
        } else {
            getView()?.getRecentNumberFailed(noNetworkConnectivityError())
        }
    }

    override fun getProviderCatalog(phoneNumber: String) {
        if (isOnline(context)) {
            getProviderCatalog.executeFlowable(
                ProviderCatalogSubscriber(),
                GetProviderCatalog.Request(phoneNumber)
            )
        } else {
            getView()?.getCatalogFailed(noNetworkConnectivityError())
        }
    }

    override fun orderPulsa(phoneNumber: String, catalogId: String) {
        if (isOnline(context)) {
            orderPulsa.executeSingle(
                OrderPulsaSubscriber(),
                OrderPulsa.Request(phoneNumber, catalogId)
            )
        } else {
            getView()?.orderPulsaFailed(noNetworkConnectivityError())
        }
    }

    override fun onDetach() {
        super.onDetach()
        getRecentNumbers.clearSubscription()
        getProviderCatalog.clearSubscription()
        orderPulsa.clearSubscription()
    }

    inner class RecentNumberSubscriber<T : DataResult<BaseResponse<List<RecentNumber>>>> :
        BaseSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.showLoading()
        }

        override fun onNext(response: T) {
            super.onNext(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.getRecentNumberSuccess(response.successData as BaseResponse<List<RecentNumber>>)
                }

                is DataResult.Error -> {
                    getView()?.getRecentNumberFailed(response)
                }
            }
            getView()?.getRecentNumberFinished()
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getRecentNumberFailed(DataResult.Error(Exception(e?.message)))
            getView()?.getRecentNumberFinished()
        }

        override fun onComplete() {
            super.onComplete()
            getView()?.getRecentNumberFinished()
        }
    }

    inner class ProviderCatalogSubscriber<T : DataResult<BaseResponse<ProviderCatalog>>> :
        BaseSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.showLoading()
        }

        override fun onNext(response: T) {
            super.onNext(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.getCatalogSuccess(response.successData as BaseResponse<ProviderCatalog>)
                }

                is DataResult.Error -> {
                    getView()?.getCatalogFailed(response)
                }
            }
            getView()?.getCatalogFinished()
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getCatalogFailed(handleSubscriberError(e?.message))
            getView()?.getCatalogFinished()
        }

        override fun onComplete() {
            super.onComplete()
            getView()?.getCatalogFinished()
        }
    }

    inner class OrderPulsaSubscriber<T : DataResult<BaseResponse<Order>>> :
        BaseSingleSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.showLoading()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.orderPulsaSuccess(response.successData as BaseResponse<Order>)
                }

                is DataResult.Error -> {
                    getView()?.orderPulsaFailed(response)
                }
            }
            getView()?.orderPulsaFinished()
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.orderPulsaFailed(handleSubscriberError(e?.message))
            getView()?.orderPulsaFinished()
        }

    }
}