package id.dana.apprentech.danapulsa.ui.otp

import id.dana.apprentech.danapulsa.base.BasePresenterContract

interface OtpPresenterContract<V: OtpContract>: BasePresenterContract<V> {
    fun requestForgotPinOtp(userId: Int)

    fun requestChangePinOtp()

    fun resendOtp(userId: String)

    fun doOtpVerify(userId: Int, otpCode: Int)

    fun getLocalProfile()
}