package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.CancelOrder
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.GetOrderDetail
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.GetVoucherRecommendation
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.PayOrder
import id.dana.apprentech.danapulsa.domain.mobilerecharge.repository.MobileRechargeRepository
import id.dana.apprentech.danapulsa.ui.payment.PaymentContract
import id.dana.apprentech.danapulsa.ui.payment.PaymentPresenter
import id.dana.apprentech.danapulsa.ui.payment.PaymentPresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable

/**
 *created by Lutfi Rizky Ramadhan on 17/06/20
 */
@Module
class PaymentModule {

    @Provides
    fun provideGetOrderDetail(
        repository: MobileRechargeRepository,
        compositeDisposable: CompositeDisposable
    ): GetOrderDetail = GetOrderDetail(repository, compositeDisposable)

    @Provides
    fun provideGetVoucherRecommendation(
        repository: MobileRechargeRepository,
        compositeDisposable: CompositeDisposable
    ): GetVoucherRecommendation = GetVoucherRecommendation(repository, compositeDisposable)

    @Provides
    fun providePayOrder(
        repository: MobileRechargeRepository,
        compositeDisposable: CompositeDisposable
    ): PayOrder = PayOrder(repository, compositeDisposable)

    @Provides
    fun provideCancelOrder(
        repository: MobileRechargeRepository,
        compositeDisposable: CompositeDisposable
    ): CancelOrder = CancelOrder(repository, compositeDisposable)

    @Provides
    fun providePaymentPresenter(
        context: Context,
        getOrderDetail: GetOrderDetail,
        getVoucherRecommendation: GetVoucherRecommendation,
        payOrder: PayOrder,
        cancelOrder: CancelOrder
    ): PaymentPresenterContract<PaymentContract> =
        PaymentPresenter(context, getOrderDetail, getVoucherRecommendation, payOrder, cancelOrder)
}