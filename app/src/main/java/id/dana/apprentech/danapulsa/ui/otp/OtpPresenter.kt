package id.dana.apprentech.danapulsa.ui.otp

import android.content.Context
import android.util.Log
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.changepin.interactor.GetChangePinOtp
import id.dana.apprentech.danapulsa.domain.changepin.interactor.GetForgotPinOtp
import id.dana.apprentech.danapulsa.domain.changepin.interactor.ResendOtp
import id.dana.apprentech.danapulsa.domain.changepin.interactor.VerifyOtp
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfileLocal
import id.dana.apprentech.danapulsa.domain.util.DataResult
import java.lang.Exception
import javax.inject.Inject
import kotlin.math.log

class OtpPresenter<V : OtpContract> @Inject constructor(
    private val context: Context,
    private val getUserProfileLocal: GetUserProfileLocal,
    private val getChangePinOtp: GetChangePinOtp,
    private val getForgotPinOtp: GetForgotPinOtp,
    private val resendOtp: ResendOtp,
    private val verifyOtp: VerifyOtp
) : BasePresenter<V>(), OtpPresenterContract<V> {

    override fun requestForgotPinOtp(userId: Int) {
        if (isOnline(context)) {
            getForgotPinOtp.executeSingle(RequestOtpSubscriber(), getForgotPinOtp.Request(userId))
        } else {
            getView()?.onFailedRequestOtp(noNetworkConnectivityError())
        }
    }

    override fun requestChangePinOtp() {
        if (isOnline(context)) {
            getChangePinOtp.executeSingle(RequestOtpSubscriber(), Empty())
        } else {
            getView()?.onFailedRequestOtp(noNetworkConnectivityError())
        }
    }

    override fun resendOtp(userId: String) {
        if (isOnline(context)) {
            resendOtp.executeSingle(RequestOtpSubscriber(), resendOtp.Request(userId))
        } else {
            getView()?.onFailedRequestOtp(noNetworkConnectivityError())
        }
    }

    override fun doOtpVerify(userId: Int, otpCode: Int) {
        if (isOnline(context)) {
            verifyOtp.executeSingle(VerifyOtpSubscriber(), verifyOtp.Request(userId, otpCode))
        } else {
            getView()?.onFailedVerify(noNetworkConnectivityError())
        }
    }

    override fun getLocalProfile() {
        getUserProfileLocal.executeSingle(ProfileLocalSubscriber(), Empty())
    }

    inner class RequestOtpSubscriber : BaseSingleSubscriber<DataResult<NonBodyResponse>>() {

        override fun onStart() {
            super.onStart()
            getView()?.onGetOtp()
        }

        override fun onSuccess(response: DataResult<NonBodyResponse>) {
            super.onSuccess(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.notifyOtpSent(true)
                    getView()?.startCountDown()
                }
                is DataResult.Error -> {
                    getView()?.onFailedRequestOtp(response)
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.onFailedRequestOtp(DataResult.Error(Exception(e?.message)))
            getView()?.notifyOtpSent(false)
        }
    }

    inner class VerifyOtpSubscriber<T : DataResult<NonBodyResponse>>
        : BaseSingleSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.onVerifyOtp()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.onOtpVerified(response)
                }
                is DataResult.Error -> {
                    getView()?.onFailedVerify(response)
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.onFailedVerify(DataResult.Error(Exception(e?.message)))
        }
    }

    inner class ProfileLocalSubscriber
        : BaseSingleSubscriber<UserProfile>() {
        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getProfileFailed(DataResult.Error(Exception(e?.message)))
        }

        override fun onSuccess(response: UserProfile) {
            super.onSuccess(response)
            getView()?.getProfile(response)
        }
    }
}