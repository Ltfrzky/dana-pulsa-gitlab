package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.login.LoginRepositoryImpl
import id.dana.apprentech.danapulsa.data.login.network.LoginApi
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.domain.login.interactors.Login
import id.dana.apprentech.danapulsa.domain.login.interactors.VerifyPin
import id.dana.apprentech.danapulsa.domain.login.repository.LoginRepository
import id.dana.apprentech.danapulsa.domain.sample.interactor.LoginInteractor
import id.dana.apprentech.danapulsa.ui.login.LoginContract
import id.dana.apprentech.danapulsa.ui.login.LoginPresenter
import id.dana.apprentech.danapulsa.ui.login.LoginPresenterContract
import id.dana.apprentech.danapulsa.ui.verifypin.VerifyPinContract
import id.dana.apprentech.danapulsa.ui.verifypin.VerifyPinPresenter
import id.dana.apprentech.danapulsa.ui.verifypin.VerifyPinPresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */
@Module
class LoginModule {
    @Provides
    @Singleton
    fun provideLoginApi(retrofit: Retrofit): LoginApi = retrofit.create(LoginApi::class.java)

    @Provides
    fun provideLoginRepository(
        loginApi: LoginApi,
        preferencesManager: PreferencesManager
    ): LoginRepository = LoginRepositoryImpl(loginApi, preferencesManager)

    @Provides
    fun provideLoginInteractor(
        repository: LoginRepository,
        compositeDisposable: CompositeDisposable
    ): Login = Login(repository, compositeDisposable)

    @Provides
    fun provideVerifyPinInteractor(
        repository: LoginRepository,
        compositeDisposable: CompositeDisposable
    ): VerifyPin = VerifyPin(repository, compositeDisposable)

    @Provides
    fun provideLoginPresenter(
        context: Context,
        loginInteractor: Login
    ): LoginPresenterContract<LoginContract> = LoginPresenter(context, loginInteractor)

    @Provides
    fun provideVerifyPinPresenter(
        context: Context,
        verifyPinInteractor: VerifyPin
    ): VerifyPinPresenterContract<VerifyPinContract> =
        VerifyPinPresenter(context, verifyPinInteractor)
}