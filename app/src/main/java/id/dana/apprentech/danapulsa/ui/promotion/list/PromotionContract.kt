package id.dana.apprentech.danapulsa.ui.promotion.list

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Promotion

interface PromotionContract: BaseContract {

    fun onLoadPromotion()

    fun getPromotionSuccess(response: BaseResponse<List<Promotion>>)

    fun getPromotionFailed(response: DataResult.Error)

    fun getPromotionFinished()
}