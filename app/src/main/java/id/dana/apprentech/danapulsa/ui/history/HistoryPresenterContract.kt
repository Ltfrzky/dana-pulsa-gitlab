package id.dana.apprentech.danapulsa.ui.history

import id.dana.apprentech.danapulsa.base.BasePresenterContract

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

interface HistoryPresenterContract<V: HistoryContract> : BasePresenterContract<V> {
    fun getHistoryInProgress(page: Int, isLoadMore: Boolean)

    fun getHistoryCompleted(page: Int, isLoadMore: Boolean)
}