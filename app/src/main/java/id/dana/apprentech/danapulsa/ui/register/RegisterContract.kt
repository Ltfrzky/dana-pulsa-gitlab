package id.dana.apprentech.danapulsa.ui.register

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.register.model.User
import id.dana.apprentech.danapulsa.domain.util.DataResult

interface RegisterContract: BaseContract {

    fun setNextButtonAvailability(available: Boolean)

    fun onRegisterSuccess(response: NonBodyResponse)

    fun onRegisterFailed(response: DataResult.Error)

    fun registerLoading()

    fun invalidInput(message: String)
}