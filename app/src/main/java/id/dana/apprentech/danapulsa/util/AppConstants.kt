package id.dana.apprentech.danapulsa.util

/**
 *created by Lutfi Rizky Ramadhan on 18/06/20
 */

object AppConstants {
    const val PICK_CONTACT_RESULT = 1

    const val CHANGE_VOUCHER = 2
}