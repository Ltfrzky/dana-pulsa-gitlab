package id.dana.apprentech.danapulsa.ui.home

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.profile.model.Balance
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface HomeContract : BaseContract {
    fun getBalanceSuccess(result: BaseResponse<Int>)

    fun getBalanceFailed(result: DataResult.Error)

    fun getBalanceLoading()

    fun getBalanceFinished()
}