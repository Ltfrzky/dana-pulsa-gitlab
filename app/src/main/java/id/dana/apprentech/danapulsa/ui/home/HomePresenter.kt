package id.dana.apprentech.danapulsa.ui.home

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserBalance
import id.dana.apprentech.danapulsa.domain.util.DataResult
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

class HomePresenter<V : HomeContract> @Inject constructor(
    private val context: Context,
    private val getUserBalance: GetUserBalance
) : BasePresenter<V>(), HomePresenterContract<V> {

    override fun getUserBalance() {
        if (isOnline(context)) {
            getUserBalance.executeSingle(GetBalanceSubscriber(), Empty())
        } else {
            getView()?.getBalanceFailed(noNetworkConnectivityError())
        }
    }

    inner class GetBalanceSubscriber<T : DataResult<BaseResponse<Int>>> :
        BaseSingleSubscriber<T>() {

        override fun onStart() {
            super.onStart()
            getView()?.getBalanceLoading()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when(response){
                is DataResult.Success<*> -> {
                    getView()?.getBalanceSuccess(response.successData as BaseResponse<Int>)
                    getView()?.getBalanceFinished()
                }

                is DataResult.Error -> {
                    getView()?.getBalanceFailed(response)
                    getView()?.getBalanceFinished()
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getBalanceFailed(noNetworkConnectivityError())
            getView()?.getBalanceFinished()
        }
    }
}