package id.dana.apprentech.danapulsa.ui.changepin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.util.BundleKeys
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_CHANGE_PIN
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_PIN
import kotlinx.android.synthetic.main.activity_new_pin.*
import kotlinx.android.synthetic.main.base_toolbar.*

class NewPinActivity : BaseActivity(), ChangePinContract {

    private var changePinMode: Boolean = false

    override fun getLayoutResource(): Int = R.layout.activity_new_pin

    override fun setupLib() {
    }

    override fun setupUI() {
        btnChangePinNext.isEnabled = false
        if(changePinMode){
            setupToolbar(baseToolbar, getString(R.string.change_pin), true)
        }else{
            setupToolbar(baseToolbar, getString(R.string.forgot_pin), true)
        }

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun setupIntent() {
        super.setupIntent()
        val extras = intent.extras
        if(extras != null){
            changePinMode = extras.getBoolean(KEY_CHANGE_PIN)
        }
    }

    override fun setupAction() {
        var newPin: String? = null
        pinEntryNewPin.setOnPinEnteredListener{
            if (it.length == 6){
                btnChangePinNext.isEnabled = true
                newPin = it.toString()
                hideKeyboard()
            } else {
                newPin = null
                btnChangePinNext.isEnabled = false
            }
        }

        btnChangePinNext.setOnClickListener {
            val confirmPin = Intent(this, ConfirmNewPinActivity::class.java)
            confirmPin.putExtra(KEY_PIN, newPin)
            confirmPin.putExtra(KEY_CHANGE_PIN, changePinMode)
            startActivity(confirmPin)
        }
    }

    override fun setupProcess() {
    }

    override fun onChangePinStart() {
    }

    override fun onChangePinSuccess(response: DataResult<NonBodyResponse>) {
    }

    override fun onChangePinFailed(result: DataResult.Error) {
    }
}