package id.dana.apprentech.danapulsa.ui.verifypin

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.MenuItem
import android.view.View
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.data.util.PreferencesKey.KEY_USER_PHONE
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.login.LoginActivity
import id.dana.apprentech.danapulsa.ui.main.MainActivity
import id.dana.apprentech.danapulsa.ui.otp.OtpActivity
import id.dana.apprentech.danapulsa.util.BundleKeys
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_CHANGE_PIN
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_USER_ID
import id.dana.apprentech.danapulsa.util.MessageUtils
import kotlinx.android.synthetic.main.activity_verify_pin.*
import kotlinx.android.synthetic.main.base_toolbar.*
import javax.inject.Inject

class VerifyPinActivity : BaseActivity(), VerifyPinContract {

    companion object {
        @JvmStatic
        fun start(context: Context, user: UserProfile, toMainActivity: Boolean) {
            val starter = Intent(context, VerifyPinActivity::class.java)
            starter.putExtra(BundleKeys.KEY_USER_PROFILE, user)
            starter.putExtra(BundleKeys.KEY_VERIFY_PIN_DESTINATION, toMainActivity)
            context.startActivity(starter)
        }
    }

    @Inject
    lateinit var presenter: VerifyPinPresenterContract<VerifyPinContract>

    @Inject
    lateinit var preferencesManager: PreferencesManager //TODO Create Interactor

    lateinit var userProfile: UserProfile

    var toMainActivity = true

    var isLoading = false

    override fun getLayoutResource(): Int = R.layout.activity_verify_pin

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupIntent() {
        userProfile = intent.getParcelableExtra(BundleKeys.KEY_USER_PROFILE)!!
        toMainActivity = intent.getBooleanExtra(BundleKeys.KEY_VERIFY_PIN_DESTINATION, true)
    }

    override fun setupUI() {
        if (toMainActivity) {
            setupToolbar(baseToolbar, getString(R.string.activity_sign_in), true)
        } else {
            setupToolbar(baseToolbar, getString(R.string.activity_verify_pin), false)
        }
    }

    override fun setupAction() {
        pinEntryLogin.setOnPinEnteredListener {
            if (!isLoading) presenter.verifyPin(userProfile.id, it.toString().toInt())
        }

        btnLoginForgotPin.setOnClickListener {
            val forgotPin = Intent(this, OtpActivity::class.java)
            forgotPin.putExtra(KEY_CHANGE_PIN, false)
            forgotPin.putExtra(KEY_USER_ID, userProfile.id)
            forgotPin.putExtra(KEY_USER_PHONE, userProfile.phone)
            startActivity(forgotPin)
        }
    }

    override fun setupProcess() {}

    override fun verifyPinSuccess(result: BaseResponse<Unit>) {
        if (toMainActivity) {
            MainActivity.start(this)
        } else {
            setResult(Activity.RESULT_OK)
        }
        finish()
    }

    override fun verifyPinFailed(result: DataResult.Error) {
        pinEntryLogin.text = null
        MessageUtils.showAlertDialog(
            supportFragmentManager,
            getString(R.string.dialog_error_title),
            result.message
        )
    }

    override fun verifyPinLoading() {
        hideKeyboard()
        pbVerifyPin.visibility = View.VISIBLE
    }

    override fun verifyPinFinished() {
        pbVerifyPin.visibility = View.GONE
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (!isLoading) onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (toMainActivity) {
            preferencesManager.clearPreferences() //TODO Create interactor
            LoginActivity.start(this)
            finish()
        } else {
            finish()
        }
    }
}