package id.dana.apprentech.danapulsa.ui.history

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.data.util.ext.handleSubscriberError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSubscriber
import id.dana.apprentech.danapulsa.domain.history.interactor.GetHistoryCompleted
import id.dana.apprentech.danapulsa.domain.history.interactor.GetHistoryInProgress
import id.dana.apprentech.danapulsa.domain.history.model.History
import id.dana.apprentech.danapulsa.domain.util.DataResult
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

class HistoryPresenter<V : HistoryContract> @Inject constructor(
    private val context: Context,
    private val getHistoryInProgress: GetHistoryInProgress,
    private val getHistoryCompleted: GetHistoryCompleted
) : BasePresenter<V>(), HistoryPresenterContract<V> {

    private var isLoadMore = false

    override fun getHistoryInProgress(page: Int, isLoadMore: Boolean) {
        if (isOnline(context)) {
            this.isLoadMore = isLoadMore
            getHistoryInProgress.executeFlowable(
                GetHistoryInProgressSubscriber(),
                GetHistoryInProgress.Request(page)
            )
        } else {
            getView()?.getHistoryFailed(noNetworkConnectivityError())
        }
    }

    override fun getHistoryCompleted(page: Int, isLoadMore: Boolean) {
        if (isOnline(context)) {
            this.isLoadMore = isLoadMore
            getHistoryCompleted.executeFlowable(
                GetHistoryInProgressSubscriber(),
                GetHistoryCompleted.Request(page)
            )
        } else {
            getView()?.getHistoryFailed(noNetworkConnectivityError())
        }
    }

    inner class GetHistoryInProgressSubscriber<T : DataResult<BaseResponse<List<History>>>> :
        BaseSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            if (!isLoadMore) getView()?.getHistoryLoading()
        }

        override fun onNext(response: T) {
            super.onNext(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.getHistorySuccess(response.successData as BaseResponse<List<History>>)
                }

                is DataResult.Error -> {
                    getView()?.getHistoryFailed(response)
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getHistoryFailed(handleSubscriberError(e?.message))
            getView()?.getHistoryLoadingFinished()
        }

        override fun onComplete() {
            super.onComplete()
            getView()?.getHistoryLoadingFinished()
        }
    }
}