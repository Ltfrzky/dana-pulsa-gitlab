package id.dana.apprentech.danapulsa.ui.register

import android.content.Intent
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_EMAIL
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_NAME
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_PHONE
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_PIN
import kotlinx.android.synthetic.main.activity_register_phone.btnRegisterNext
import kotlinx.android.synthetic.main.activity_register_pin.*
import kotlinx.android.synthetic.main.base_toolbar.*

class RegisterPinActivity : BaseActivity(), RegisterContract {

    override fun getLayoutResource(): Int = R.layout.activity_register_pin

    override fun setupLib() {
//        TODO("Not yet implemented")
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, getString(R.string.sign_up), true)
        setNextButtonAvailability(false)
    }

    override fun setupAction() {
        var pin: String? = null
        pinEntryRegister.setOnPinEnteredListener {
            if (it.length == 6) {
                setNextButtonAvailability(true)
                pin = it.toString()
                hideKeyboard()
            } else {
                pin = null
                setNextButtonAvailability(false)
            }
        }

        btnRegisterNext.setOnClickListener {
            val extras = intent.extras
            var fullname: String? = null
            var email: String? = null
            var phone: String? = null
            if (extras != null) {
                fullname = extras.getString(KEY_NAME)
                email = extras.getString(KEY_EMAIL)
                phone = extras.getString(KEY_PHONE)
            }
            val registerConfirmPin = Intent(this, RegisterConfirmPinActivity::class.java)
            registerConfirmPin.putExtra(KEY_NAME, fullname)
            registerConfirmPin.putExtra(KEY_EMAIL, email)
            registerConfirmPin.putExtra(KEY_PHONE, phone)
            registerConfirmPin.putExtra(KEY_PIN, pin)
            startActivity(registerConfirmPin)
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun setupProcess() {
//        TODO("Not yet implemented")
    }

    override fun setNextButtonAvailability(available: Boolean) {
        btnRegisterNext.isEnabled = available
    }

    override fun onRegisterSuccess(response: NonBodyResponse) {
    }

    override fun onRegisterFailed(response: DataResult.Error) {
    }

    override fun registerLoading() {
    }

    override fun invalidInput(message: String) {
    }
}