package id.dana.apprentech.danapulsa.ui.sample

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.domain.base.BaseSubscriber
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.sample.model.UserSample
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.sample.interactor.LoginInteractor
import id.dana.apprentech.danapulsa.domain.sample.interactor.UserDataInteractor
import javax.inject.Inject
import kotlin.Exception

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

class SamplePresenter<V : SampleContract> @Inject
internal constructor(
    private val context: Context,
    private val loginInteractor: LoginInteractor,
    private val userDataInteractor: UserDataInteractor
) : BasePresenter<V>(),
    SamplePresenterContract<V> {

    override fun postLogin(username: String, password: String) {
        if (isOnline(context)) {
            loginInteractor.executeFlowable(LoginSubscriber(), LoginInteractor.Request(username, password))
        } else {
            getView()?.loginFailed(noNetworkConnectivityError())
        }
    }

    override fun getUserData() {
        userDataInteractor.executeFlowable(object : BaseSubscriber<UserSample>() {
            override fun onNext(response: UserSample) {
                super.onNext(response)
                getView()?.getUserDataSuccess(response)
            }
        }, Empty())
    }

    override fun onDetach() {
        super.onDetach()
        loginInteractor.clearSubscription()
    }

    inner class LoginSubscriber<T : DataResult<BaseResponse<UserSample>>> : BaseSubscriber<T>() {

        override fun onStart() {
            super.onStart()
            getView()?.loginLoading()
        }

        override fun onNext(response: T) {
            super.onNext(response)
            when(response){
                is DataResult.Success<*> -> {
                    getView()?.loginSuccess(response.successData as BaseResponse<UserSample>)
                }
                is DataResult.Error -> {
                    getView()?.loginFailed(response)
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.loginFailed(DataResult.Error(Exception(e?.message)))
        }
    }

}