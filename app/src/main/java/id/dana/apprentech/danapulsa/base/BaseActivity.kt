package id.dana.apprentech.danapulsa.base

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.os.IBinder
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import id.dana.apprentech.danapulsa.custom.DialogAlert
import id.dana.apprentech.danapulsa.custom.DialogAsk
import id.dana.apprentech.danapulsa.custom.DialogAsk.Companion.DIALOG_CONTENT
import id.dana.apprentech.danapulsa.custom.DialogAsk.Companion.DIALOG_NEGATIVE_BUTTON
import id.dana.apprentech.danapulsa.custom.DialogAsk.Companion.DIALOG_POSITIVE_BUTTON
import id.dana.apprentech.danapulsa.custom.DialogAsk.Companion.DIALOG_TITLE

/**
 *created by Lutfi Rizky Ramadhan on 03/06/20
 */

abstract class BaseActivity : AppCompatActivity(),
    BaseContract {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResource())
        setupLib()
        setupIntent()
        setupUI()
        setupAction()
        setupProcess()
    }

    override fun setupToolbar(toolbar: Toolbar, isBack: Boolean) {
        setSupportActionBar(toolbar)

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(isBack)
            it.setDisplayShowTitleEnabled(false)
        }
    }

    override fun setupToolbar(toolbar: Toolbar, title: String, isBack: Boolean) {
        setSupportActionBar(toolbar)

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(isBack)
            it.title = title
        }
    }

    override fun hideKeyboard() {
        val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        var iBinder: IBinder? = null
        try {
            iBinder = currentFocus!!.windowToken
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
        if (iBinder != null) {
            inputManager.hideSoftInputFromWindow(
                iBinder,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val currentFocus = currentFocus
            if (currentFocus is EditText) {
                val outRect = Rect()
                currentFocus.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    currentFocus.clearFocus()
                    val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0)
                }
            }
        }

        return super.dispatchTouchEvent(event)
    }

    protected abstract fun getLayoutResource(): Int

    protected abstract fun setupLib()

    protected open fun setupIntent() {}

    protected abstract fun setupUI()

    protected abstract fun setupAction()

    protected abstract fun setupProcess()
}