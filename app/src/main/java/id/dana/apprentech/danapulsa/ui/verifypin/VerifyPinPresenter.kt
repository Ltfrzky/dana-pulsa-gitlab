package id.dana.apprentech.danapulsa.ui.verifypin

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.handleSubscriberError
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.BaseSubscriber
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.login.interactors.VerifyPin
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

class VerifyPinPresenter<V: VerifyPinContract>(
    private val context: Context,
    private val verifyPin: VerifyPin
) : BasePresenter<V>(), VerifyPinPresenterContract<V>{

    override fun verifyPin(userId: Int, pin: Int) {
        if(isOnline(context)){
            verifyPin.executeSingle(VerifyPinSubscriber(), VerifyPin.Request(userId, pin))
        }else{
            getView()?.verifyPinFailed(noNetworkConnectivityError())
        }
    }

    inner class VerifyPinSubscriber<T: DataResult<BaseResponse<Empty>>> : BaseSingleSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.verifyPinLoading()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when(response){
                is DataResult.Success<*> ->{
                    getView()?.verifyPinSuccess(response.successData as BaseResponse<Unit>)
                    getView()?.verifyPinFinished()
                }

                is DataResult.Error -> {
                    getView()?.verifyPinFailed(response)
                }
            }
        }

//        override fun onNext(response: T) {
//            super.onNext(response)
//            when(response){
//                is DataResult.Success<*> ->{
//                    getView()?.verifyPinSuccess(response.successData as BaseResponse<Unit>)
//                }
//
//                is DataResult.Error -> {
//                    getView()?.verifyPinFailed(response)
//                }
//            }
//        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.verifyPinFailed(noNetworkConnectivityError())
            getView()?.verifyPinFinished()
        }

//        override fun onComplete() {
//            super.onComplete()
//            getView()?.verifyPinFinished()
//        }
    }
}