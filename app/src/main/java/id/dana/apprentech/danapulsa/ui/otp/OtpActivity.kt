package id.dana.apprentech.danapulsa.ui.otp

import android.content.Intent
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import androidx.core.widget.addTextChangedListener
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.changepin.NewPinActivity
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_CHANGE_PIN
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_PHONE
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_USER_ID
import id.dana.apprentech.danapulsa.util.MessageUtils.showAlertDialog
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.android.synthetic.main.base_toolbar.*
import kotlinx.android.synthetic.main.progress_view.*
import javax.inject.Inject

class OtpActivity : BaseActivity(), OtpContract {

    @Inject
    lateinit var presenter: OtpPresenterContract<OtpContract>

    private var userId: Int? = null
    private var userPhone: String? = null

    private var changePinMode = true
    private lateinit var timer: CountDownTimer

    override fun getLayoutResource(): Int = R.layout.activity_otp

    override fun setupIntent() {
        super.setupIntent()
        val bundle = intent.extras
        if (bundle != null) {
            changePinMode = bundle.getBoolean(KEY_CHANGE_PIN)
        }
        userId = bundle?.getInt(KEY_USER_ID)
        userPhone = bundle?.getString(KEY_PHONE)
    }

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupUI() {
        progressLayout.visibility = View.GONE
        btnChangePinNext.isEnabled = false
        btnResendOtp.isEnabled = false
        tvOtpConfirmPhone.text = "..."
        if (changePinMode) {
            presenter.requestChangePinOtp()
            setupToolbar(baseToolbar, "Change PIN", true)
        } else {
            userId?.let { presenter.requestForgotPinOtp(it) }
            setupToolbar(baseToolbar, "Forgot PIN", true)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun setupAction() {
        var otpCode: Int? = null
        otpPIN.addTextChangedListener {
            when (it?.length) {
                4 -> btnChangePinNext.isEnabled = true
                else -> btnChangePinNext.isEnabled = false
            }
        }

        otpPIN.setOtpCompletionListener {
            otpCode = it.toInt()
            hideKeyboard()
        }

        btnChangePinNext.setOnClickListener {
            if (userId != null && otpCode != null) {
                presenter.doOtpVerify(userId!!, otpCode!!)
            }
        }

        btnResendOtp.setOnClickListener {
            userId?.let { id -> presenter.resendOtp(id.toString()) }
        }
    }

    override fun setupProcess() {
    }

    override fun notifyOtpSent(sent: Boolean) {
        progressLayout.visibility = View.GONE
        Log.d("State", "userphone: $userPhone")
        tvOtpConfirmPhone.text = userPhone
    }

    override fun onFailedRequestOtp(response: DataResult.Error) {
        showAlertDialog(supportFragmentManager, getString(R.string.whoops), response.message)
    }

    override fun onVerifyOtp() {
        progressLayout.visibility = View.VISIBLE
    }

    override fun onGetOtp() {
        progressLayout.visibility = View.VISIBLE
    }

    override fun onOtpVerified(response: DataResult<NonBodyResponse>) {
        progressLayout.visibility = View.GONE
        val newPin = Intent(this, NewPinActivity::class.java)
        newPin.putExtra(KEY_CHANGE_PIN, changePinMode)
        startActivity(newPin)
        finish()
    }

    override fun onFailedVerify(response: DataResult.Error) {
        showAlertDialog(supportFragmentManager, getString(R.string.whoops), response.message)
        progressLayout.visibility = View.GONE
    }

    override fun getProfile(result: UserProfile) {

    }

    override fun getProfileFailed(response: DataResult.Error) {

    }

    override fun startCountDown() {
        timer = object : CountDownTimer(180000, 1000) {
            override fun onFinish() {
                btnResendOtp.visibility = View.VISIBLE
                tvCountDown.visibility = View.GONE
                btnResendOtp.text = getString(R.string.resend)
                btnResendOtp.isEnabled = true
            }

            override fun onTick(times: Long) {
                val second = (times / 1000) % 60
                val minute = times / (1000 * 60) % 60
                val countDown = "${getString(R.string.you_can_resend)} $minute : $second"

                btnResendOtp.visibility = View.GONE
                tvCountDown.visibility = View.VISIBLE
                tvCountDown.text = countDown
            }
        }
        timer.start()
    }
}