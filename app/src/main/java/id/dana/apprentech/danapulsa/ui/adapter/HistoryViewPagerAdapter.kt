package id.dana.apprentech.danapulsa.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import id.dana.apprentech.danapulsa.ui.history.HistoryFragment

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

class HistoryViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    companion object {
        const val HISTORY_IN_PROGRESS = 0
    }

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return if (position == HISTORY_IN_PROGRESS) {
            HistoryFragment.newInstance(false)
        } else {
            HistoryFragment.newInstance(true)
        }
    }
}