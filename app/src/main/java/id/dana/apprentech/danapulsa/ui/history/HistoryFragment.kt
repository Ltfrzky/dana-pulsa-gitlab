package id.dana.apprentech.danapulsa.ui.history

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseFragment
import id.dana.apprentech.danapulsa.base.adapter.BaseRecyclerAdapter
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.history.model.History
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.adapter.HistoryAdapter
import id.dana.apprentech.danapulsa.ui.transactiondetail.TransactionDetailActivity
import id.dana.apprentech.danapulsa.ui.payment.PaymentActivity
import id.dana.apprentech.danapulsa.util.BundleKeys
import kotlinx.android.synthetic.main.fragment_history.*
import javax.inject.Inject


class HistoryFragment : BaseFragment(), HistoryContract {

    companion object {
        fun newInstance(isCompletedHistory: Boolean): HistoryFragment {
            val args = Bundle()
            args.putBoolean(BundleKeys.KEY_HISTORY_TYPE, isCompletedHistory)

            val fragment = HistoryFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var presenter: HistoryPresenterContract<HistoryContract>

    private var isCompletedHistory: Boolean = false

    private var page = 1

    lateinit var historyAdapter: HistoryAdapter

    override fun getLayoutResource(): Int = R.layout.fragment_history

    override fun setupLib() {
        Injector.obtain(requireContext())?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupIntent(savedInstanceState: Bundle?) {
        isCompletedHistory = arguments?.getBoolean(BundleKeys.KEY_HISTORY_TYPE, false)!!
    }

    override fun setupUI() {
        historyAdapter = HistoryAdapter(requireContext(), ArrayList())
        rvHistory.layoutManager = LinearLayoutManager(requireContext())
        rvHistory.adapter = historyAdapter
    }

    override fun setupAction() {
        historyAdapter.setOnItemClickListener(object : BaseRecyclerAdapter.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                if(isCompletedHistory){
                    TransactionDetailActivity.start(requireContext(), historyAdapter.data[position].id.toString())
                }else{
                    PaymentActivity.start(requireContext(), historyAdapter.data[position].id.toString())
                    requireActivity().finish()
                }
            }
        })

        srlHistory.setOnRefreshListener {
            srlHistory.isRefreshing = false
        }
    }

    override fun setupProcess() {
        if (isCompletedHistory) {
            presenter.getHistoryCompleted(page, false)
        } else {
            presenter.getHistoryInProgress(page, false)
        }
    }

    override fun getHistorySuccess(result: BaseResponse<List<History>>) {
        if (result.data.isNotEmpty()) {
            historyAdapter.clearData()
            historyAdapter.addOrUpdateItem(result.data)
        } else {
            historyAdapter.clearData()
            tvLoadMessage.visibility = View.VISIBLE
            tvLoadMessage.text = if (isCompletedHistory) {
                getString(R.string.get_history_completed_empty)
            } else {
                getString(R.string.get_history_in_progress_empty)
            }
        }
    }

    override fun getHistoryFailed(result: DataResult.Error) {
        tvLoadMessage.visibility = View.VISIBLE
        tvLoadMessage.text = getString(R.string.get_history_error_message)
    }

    override fun getHistoryLoading() {
        pbHistory.visibility = View.VISIBLE
        rvHistory.visibility = View.GONE
        tvLoadMessage.visibility = View.GONE
        tvLoadMessage.text = null
    }

    override fun getHistoryLoadingFinished() {
        pbHistory.visibility = View.GONE
        rvHistory.visibility = View.VISIBLE
    }
}