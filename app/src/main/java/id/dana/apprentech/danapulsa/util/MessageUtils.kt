package id.dana.apprentech.danapulsa.util

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import id.dana.apprentech.danapulsa.custom.DialogAlert
import id.dana.apprentech.danapulsa.custom.DialogAsk

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

object MessageUtils {

    fun showMessage(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    fun showAlertDialog(fragmentManager: FragmentManager, title: String, content: String) {
        val dialog = DialogAlert(null)
        val bundle = Bundle()

        bundle.putString(DialogAlert.DIALOG_TITLE, title)
        bundle.putString(DialogAlert.DIALOG_CONTENT, content)
        dialog.arguments = bundle

        val fragmentTransaction = fragmentManager.beginTransaction()
        val previousDialog = fragmentManager.findFragmentByTag("dialog")
        if (previousDialog != null) {
            fragmentTransaction.remove(previousDialog)
        }

        fragmentTransaction.addToBackStack(null)
        dialog.show(fragmentTransaction, "dialog")
    }

    fun showAlertDialog(
        fragmentManager: FragmentManager,
        title: String,
        content: String,
        positiveAction: DialogAlert.DialogListener
    ) {
        val dialog = DialogAlert(positiveAction)
        val bundle = Bundle()

        bundle.putString(DialogAlert.DIALOG_TITLE, title)
        bundle.putString(DialogAlert.DIALOG_CONTENT, content)
        dialog.isCancelable = false
        dialog.arguments = bundle

        val fragmentTransaction = fragmentManager.beginTransaction()
        val previousDialog = fragmentManager.findFragmentByTag("dialog")
        if (previousDialog != null) {
            fragmentTransaction.remove(previousDialog)
        }

        fragmentTransaction.addToBackStack(null)
        dialog.show(fragmentTransaction, "dialog")
    }

    fun showAskDialog(
        fragmentManager: FragmentManager,
        title: String,
        content: String,
        positiveAction: DialogAsk.DialogListener
    ) {
        val dialog = DialogAsk(positiveAction)
        val bundle = Bundle()

        bundle.putString(DialogAsk.DIALOG_TITLE, title)
        bundle.putString(DialogAsk.DIALOG_CONTENT, content)
        dialog.arguments = bundle

        val fragmentTransaction = fragmentManager.beginTransaction()
        val previousDialog = fragmentManager.findFragmentByTag("dialog")
        if (previousDialog != null) {
            fragmentTransaction.remove(previousDialog)
        }

        fragmentTransaction.addToBackStack(null)
        dialog.show(fragmentTransaction, "dialog")
    }

    fun showAskDialog(
        fragmentManager: FragmentManager,
        title: String,
        content: String,
        positiveButton: String,
        negativeButton: String
    ) {
        val dialog = DialogAsk(null)
        val bundle = Bundle()

        bundle.putString(DialogAsk.DIALOG_TITLE, title)
        bundle.putString(DialogAsk.DIALOG_CONTENT, content)
        bundle.putString(DialogAsk.DIALOG_NEGATIVE_BUTTON, negativeButton)
        bundle.putString(DialogAsk.DIALOG_POSITIVE_BUTTON, positiveButton)
        dialog.arguments = bundle

        val fragmentTransaction = fragmentManager.beginTransaction()
        val previousDialog = fragmentManager.findFragmentByTag("dialog")
        if (previousDialog != null) {
            fragmentTransaction.remove(previousDialog)
        }
        fragmentTransaction.addToBackStack(null)
        dialog.show(fragmentTransaction, "dialog")
    }

    fun showAskDialog(
        fragmentManager: FragmentManager,
        title: String,
        content: String,
        positiveButton: String,
        negativeButton: String,
        positiveAction: DialogAsk.DialogListener
    ) {
        val dialog = DialogAsk(positiveAction)
        val bundle = Bundle()
        bundle.putString(DialogAsk.DIALOG_TITLE, title)
        bundle.putString(DialogAsk.DIALOG_CONTENT, content)
        bundle.putString(DialogAsk.DIALOG_NEGATIVE_BUTTON, negativeButton)
        bundle.putString(DialogAsk.DIALOG_POSITIVE_BUTTON, positiveButton)

        dialog.arguments = bundle
        val fragmentTransaction = fragmentManager.beginTransaction()
        val previousDialog = fragmentManager.findFragmentByTag("dialog")
        if (previousDialog != null) {
            fragmentTransaction.remove(previousDialog)
        }
        fragmentTransaction.addToBackStack(null)
        dialog.show(fragmentTransaction, "dialog")
    }
}