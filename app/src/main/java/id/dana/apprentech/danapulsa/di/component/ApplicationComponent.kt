package id.dana.apprentech.danapulsa.di.component

import id.dana.apprentech.danapulsa.BaseApplication
import id.dana.apprentech.danapulsa.di.module.ApplicationModule
import id.dana.apprentech.danapulsa.ui.main.MainActivity
import id.dana.apprentech.danapulsa.ui.sample.SampleActivity
import dagger.Component
import id.dana.apprentech.danapulsa.di.module.feature.LoginModule
import id.dana.apprentech.danapulsa.di.module.feature.MobileRechargeModule
import id.dana.apprentech.danapulsa.di.module.feature.RegisterModule
import id.dana.apprentech.danapulsa.di.module.feature.*
import id.dana.apprentech.danapulsa.ui.home.HomeFragment
import id.dana.apprentech.danapulsa.ui.login.LoginActivity
import id.dana.apprentech.danapulsa.ui.mobilerecharge.MobileRechargeActivity
import id.dana.apprentech.danapulsa.ui.register.RegisterConfirmPinActivity
import id.dana.apprentech.danapulsa.di.module.feature.SplashModule
import id.dana.apprentech.danapulsa.ui.payment.PaymentActivity
import id.dana.apprentech.danapulsa.ui.changepin.ConfirmNewPinActivity
import id.dana.apprentech.danapulsa.ui.history.HistoryFragment
import id.dana.apprentech.danapulsa.ui.otp.OtpActivity
import id.dana.apprentech.danapulsa.ui.profile.ProfileFragment
import id.dana.apprentech.danapulsa.ui.promotion.list.PromotionActivity
import id.dana.apprentech.danapulsa.ui.promotion.detail.PromotionDetailActivity
import id.dana.apprentech.danapulsa.ui.splash.SplashActivity
import id.dana.apprentech.danapulsa.ui.transactiondetail.TransactionDetailActivity
import id.dana.apprentech.danapulsa.ui.verifypin.VerifyPinActivity
import javax.inject.Singleton

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */
@Singleton
@Component(modules = [ApplicationModule::class, SampleModule::class,
    MobileRechargeModule::class,
    RegisterModule::class,
    SplashModule::class,
    LoginModule::class,
    HomeModule::class,
    PaymentModule::class,
    ProfileModule::class,
    ChangePinModule::class,
    VoucherModule::class,
    ChangePinModule::class,
    HistoryModule::class
])
interface ApplicationComponent {
    fun inject(baseApplication: BaseApplication)

    fun inject(mainActivity: MainActivity)

    fun inject(sampleActivity: SampleActivity)

    fun inject(mobileRechargeActivity: MobileRechargeActivity)

    fun inject(registerActivity: RegisterConfirmPinActivity)

    fun inject(loginActivity: LoginActivity)

    fun inject(verifyPinActivity: VerifyPinActivity)

    fun inject(splashActivity: SplashActivity)

    fun inject(homeFragment: HomeFragment)

    fun inject(paymentActivity: PaymentActivity)

    fun inject(profileFragment: ProfileFragment)

    fun inject(otpActivity: OtpActivity)

    fun inject(changePinActivity: ConfirmNewPinActivity)

    fun inject(promotionActivity: PromotionActivity)

    fun inject(promotionDetailActivity: PromotionDetailActivity)

    fun inject(historyFragment: HistoryFragment)

    fun inject(transactionDetailActivity: TransactionDetailActivity)
}