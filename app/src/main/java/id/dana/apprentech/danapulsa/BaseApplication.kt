package id.dana.apprentech.danapulsa

import android.content.Context
import androidx.multidex.MultiDexApplication
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.di.component.ApplicationComponent
import id.dana.apprentech.danapulsa.di.component.DaggerApplicationComponent
import id.dana.apprentech.danapulsa.di.module.ApplicationModule

/**
 *created by Lutfi Rizky Ramadhan on 03/06/20
 */

class BaseApplication : MultiDexApplication() {

    companion object {
        operator fun get(context: Context): BaseApplication {
            return context.applicationContext as BaseApplication
        }
    }

    lateinit var injector: ApplicationComponent private set

    override fun onCreate() {
        super.onCreate()
        injector = Injector.create(this)
        injector.inject(this)
        DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this)).build().inject(this)
    }
}