package id.dana.apprentech.danapulsa.util

import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

object CommonUtils {
    val localeID = Locale("in", "ID")

    fun formatPhoneNumber(value: String): String {
        if (value.length > 12) {
            return "${value.subSequence(0..4)} ${value.subSequence(5..8)} ${value.subSequence(9..12)}"
        }
        return value.chunked(4).joinToString(" ")
    }

    fun formatNumber(value: Int): String {
        val formatter: NumberFormat = NumberFormat.getNumberInstance(localeID)
        return formatter.format(value)
    }

    fun formatNumber(value: Float): String {
        val formatter: NumberFormat = NumberFormat.getNumberInstance(localeID)
        return formatter.format(value)
    }

    fun formatCurrency(value: Int): String {
        val formatter: NumberFormat = NumberFormat.getCurrencyInstance(localeID)
        formatter.maximumFractionDigits = 0
        return formatter.format(value)
    }

    fun formatCurrency(value: Float): String {
        val formatter: NumberFormat = NumberFormat.getCurrencyInstance(localeID)
        formatter.maximumFractionDigits = 0
        return formatter.format(value)
    }

    fun formatDisplayPhone( number: String?): String{
        val new = number?.replaceRange(0, 2, "0")
        return new.toString()
    }

    fun capitalizeName( name: String): String {
        return name.split(" ").joinToString(" ") { it.capitalize() }
    }

    fun getHumanReadableDateTime(millis: String): String {
        val formatter = SimpleDateFormat("dd MMMM yyyy - HH:mm:ss", localeID)
        val date: Calendar = Calendar.getInstance()
        date.timeInMillis = millis.toLong()
        return formatter.format(date.time)
    }
}