package id.dana.apprentech.danapulsa.ui.changepin

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.SingleUseCase
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.changepin.interactor.ChangePin
import id.dana.apprentech.danapulsa.domain.util.DataResult
import java.lang.Exception
import javax.inject.Inject

class ChangePinPresenter<V : ChangePinContract> @Inject constructor(
    private val context: Context,
    private val changePin: ChangePin
) : BasePresenter<V>(), ChangePinPresenterContract<V> {
    override fun postNewPin(newPin: Int) {
        if (isOnline(context)) {
            changePin.executeSingle(ChangePinSubscriber(), changePin.Request(newPin))
        } else {
            getView()?.onChangePinFailed(noNetworkConnectivityError())
        }
    }

    inner class ChangePinSubscriber<T : DataResult<NonBodyResponse>>
        : BaseSingleSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.onChangePinStart()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.onChangePinSuccess(response)
                }
                is DataResult.Error -> {
                    getView()?.onChangePinFailed(response)
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.onChangePinFailed(DataResult.Error(Exception(e?.message)))
        }
    }
}