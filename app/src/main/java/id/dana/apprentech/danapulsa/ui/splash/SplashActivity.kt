package id.dana.apprentech.danapulsa.ui.splash

import android.os.Handler
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.login.interactors.Login
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.login.LoginActivity
import id.dana.apprentech.danapulsa.ui.main.MainActivity
import id.dana.apprentech.danapulsa.ui.verifypin.VerifyPinActivity
import id.dana.apprentech.danapulsa.util.MessageUtils
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashContract {

    @Inject
    lateinit var presenter: SplashPresenterContract<SplashContract>

    lateinit var userProfile: UserProfile

    override fun getLayoutResource(): Int = R.layout.activity_splash

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupUI() {}

    override fun setupAction() {}

    override fun setupProcess() {
        presenter.getUserProfileLocal()
        presenter.checkSession()
    }

    override fun getUserProfile(result: UserProfile) {
        userProfile = result
    }

    override fun checkSessionSuccess(result: BaseResponse<UserProfile>) {
        MessageUtils.showMessage(this, result.message)
        MainActivity.start(this)
        this.finish()
    }

    override fun checkSessionFailed(result: DataResult.Error) {
        MessageUtils.showMessage(this, "${result.code} - ${result.message}")
        if (userProfile.id == 0) {
            LoginActivity.start(this)
            this.finish()
        } else {
            VerifyPinActivity.start(this, userProfile, true)
            this.finish()
        }
    }
}