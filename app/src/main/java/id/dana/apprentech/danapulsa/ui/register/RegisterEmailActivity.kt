package id.dana.apprentech.danapulsa.ui.register

import android.content.Intent
import androidx.core.widget.addTextChangedListener
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_EMAIL
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_NAME
import id.dana.apprentech.danapulsa.util.PatternRegex
import kotlinx.android.synthetic.main.activity_register_email.*
import kotlinx.android.synthetic.main.activity_register_name.btnRegisterNext
import kotlinx.android.synthetic.main.base_toolbar.*
import java.util.regex.Pattern

class RegisterEmailActivity : BaseActivity(), RegisterContract {

    override fun getLayoutResource(): Int = R.layout.activity_register_email

    override fun setupLib() {
//        TODO("Not yet implemented")
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, getString(R.string.sign_up), true)
        setNextButtonAvailability(false)
    }

    override fun setupAction() {
        edtRegisterEmail.addTextChangedListener {
            when {
                it.isNullOrEmpty() -> {
                    setNextButtonAvailability(false)
                    layoutEdtRegisterEmail.error = null
                }
                Pattern.matches(PatternRegex.EMAIL_PATTERN, it) -> {
                    if (filterValidEmail(it.toString())) {
                        setNextButtonAvailability(true)
                        layoutEdtRegisterEmail.error = null
                    }
                }
                else -> {
                    setNextButtonAvailability(false)
                    invalidInput(getString(R.string.invalid_register_email))
                }
            }
        }

        btnRegisterNext.setOnClickListener {
            val extras = intent.extras
            var fullname: String? = null
            val registerPhone = Intent(this, RegisterPhoneActivity::class.java)
            if (extras != null) {
                fullname = extras.getString(KEY_NAME)
            }
            registerPhone.putExtra(KEY_NAME, fullname)
            registerPhone.putExtra(KEY_EMAIL, edtRegisterEmail.text.toString())
            startActivity(registerPhone)
        }
    }

    private fun filterValidEmail(email: String): Boolean {
        return when {
            email.takeLast(3).contentEquals(".id") -> true
            email.takeLast(3).contentEquals("com") -> true
            email.takeLast(3).contentEquals(".co") -> true
            else -> false
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun setupProcess() {
//        TODO("Not yet implemented")
    }

    override fun setNextButtonAvailability(available: Boolean) {
        btnRegisterNext.isEnabled = available
    }

    override fun onRegisterSuccess(response: NonBodyResponse) {
//        TODO("Not yet implemented")
    }

    override fun onRegisterFailed(response: DataResult.Error) {
//        TODO("Not yet implemented")
    }

    override fun registerLoading() {
//        TODO("Not yet implemented")
    }

    override fun invalidInput(message: String) {
        layoutEdtRegisterEmail.error = message
    }
}