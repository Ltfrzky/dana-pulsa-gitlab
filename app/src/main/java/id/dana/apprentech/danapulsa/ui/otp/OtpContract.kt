package id.dana.apprentech.danapulsa.ui.otp

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult

interface OtpContract: BaseContract {
    fun notifyOtpSent(sent: Boolean)

    fun onFailedRequestOtp(response: DataResult.Error)

    fun onVerifyOtp()

    fun onGetOtp()

    fun onOtpVerified(response: DataResult<NonBodyResponse>)

    fun onFailedVerify(response: DataResult.Error)

    fun getProfile(result: UserProfile)

    fun getProfileFailed(response: DataResult.Error)

    fun startCountDown()
}