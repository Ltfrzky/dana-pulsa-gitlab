package id.dana.apprentech.danapulsa.ui.promotion.list

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSubscriber
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.interactor.GetPromotion
import id.dana.apprentech.danapulsa.domain.voucher.model.Promotion
import java.lang.Exception
import javax.inject.Inject

class PromotionPresenter<V : PromotionContract> @Inject constructor(
    private val context: Context,
    private val getPromotion: GetPromotion
) : BasePresenter<V>(),
    PromotionPresenterContract<V> {
    override fun getPromotions(page: String) {
        if (isOnline(context)) {
            getPromotion.executeFlowable(PromotionSubscriber(), getPromotion.Request(page))
        } else {
            getView()?.getPromotionFailed(noNetworkConnectivityError())
        }
    }

    inner class PromotionSubscriber<T : DataResult<BaseResponse<List<Promotion>>>>
        : BaseSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.onLoadPromotion()
        }

        override fun onNext(response: T) {
            super.onNext(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.getPromotionSuccess(response.successData as BaseResponse<List<Promotion>>)
                }
                is DataResult.Error -> {
                    getView()?.getPromotionFailed(response)
                }
            }
        }

        override fun onComplete() {
            super.onComplete()
            getView()?.getPromotionFinished()
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getPromotionFailed(DataResult.Error(Exception(e?.message)))
        }
    }
}