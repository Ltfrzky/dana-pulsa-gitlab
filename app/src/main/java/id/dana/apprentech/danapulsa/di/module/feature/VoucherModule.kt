package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.voucher.VoucherRepositoryImpl
import id.dana.apprentech.danapulsa.data.voucher.network.VoucherApi
import id.dana.apprentech.danapulsa.domain.voucher.interactor.GetPromotion
import id.dana.apprentech.danapulsa.domain.voucher.interactor.GetVoucherDetail
import id.dana.apprentech.danapulsa.domain.voucher.repository.VoucherRepository
import id.dana.apprentech.danapulsa.ui.promotion.detail.PromotionDetailContract
import id.dana.apprentech.danapulsa.ui.promotion.detail.PromotionDetailPresenter
import id.dana.apprentech.danapulsa.ui.promotion.detail.PromotionDetailPresenterContract
import id.dana.apprentech.danapulsa.ui.promotion.list.PromotionContract
import id.dana.apprentech.danapulsa.ui.promotion.list.PromotionPresenter
import id.dana.apprentech.danapulsa.ui.promotion.list.PromotionPresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class VoucherModule {
    @Provides
    @Singleton
    fun provideVoucherApi(retrofit: Retrofit): VoucherApi =
        retrofit.create(VoucherApi::class.java)

    @Provides
    @Singleton
    fun provideVoucherRepository(
        voucherApi: VoucherApi
    ): VoucherRepository = VoucherRepositoryImpl(voucherApi)

    @Provides
    @Singleton
    fun provideGetPromotionInteractor(
        repository: VoucherRepository,
        compositeDisposable: CompositeDisposable
    ): GetPromotion = GetPromotion(repository, compositeDisposable)

    @Provides
    @Singleton
    fun provideGetVoucherDetailInteractor(
        repository: VoucherRepository,
        compositeDisposable: CompositeDisposable
    ): GetVoucherDetail = GetVoucherDetail(repository, compositeDisposable)

    @Provides
    @Singleton
    fun providePromotionPresenter(
        context: Context,
        getPromotion: GetPromotion
    ): PromotionPresenterContract<PromotionContract> =
        PromotionPresenter(
            context,
            getPromotion
        )

    @Provides
    @Singleton
    fun providePromotionDetailPresenter(
        context: Context,
        getVoucherDetail: GetVoucherDetail
    ): PromotionDetailPresenterContract<PromotionDetailContract> =
        PromotionDetailPresenter(context, getVoucherDetail)

}