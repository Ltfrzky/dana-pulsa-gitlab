package id.dana.apprentech.danapulsa.base.adapter

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 *created by Lutfi Rizky Ramadhan on 08/06/20
 */

abstract class BaseViewHolder<Data>(
    protected val context: Context,
    itemView: View,
    private val itemClickListener: BaseRecyclerAdapter.OnItemClickListener?,
    private val longItemClickListener: BaseRecyclerAdapter.OnLongItemClickListener?
) : RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {

    init {
        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
    }

    var hasHeader = false

    abstract fun bind(data: Data)

    override fun onClick(v: View?) {
        itemClickListener?.onItemClick(v, if (hasHeader) adapterPosition - 1 else adapterPosition)
    }

    override fun onLongClick(v: View?): Boolean {
        return if (longItemClickListener != null) {
            longItemClickListener.onLongItemClick(v, if (hasHeader) adapterPosition - 1 else adapterPosition)
            true
        } else {
            false
        }
    }

}