package id.dana.apprentech.danapulsa.custom

import android.content.Context
import android.os.Bundle
import android.os.Message
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import id.dana.apprentech.danapulsa.R
import kotlinx.android.synthetic.main.fragment_dialog_ask.*

class DialogAsk(private val positiveAction: DialogListener?) : DialogFragment() {

    companion object {
        const val DIALOG_TITLE = "title"
        const val DIALOG_CONTENT = "content"
        const val DIALOG_POSITIVE_BUTTON = "posbutton"
        const val DIALOG_NEGATIVE_BUTTON = "negbutton"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dialog_ask, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvDialogTitle.text = arguments?.getString(DIALOG_TITLE)
        tvDialogContent.text = arguments?.getString(DIALOG_CONTENT)
        btnDialogNegative.text = arguments?.getString(DIALOG_NEGATIVE_BUTTON) ?: "No"
        btnDialogPositive.text = arguments?.getString(DIALOG_POSITIVE_BUTTON) ?: "Yes"
        btnDialogNegative.setOnClickListener {
            dismiss()
        }
        btnDialogPositive.setOnClickListener {
            if (positiveAction != null) {
                positiveAction.onPositiveAskDialog()
            } else {
                val dialogListener = activity as DialogListener
                dialogListener.onPositiveAskDialog()
            }
            dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        val params = dialog?.window?.attributes
        params?.width = ViewGroup.LayoutParams.MATCH_PARENT
        params?.height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.attributes = params
    }

    interface DialogListener {
        fun onPositiveAskDialog()
    }
}