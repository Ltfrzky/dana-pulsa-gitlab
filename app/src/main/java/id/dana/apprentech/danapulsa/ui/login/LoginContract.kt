package id.dana.apprentech.danapulsa.ui.login

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface LoginContract: BaseContract {

    fun loginSuccess(result: BaseResponse<UserProfile>)

    fun loginFailed(result: DataResult.Error)

    fun loginLoading()

    fun loginFinished()
}