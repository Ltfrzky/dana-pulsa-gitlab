package id.dana.apprentech.danapulsa.ui.promotion.detail

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher

interface PromotionDetailContract: BaseContract {
    fun onLoadingPromotion()

    fun onGetPromotionDetailSuccess(response: BaseResponse<Voucher>)

    fun onGetPromotionDetailFailed(response: DataResult.Error)
}