package id.dana.apprentech.danapulsa.ui.history

import android.os.Bundle
import com.google.android.material.tabs.TabLayoutMediator
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseFragment
import id.dana.apprentech.danapulsa.ui.adapter.HistoryViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_view_history.*

class ViewHistoryFragment : BaseFragment() {

    companion object {
        fun newInstance(): ViewHistoryFragment {
            val args = Bundle()

            val fragment = ViewHistoryFragment()
            fragment.arguments = args
            return fragment
        }
    }

    lateinit var historyViewPagerAdapter: HistoryViewPagerAdapter

    override fun getLayoutResource(): Int = R.layout.fragment_view_history

    override fun setupLib() {}

    override fun setupUI() {
        historyViewPagerAdapter = HistoryViewPagerAdapter(childFragmentManager, lifecycle)
        vpHistory.adapter = historyViewPagerAdapter
        vpHistory.requestDisallowInterceptTouchEvent(true)

        TabLayoutMediator(tabHistory, vpHistory) { tab, position ->
            when(position){
                0 -> tab.text = "In Progress"
                1 -> tab.text = "Completed"
            }
        }.attach()
    }

    override fun setupAction() {}

    override fun setupProcess() {}
}