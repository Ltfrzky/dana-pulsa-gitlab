package id.dana.apprentech.danapulsa.ui.login

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.view.View
import androidx.core.widget.addTextChangedListener
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.ui.register.RegisterNameActivity
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.verifypin.VerifyPinActivity
import id.dana.apprentech.danapulsa.util.MessageUtils
import id.dana.apprentech.danapulsa.util.PatternRegex
import kotlinx.android.synthetic.main.activity_login.*
import java.util.regex.Pattern
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginContract {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, LoginActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Inject
    lateinit var presenter: LoginPresenterContract<LoginContract>

    override fun getLayoutResource(): Int = R.layout.activity_login

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupUI() {}

    override fun setupAction() {
        edtLoginPhone.addTextChangedListener {
            var errorMessage: String? = null
            when {
                it.isNullOrEmpty() -> {
                    btnLoginSignIn.isEnabled = false
                    errorMessage = "Please input phone number"
                }

                Pattern.matches(PatternRegex.PHONE_PATTERN, it) -> {
                    btnLoginSignIn.isEnabled = true
                    errorMessage = null
                }

                else -> {
                    btnLoginSignIn.isEnabled = false
                    errorMessage = "Invalid phone number"
                }
            }
            tilLoginPhone.error = errorMessage
        }

        btnLoginSignIn.setOnClickListener {
            hideKeyboard()
            presenter.login(edtLoginPhone.text.toString())
        }

        btnLoginSignUp.setOnClickListener{
            startActivity(Intent(this, RegisterNameActivity::class.java))
        }
    }

    override fun setupProcess() {}

    override fun loginSuccess(result: BaseResponse<UserProfile>) {
        VerifyPinActivity.start(this, result.data, true)
        finish()
    }

    override fun loginFailed(result: DataResult.Error) {
        MessageUtils.showAlertDialog(supportFragmentManager, getString(R.string.dialog_error_title), result.message)
        loginFinished()
    }

    override fun loginLoading() {
        pbLogin.visibility = View.VISIBLE
        btnLoginSignIn.isEnabled = false
        btnLoginSignUp.isEnabled = false
        btnLoginSignIn.text = null
    }

    override fun loginFinished() {
        pbLogin.visibility = View.GONE
        btnLoginSignIn.isEnabled = true
        btnLoginSignUp.isEnabled = true
        btnLoginSignIn.text = getString(R.string.sign_in)
    }

}