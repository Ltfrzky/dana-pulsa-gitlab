package id.dana.apprentech.danapulsa.ui.register

import android.content.Intent
import androidx.core.widget.addTextChangedListener
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.util.BundleKeys
import id.dana.apprentech.danapulsa.util.PatternRegex
import kotlinx.android.synthetic.main.activity_register_name.*
import kotlinx.android.synthetic.main.base_toolbar.*
import java.util.regex.Pattern

class RegisterNameActivity : BaseActivity(), RegisterContract {

    companion object {

    }

    override fun getLayoutResource(): Int = R.layout.activity_register_name

    override fun setupLib() {
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, getString(R.string.sign_up), true)
        setNextButtonAvailability(false)
    }

    override fun setupAction() {
        edtRegisterFullName.addTextChangedListener {
            when {
                it.isNullOrEmpty() -> {
                    setNextButtonAvailability(false)
                    invalidInput(getString(R.string.empty_register_name))
                    layoutEdtRegisterFullName.requestFocus()
                }
                Pattern.matches(PatternRegex.NAME_PATTERN, it) -> {
                    layoutEdtRegisterFullName.error = null
                    setNextButtonAvailability(true)
                }
                else -> {
                    setNextButtonAvailability(false)
                    invalidInput(getString(R.string.invalid_register_name))
                }
            }
        }

        btnRegisterNext.setOnClickListener {
            val intent = Intent(this, RegisterEmailActivity::class.java)
            intent.putExtra(BundleKeys.KEY_NAME, edtRegisterFullName.text.toString())
            startActivity(intent)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun setupProcess() {
    }

    override fun setNextButtonAvailability(available: Boolean) {
        btnRegisterNext.isEnabled = available
    }

    override fun onRegisterSuccess(response: NonBodyResponse) {
//        TODO("Not yet implemented")
    }

    override fun onRegisterFailed(response: DataResult.Error) {
//        TODO("Not yet implemented")
    }

    override fun registerLoading() {
//        TODO("Not yet implemented")
    }

    override fun invalidInput(message: String) {
        layoutEdtRegisterFullName.error = message
    }
}