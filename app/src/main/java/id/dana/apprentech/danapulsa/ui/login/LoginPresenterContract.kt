package id.dana.apprentech.danapulsa.ui.login

import id.dana.apprentech.danapulsa.base.BasePresenterContract

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface LoginPresenterContract<V : LoginContract> : BasePresenterContract<V> {
    fun login(username: String)
}