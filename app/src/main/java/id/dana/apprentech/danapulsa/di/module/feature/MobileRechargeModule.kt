package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.mobilerecharge.MobileRechargeRepositoryImpl
import id.dana.apprentech.danapulsa.data.mobilerecharge.network.MobileRechargeApi
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.GetProviderCatalog
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.GetRecentNumbers
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.OrderPulsa
import id.dana.apprentech.danapulsa.domain.mobilerecharge.repository.MobileRechargeRepository
import id.dana.apprentech.danapulsa.ui.mobilerecharge.MobileRechargeContract
import id.dana.apprentech.danapulsa.ui.mobilerecharge.MobileRechargePresenter
import id.dana.apprentech.danapulsa.ui.mobilerecharge.MobileRechargePresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */
@Module
class MobileRechargeModule {

    @Provides
    @Singleton
    fun provideMobileRechargeApi(retrofit: Retrofit): MobileRechargeApi =
        retrofit.create(MobileRechargeApi::class.java)

    @Provides
    fun provideMobileRechargeRepository(
        mobileRechargeApi: MobileRechargeApi,
        preferencesManager: PreferencesManager
    ): MobileRechargeRepository =
        MobileRechargeRepositoryImpl(mobileRechargeApi, preferencesManager)

    @Provides
    fun provideGetRecentNumber(
        repository: MobileRechargeRepository,
        compositeDisposable: CompositeDisposable
    ): GetRecentNumbers = GetRecentNumbers(repository, compositeDisposable)

    @Provides
    fun provideGetProviderCatalog(
        repository: MobileRechargeRepository,
        compositeDisposable: CompositeDisposable
    ): GetProviderCatalog = GetProviderCatalog(repository, compositeDisposable)

    @Provides
    fun provideOrderPulsa(
        repository: MobileRechargeRepository,
        compositeDisposable: CompositeDisposable
    ): OrderPulsa = OrderPulsa(repository, compositeDisposable)

    @Provides
    fun provideMobileRechargePresenter(
        context: Context,
        getRecentNumbers: GetRecentNumbers,
        getProviderCatalog: GetProviderCatalog,
        orderPulsa: OrderPulsa
    ): MobileRechargePresenterContract<MobileRechargeContract> =
        MobileRechargePresenter(
            context,
            getRecentNumbers,
            getProviderCatalog,
            orderPulsa
        )
}