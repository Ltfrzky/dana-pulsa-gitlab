package id.dana.apprentech.danapulsa.ui.promotion.detail

import android.content.Context
import android.util.Log
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.interactor.GetVoucherDetail
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import java.lang.Exception
import javax.inject.Inject

class PromotionDetailPresenter<V : PromotionDetailContract> @Inject constructor(
    private val context: Context,
    private val getVoucherDetail: GetVoucherDetail
) : BasePresenter<V>(), PromotionDetailPresenterContract<V> {
    override fun getPromotionDetail(voucherId: Int) {
        if (isOnline(context)) {
            getVoucherDetail.executeSingle(VoucherDetailSubscriber(), GetVoucherDetail.Request(voucherId.toString()))
        } else {
            getView()?.onGetPromotionDetailFailed(noNetworkConnectivityError())
        }
    }

    inner class VoucherDetailSubscriber<T: DataResult<BaseResponse<Voucher>>>
        :BaseSingleSubscriber<T>(){
        override fun onStart() {
            super.onStart()
            getView()?.onLoadingPromotion()
        }
        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when(response){
                is DataResult.Success<*> -> {
                    getView()?.onGetPromotionDetailSuccess(response.successData as BaseResponse<Voucher>)
                }
                is DataResult.Error -> {
                    getView()?.onGetPromotionDetailFailed(response)
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.onGetPromotionDetailFailed(DataResult.Error(Exception(e?.message)))
        }
    }


}