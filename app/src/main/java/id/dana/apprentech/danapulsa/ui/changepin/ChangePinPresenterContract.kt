package id.dana.apprentech.danapulsa.ui.changepin

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.base.BasePresenterContract

interface ChangePinPresenterContract<V: ChangePinContract> : BasePresenterContract<V>{
    fun postNewPin(newPin: Int)
}