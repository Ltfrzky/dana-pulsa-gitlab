package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.register.RegisterRepositoryImpl
import id.dana.apprentech.danapulsa.data.register.network.RegisterApi
import id.dana.apprentech.danapulsa.domain.register.interactor.RegisterInteractor
import id.dana.apprentech.danapulsa.domain.register.repository.RegisterRepository
import id.dana.apprentech.danapulsa.ui.register.RegisterContract
import id.dana.apprentech.danapulsa.ui.register.RegisterPresenter
import id.dana.apprentech.danapulsa.ui.register.RegisterPresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RegisterModule {

    @Provides
    @Singleton
    fun provideRegisterApi(retrofit: Retrofit): RegisterApi =
        retrofit.create(RegisterApi::class.java)

    @Provides
    @Singleton
    fun provideRegisterRepository(registerApi: RegisterApi): RegisterRepository =
        RegisterRepositoryImpl(registerApi)

    @Provides
    @Singleton
    fun provideRegisterInteractor(
        repository: RegisterRepository,
        compositeDisposable: CompositeDisposable
    ): RegisterInteractor = RegisterInteractor(repository, compositeDisposable)

    @Provides
    @Singleton
    fun provideRegisterPresenter(
        context: Context,
        getRegisterInteractor: RegisterInteractor
    ): RegisterPresenterContract<RegisterContract> =
        RegisterPresenter(
            context,
            getRegisterInteractor
        )
}