package id.dana.apprentech.danapulsa.util.asset

/**
 *created by Lutfi Rizky Ramadhan on 18/06/20
 */

enum class TransactionStatus(val type: String) {
    Completed("COMPLETED"),
    Failed("FAILED"),
    Canceled("CANCELED"),
    Expired("EXPIRED"),
    Waiting("WAITING"),
    Verifying("VERIFYING")
}