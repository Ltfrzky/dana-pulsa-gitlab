package id.dana.apprentech.danapulsa.ui.sample

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.sample.model.UserSample
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

interface SampleContract : BaseContract {
    fun loginSuccess(response : BaseResponse<UserSample>)

    fun loginFailed(response: DataResult.Error)

    fun loginLoading()

    fun getUserDataSuccess(user: UserSample)
}