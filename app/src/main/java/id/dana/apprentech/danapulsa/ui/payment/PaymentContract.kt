package id.dana.apprentech.danapulsa.ui.payment

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Order
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Payment
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

interface PaymentContract : BaseContract {
    fun showLoading()

    fun finishLoading()

    fun getOrderDetailSuccess(result: BaseResponse<Order>)

    fun getOrderDetailFailed(result: DataResult.Error)

    fun getVoucherRecommendationSuccess(result: BaseResponse<List<Voucher>>)

    fun getVoucherRecommendationFailed(result: DataResult.Error)

    fun payOrderLoading()

    fun payOrderFinish()

    fun payOrderSuccess(result: BaseResponse<Payment>)

    fun payOrderFailed(result: DataResult.Error)

    fun cancelOrderSuccess(result: BaseResponse<Transaction>)

    fun cancelOrderFailed(result: DataResult.Error)
}