package id.dana.apprentech.danapulsa.ui.payment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.custom.DialogAsk
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Order
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Payment
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.main.MainActivity
import id.dana.apprentech.danapulsa.ui.pickvoucher.PickVoucherActivity
import id.dana.apprentech.danapulsa.ui.transactionresult.TransactionResultActivity
import id.dana.apprentech.danapulsa.util.AppConstants.CHANGE_VOUCHER
import id.dana.apprentech.danapulsa.util.BundleKeys
import id.dana.apprentech.danapulsa.util.CommonUtils
import id.dana.apprentech.danapulsa.util.MessageUtils
import id.dana.apprentech.danapulsa.util.asset.PaymentMethod
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.base_toolbar.*
import javax.inject.Inject

class PaymentActivity : BaseActivity(), PaymentContract {
    companion object {
        @JvmStatic
        fun start(context: Context, transactionId: String) {
            val starter = Intent(context, PaymentActivity::class.java)
            starter.putExtra(BundleKeys.KEY_TRANSACTION_ID, transactionId)
            context.startActivity(starter)
        }
    }

    @Inject
    lateinit var presenter: PaymentPresenterContract<PaymentContract>

    @Inject
    lateinit var preferencesManager: PreferencesManager //TODO GET USER BALANCE WITH PRESENTER

    private lateinit var transactionId: String

    private val methodId = PaymentMethod.Wallet.id

    private var voucher: Voucher? = null

    private var isLoading = false

    private var price = 0

    private var total = 0

    private var discountValue = 0

    private var vouchers = mutableListOf<Voucher>()

    override fun getLayoutResource(): Int = R.layout.activity_payment

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupIntent() {
        transactionId = intent.getStringExtra(BundleKeys.KEY_TRANSACTION_ID) ?: "646"
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, getString(R.string.activity_payment), true)
    }

    override fun setupAction() {
        btnVoucherAction.setOnClickListener {
            startActivityForResult(PickVoucherActivity.start(this, vouchers), CHANGE_VOUCHER)
        }

        btnPay.setOnClickListener {
            val voucherId = voucher?.voucherId?.toString() ?: "0"
            presenter.payOrder(transactionId, methodId, voucherId)
        }
    }

    override fun setupProcess() {
        presenter.getOrderDetail(transactionId)
        presenter.getVoucherRecommendation(transactionId)
    }

    override fun showLoading() {
        isLoading = true
        layoutPayment.visibility = View.GONE
        pbPaymentLayout.visibility = View.VISIBLE
    }

    override fun finishLoading() {
        isLoading = false
        layoutPayment.visibility = View.VISIBLE
        pbPaymentLayout.visibility = View.GONE
    }

    override fun getOrderDetailSuccess(result: BaseResponse<Order>) {
        price = result.data.catalog.price

        Glide.with(this).load(result.data.catalog.provider.image).into(ivCheckoutProvider)
        tvCheckoutProduct.text = getString(
            R.string.payment_product,
            result.data.catalog.provider.name,
            CommonUtils.formatNumber(result.data.catalog.value),
            result.data.phoneNumber
        )

        tvCheckoutMethod.text = getString(
            R.string.payment_method_wallet,
            CommonUtils.formatCurrency(preferencesManager.getUserBalance())
        )
        tvTransactionTotalValue.text = CommonUtils.formatCurrency(price)
        btnPay.text =
            getString(R.string.btn_pay, CommonUtils.formatCurrency(price))
        setVoucher(null)
    }

    override fun getOrderDetailFailed(result: DataResult.Error) {
        MessageUtils.showAlertDialog(
            supportFragmentManager, getString(R.string.dialog_error_title), getString(
                R.string.get_order_detail_error
            )
        )
    }

    override fun getVoucherRecommendationSuccess(result: BaseResponse<List<Voucher>>) {
        if (result.data.isNotEmpty()) {
            vouchers.addAll(result.data)
            setVoucher(vouchers[0])
        } else {
            setVoucher(null)
        }
    }

    override fun getVoucherRecommendationFailed(result: DataResult.Error) {
        setVoucher(null)
        MessageUtils.showMessage(this, result.message)
    }

    override fun payOrderLoading() {
        isLoading = true
        pbPayButton.visibility = View.VISIBLE
        btnPay.isEnabled = false
        btnPay.text = null
    }

    override fun payOrderFinish() {
        isLoading = false
        pbPayButton.visibility = View.GONE
        btnPay.isEnabled = true
        btnPay.text = getString(R.string.btn_pay, CommonUtils.formatCurrency(total))
    }

    override fun payOrderSuccess(result: BaseResponse<Payment>) {
        TransactionResultActivity.start(this, result.data)
        finish()
    }

    override fun payOrderFailed(result: DataResult.Error) {
        MessageUtils.showAlertDialog(
            supportFragmentManager,
            getString(R.string.dialog_error_title),
            result.message
        )
    }

    override fun cancelOrderSuccess(result: BaseResponse<Transaction>) {
        TransactionResultActivity.start(
            this,
            Payment(preferencesManager.getUserBalance(), false, result.data)
        )
        finish()
    }

    override fun cancelOrderFailed(result: DataResult.Error) {
        MessageUtils.showAlertDialog(
            supportFragmentManager,
            getString(R.string.dialog_error_title),
            result.message
        )
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_payment, menu)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                MessageUtils.showAskDialog(
                    supportFragmentManager,
                    getString(R.string.dialog_transaction_unfinished_title),
                    getString(R.string.dialog_transaction_unfinished_content),
                    object : DialogAsk.DialogListener {
                        override fun onPositiveAskDialog() {
                            MainActivity.start(this@PaymentActivity)
                            finish()
                        }
                    }
                )
            }

            R.id.menuCancel -> {
                MessageUtils.showAskDialog(
                    supportFragmentManager,
                    getString(R.string.dialog_cancel_transaction_title),
                    getString(R.string.dialog_cancel_transaction_content),
                    object : DialogAsk.DialogListener {
                        override fun onPositiveAskDialog() {
                            presenter.cancelOrder(transactionId)
                        }
                    }
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CHANGE_VOUCHER && resultCode == Activity.RESULT_OK) {
            setVoucher(data?.getParcelableExtra(BundleKeys.KEY_PICKED_VOUCHER))
        }
    }

    private fun setVoucher(voucher: Voucher?) {
        if (vouchers.isNotEmpty()) {
            tvCheckoutVoucherLabel.visibility = View.VISIBLE
            cardVoucher.visibility = View.VISIBLE

            if (voucher != null) {
                this.voucher = voucher
                tvVoucherTitle.text = voucher.name
                btnVoucherAction.text = getString(R.string.button_payment_voucher_change)

                tvVoucherDetail.text = if (voucher.discount > 0) {
                    getString(
                        R.string.item_voucher_max_deduction,
                        CommonUtils.formatCurrency(voucher.maxDeduction)
                    )
                } else {
                    getString(
                        R.string.item_voucher_cashback_amount,
                        CommonUtils.formatCurrency(voucher.value)
                    )
                }


            } else {
                this.voucher = null
                tvVoucherTitle.text =
                    getString(R.string.payment_eligible_vouchers, vouchers.count().toString())
                btnVoucherAction.text = getString(R.string.button_payment_voucher_use_now)
            }
        } else {
            this.voucher = null
            tvCheckoutVoucherLabel.visibility = View.GONE
            cardVoucher.visibility = View.GONE
        }
        calculateTotal(price, voucher)
    }

    private fun calculateTotal(price: Int, voucher: Voucher?) {
        if (voucher != null) {
            discountValue = price * (voucher.discount / 100)
            total = if (discountValue >= voucher.maxDeduction) {
                price - voucher.maxDeduction
            } else {
                price - discountValue
            }

            if (total == price) {
                showCostTotalOnly()
            } else {
                showCostDetail()
            }
        } else {
            total = price
            showCostTotalOnly()
        }

    }

    private fun showCostTotalOnly() {
        tvTransactionPriceLabel.visibility = View.GONE
        tvTransactionPriceValue.visibility = View.GONE
        tvTransactionPromoLabel.visibility = View.GONE
        tvTransactionPromoValue.visibility = View.GONE
        dividerCost.visibility = View.GONE

        tvTransactionTotalValue.text = CommonUtils.formatCurrency(total)
    }

    private fun showCostDetail() {
        tvTransactionPriceLabel.visibility = View.VISIBLE
        tvTransactionPriceValue.visibility = View.VISIBLE
        tvTransactionPromoLabel.visibility = View.VISIBLE
        tvTransactionPromoValue.visibility = View.VISIBLE
        dividerCost.visibility = View.VISIBLE

        tvTransactionPriceValue.text = CommonUtils.formatCurrency(price)
        tvTransactionPromoValue.text = CommonUtils.formatCurrency(discountValue)
        tvTransactionTotalValue.text = CommonUtils.formatCurrency(total)
    }

}