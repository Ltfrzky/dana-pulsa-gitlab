package id.dana.apprentech.danapulsa.di

import android.content.Context
import id.dana.apprentech.danapulsa.BaseApplication
import id.dana.apprentech.danapulsa.di.component.ApplicationComponent
import id.dana.apprentech.danapulsa.di.component.DaggerApplicationComponent
import id.dana.apprentech.danapulsa.di.module.ApplicationModule

/**
 *created by Lutfi Rizky Ramadhan on 06/06/20
 */

object Injector {
    fun obtain(context: Context): ApplicationComponent? {
        return BaseApplication[context].injector
    }

    internal fun create(app: BaseApplication): ApplicationComponent {
        return DaggerApplicationComponent.builder().applicationModule(
            ApplicationModule(
                app
            )
        ).build()
    }
}