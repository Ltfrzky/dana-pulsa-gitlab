package id.dana.apprentech.danapulsa.util

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

object PatternRegex {
    const val PHONE_CATALOG_PATTERN = "^[08]\\d{3,}\$"
    const val PHONE_PATTERN = "^[08]\\d{9,12}\$"
    const val NAME_PATTERN =  "^[a-zA-Z ]{3,20}"
    const val EMAIL_PATTERN = "^[\\w-.]+@([\\w-]+\\.)+[\\w-]{2,4}\$"
}