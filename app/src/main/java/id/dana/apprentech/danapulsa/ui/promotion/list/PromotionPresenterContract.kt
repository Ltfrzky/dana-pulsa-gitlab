package id.dana.apprentech.danapulsa.ui.promotion.list

import id.dana.apprentech.danapulsa.base.BasePresenterContract

interface PromotionPresenterContract<V: PromotionContract>: BasePresenterContract<V> {
    fun getPromotions(page: String)
}