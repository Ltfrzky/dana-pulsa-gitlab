package id.dana.apprentech.danapulsa.util

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

fun String.capitalizeEachWords(): String =
    split(" ").joinToString(" ") { it.toLowerCase().capitalize() }