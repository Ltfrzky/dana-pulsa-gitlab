package id.dana.apprentech.danapulsa.ui.verifypin

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface VerifyPinContract : BaseContract {
    fun verifyPinSuccess(result: BaseResponse<Unit>)

    fun verifyPinFailed(result: DataResult.Error)

    fun verifyPinLoading()

    fun verifyPinFinished()
}