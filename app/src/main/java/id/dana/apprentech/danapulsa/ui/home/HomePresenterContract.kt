package id.dana.apprentech.danapulsa.ui.home

import id.dana.apprentech.danapulsa.base.BasePresenterContract

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface HomePresenterContract<V: HomeContract> : BasePresenterContract<V> {
    fun getUserBalance()
}