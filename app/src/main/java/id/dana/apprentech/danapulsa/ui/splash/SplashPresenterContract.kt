package id.dana.apprentech.danapulsa.ui.splash

import id.dana.apprentech.danapulsa.base.BasePresenterContract

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface SplashPresenterContract<V: SplashContract> : BasePresenterContract<V> {
    fun getUserProfileLocal()

    fun checkSession()
}