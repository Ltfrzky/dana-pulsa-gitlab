package id.dana.apprentech.danapulsa.ui.register

import android.content.Intent
import android.util.Log
import android.view.View
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.custom.DialogAsk
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.login.LoginActivity
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_EMAIL
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_NAME
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_PHONE
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_PIN
import id.dana.apprentech.danapulsa.util.MessageUtils
import kotlinx.android.synthetic.main.activity_register_confirm_pin.*
import kotlinx.android.synthetic.main.activity_register_pin.btnRegisterNext
import kotlinx.android.synthetic.main.base_toolbar.*
import javax.inject.Inject

class RegisterConfirmPinActivity : BaseActivity(), RegisterContract {

    override fun getLayoutResource(): Int = R.layout.activity_register_confirm_pin

    @Inject
    lateinit var presenter: RegisterPresenterContract<RegisterContract>
    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupUI() {
        setNextButtonAvailability(false)
        setupToolbar(baseToolbar, getString(R.string.sign_up), true)
    }

    override fun setupAction() {
        var currentPin: String? = null
        if (pinEntryConfirmRegister != null) {
            pinEntryConfirmRegister.setOnPinEnteredListener {
                if (it.toString().length == 6) {
                    currentPin = it.toString()
                    setNextButtonAvailability(true)
                    hideKeyboard()
                } else {
                    currentPin = null
                    setNextButtonAvailability(false)
                }
            }
        } else {
            setNextButtonAvailability(false)
        }


        btnRegisterNext.setOnClickListener {
            val bundle = intent.extras
            var prevPin: String? = null
            var fullname: String? = null
            var email: String? = null
            var phone: String? = null
            val registerComplete = Intent(this, RegisterConfirmPinActivity::class.java)
            if (bundle != null) {
                fullname = bundle.getString(KEY_NAME)
                email = bundle.getString(KEY_EMAIL)
                phone = bundle.getString(KEY_PHONE)
                prevPin = bundle.getString(KEY_PIN)
                Log.d("State", "all data: $fullname, $email, $phone, $prevPin")
            }
            if (currentPin == prevPin) {
                registerComplete.putExtra("Pin", currentPin)
                if (fullname != null && email != null && phone != null && prevPin != null)
                    presenter.postRegister(fullname, email, phone, prevPin.toInt())
            } else {
                pinEntryConfirmRegister.text = null
                MessageUtils.showAlertDialog(
                    supportFragmentManager,
                    getString(R.string.sign_up),
                    getString(R.string.pin_not_match)
                )
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun setupProcess() {
//        TODO("Not yet implemented")
    }

    override fun setNextButtonAvailability(available: Boolean) {
        btnRegisterNext.isEnabled = available
    }

    override fun onRegisterSuccess(response: NonBodyResponse) {
        pbRegister.visibility = View.GONE
        MessageUtils.showMessage(this, "User Successfully Created")
        val nextActivity = Intent(this, LoginActivity::class.java)
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(nextActivity)
        finish()
    }

    override fun onRegisterFailed(response: DataResult.Error) {
        pbRegister.visibility = View.GONE
        if (response.code == 409) {
            MessageUtils.showAskDialog(
                supportFragmentManager,
                getString(R.string.whoops),
                response.message,
                getString(R.string.dialog_ask_go_to_login),
                getString(R.string.dialog_ask_dismiss),
                object : DialogAsk.DialogListener{
                    override fun onPositiveAskDialog() {
                        goToLogin()
                    }
                }
            )
        } else {
            MessageUtils.showAlertDialog(supportFragmentManager, getString(R.string.dialog_error_title), response.message)
        }

    }

    override fun registerLoading() {
        pbRegister.visibility = View.VISIBLE
    }

    override fun invalidInput(message: String) {
    }

    private fun goToLogin() {
        val nextActivity = Intent(this, LoginActivity::class.java)
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(nextActivity)
        finish()
    }
}