package id.dana.apprentech.danapulsa.base.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.lang.Exception

/**
 *created by Lutfi Rizky Ramadhan on 08/06/20
 */

abstract class BaseRecyclerAdapter<Data, Holder : BaseViewHolder<Data>>(
    protected val context: Context,
    var data: MutableList<Data>
) : RecyclerView.Adapter<Holder>() {

    protected var itemClickListener: OnItemClickListener? = null

    protected var longItemClickListener: OnLongItemClickListener? = null

    protected fun getView(parent: ViewGroup, viewType: Int): View {
        return LayoutInflater.from(context).inflate(getItemResourceLayout(viewType), parent, false)
    }

    protected abstract fun getItemResourceLayout(viewType: Int): Int

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return try {
            data.size
        } catch (e: Exception) {
            0
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun setOnItemClickListener(itemClickListener: OnItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    fun setOnLongItemClickListener(longItemClickListener: OnLongItemClickListener) {
        this.longItemClickListener = longItemClickListener
    }

    fun addItem(item: Data) {
        data.add(item)
        notifyItemInserted(data.size - 1)
    }

    fun addItem(item: Data, position: Int) {
        data.add(position, item)
        notifyItemInserted(position)
    }

    fun addItem(items: List<Data>) {
        items.forEach { data.add(it) }
        notifyDataSetChanged()
    }

    fun addOrUpdateItem(item: Data) {
        val itemIndex = data.indexOf(item)
        if (itemIndex >= 0) {
            data[itemIndex] = item
            notifyItemChanged(itemIndex)
        } else {
            addItem(item)
        }
    }

    fun addOrUpdateItem(items: List<Data>) {
        items.forEach {
            val itemIndex = data.indexOf(it)
            if (itemIndex >= 0) {
                data[itemIndex] = it
            } else {
                addItem(it)
            }
        }

        notifyDataSetChanged()
    }

    fun addOrUpdateItemToFirst(items: List<Data>) {
        items.forEach {
            val itemIndex = data.indexOf(it)
            if (itemIndex >= 0) {
                data[itemIndex] = it
            } else {
                addItem(it, 0)
            }
        }

        notifyDataSetChanged()
    }

    fun addItemToFirst(item: Data) {
        data.add(0, item)
        notifyDataSetChanged()
    }

    fun addItemToFirst(items: List<Data>) {
        data.addAll(0, items)
        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        if (position >= 0 && position < data.size) {
            data.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun removeItem(item: Data) {
        removeItem(data.indexOf(item))
    }

    fun clearData() {
        data.clear()
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    interface OnLongItemClickListener {
        fun onLongItemClick(view: View?, position: Int)
    }
}