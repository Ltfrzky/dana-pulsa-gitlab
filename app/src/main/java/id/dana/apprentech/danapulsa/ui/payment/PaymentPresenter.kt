package id.dana.apprentech.danapulsa.ui.payment

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.BaseSubscriber
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.CancelOrder
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.GetOrderDetail
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.GetVoucherRecommendation
import id.dana.apprentech.danapulsa.domain.mobilerecharge.interactor.PayOrder
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Order
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Payment
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import id.dana.apprentech.danapulsa.domain.util.DataResult
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

class PaymentPresenter<V : PaymentContract> @Inject constructor(
    private val context: Context,
    private val getOrderDetail: GetOrderDetail,
    private val getVoucherRecommendation: GetVoucherRecommendation,
    private val payOrder: PayOrder,
    private val cancelOrder: CancelOrder
) : BasePresenter<V>(), PaymentPresenterContract<V> {

    override fun getOrderDetail(transactionId: String) {
        if (isOnline(context)) {
            getOrderDetail.executeSingle(
                GetOrderDetailSubscriber(),
                GetOrderDetail.Request(transactionId)
            )
        } else {
            getView()?.getOrderDetailFailed(noNetworkConnectivityError())
        }
    }

    override fun getVoucherRecommendation(transactionId: String) {
        if (isOnline(context)) {
            getVoucherRecommendation.executeFlowable(
                GetVoucherRecommendationSubscriber(),
                GetVoucherRecommendation.Request(transactionId)
            )
        } else {
            getView()?.getVoucherRecommendationFailed(noNetworkConnectivityError())
        }
    }

    override fun payOrder(transactionId: String, methodId: String, voucherId: String) {
        if (isOnline(context)) {
            payOrder.executeSingle(
                PayOrderSubscriber(),
                PayOrder.Request(transactionId, methodId, voucherId)
            )
        } else {
            getView()?.payOrderFailed(noNetworkConnectivityError())
        }
    }

    override fun cancelOrder(transactionId: String) {
        if (isOnline(context)) {
            cancelOrder.executeSingle(CancelOrderSubscriber(), CancelOrder.Request(transactionId))
        } else {
            getView()?.cancelOrderFailed(noNetworkConnectivityError())
        }
    }

    inner class GetOrderDetailSubscriber<T : DataResult<BaseResponse<Order>>> :
        BaseSingleSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.showLoading()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when(response){
                is DataResult.Success<*> ->{
                    getView()?.getOrderDetailSuccess(response.successData as BaseResponse<Order>)
                }

                is DataResult.Error -> {
                    getView()?.getOrderDetailFailed(response)
                }
            }
            getView()?.finishLoading()
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getOrderDetailFailed(noNetworkConnectivityError())
            getView()?.finishLoading()
        }
    }

    inner class GetVoucherRecommendationSubscriber<T : DataResult<BaseResponse<List<Voucher>>>> :
        BaseSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.showLoading()
        }

        override fun onNext(response: T) {
            super.onNext(response)
            when(response){
                is DataResult.Success<*> ->{
                    getView()?.getVoucherRecommendationSuccess(response.successData as BaseResponse<List<Voucher>>)
                }

                is DataResult.Error -> {
                    getView()?.getOrderDetailFailed(response)
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getVoucherRecommendationFailed(noNetworkConnectivityError())
            getView()?.finishLoading()
        }

        override fun onComplete() {
            super.onComplete()
            getView()?.finishLoading()
        }
    }

    inner class PayOrderSubscriber<T : DataResult<BaseResponse<Payment>>> :
        BaseSingleSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.payOrderLoading()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when(response){
                is DataResult.Success<*> ->{
                    getView()?.payOrderSuccess(response.successData as BaseResponse<Payment>)
                }

                is DataResult.Error -> {
                    getView()?.payOrderFailed(response)
                }
            }
            getView()?.payOrderFinish()
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.payOrderFailed(noNetworkConnectivityError())
            getView()?.payOrderFinish()
        }
    }

    inner class CancelOrderSubscriber<T : DataResult<BaseResponse<Transaction>>> :
        BaseSingleSubscriber<T>() {

        override fun onStart() {
            super.onStart()
            getView()?.showLoading()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when(response){
                is DataResult.Success<*> ->{
                    getView()?.cancelOrderSuccess(response.successData as BaseResponse<Transaction>)
                }

                is DataResult.Error -> {
                    getView()?.cancelOrderFailed(response)
                }
            }
            getView()?.finishLoading()
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.cancelOrderFailed(noNetworkConnectivityError())
            getView()?.finishLoading()
        }
    }
}