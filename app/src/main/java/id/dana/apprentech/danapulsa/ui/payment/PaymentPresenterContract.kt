package id.dana.apprentech.danapulsa.ui.payment

import id.dana.apprentech.danapulsa.base.BasePresenterContract

/**
 *created by Lutfi Rizky Ramadhan on 12/06/20
 */

interface PaymentPresenterContract<V : PaymentContract> : BasePresenterContract<V> {
    fun getOrderDetail(transactionId: String)

    fun getVoucherRecommendation(transactionId: String)

    fun payOrder(transactionId: String, methodId: String, voucherId: String)

    fun cancelOrder(transactionId: String)
}