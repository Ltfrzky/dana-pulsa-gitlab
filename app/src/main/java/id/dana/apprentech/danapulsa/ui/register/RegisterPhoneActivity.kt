package id.dana.apprentech.danapulsa.ui.register

import android.content.Intent
import androidx.core.widget.addTextChangedListener
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_EMAIL
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_NAME
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_PHONE
import id.dana.apprentech.danapulsa.util.PatternRegex
import kotlinx.android.synthetic.main.activity_register_phone.*
import kotlinx.android.synthetic.main.base_toolbar.*
import java.util.regex.Pattern

class RegisterPhoneActivity : BaseActivity(), RegisterContract {

    override fun getLayoutResource(): Int = R.layout.activity_register_phone

    override fun setupLib() {
//        TODO("Not yet implemented")
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, getString(R.string.sign_up), true)
        setNextButtonAvailability(false)
    }

    override fun setupAction() {
        edtRegisterPhone.addTextChangedListener {
            when {
                it.isNullOrEmpty() -> {
                    setNextButtonAvailability(false)
                    layoutEdtRegisterPhone.error = null
                }
                Pattern.matches(PatternRegex.PHONE_PATTERN, it) -> {
                    setNextButtonAvailability(true)
                    layoutEdtRegisterPhone.error = null
                }
                else -> {
                    setNextButtonAvailability(false)
                    invalidInput(getString(R.string.invalid_register_phone))
                }
            }
        }

        btnRegisterNext.setOnClickListener {
            val extras = intent.extras
            var fullname: String? = null
            var email: String? = null
            if (extras != null) {
                fullname = extras.getString(KEY_NAME)
                email = extras.getString(KEY_EMAIL)
            }
            val registerPin = Intent(this, RegisterPinActivity::class.java)
            registerPin.putExtra(KEY_NAME, fullname)
            registerPin.putExtra(KEY_EMAIL, email)
            registerPin.putExtra(KEY_PHONE, edtRegisterPhone.text.toString())
            startActivity(registerPin)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun setupProcess() {
//        TODO("Not yet implemented")
    }

    override fun setNextButtonAvailability(available: Boolean) {
        btnRegisterNext.isEnabled = available
    }

    override fun onRegisterSuccess(response: NonBodyResponse) {
//        TODO("Not yet implemented")
    }

    override fun onRegisterFailed(response: DataResult.Error) {
//        TODO("Not yet implemented")
    }

    override fun registerLoading() {
//        TODO("Not yet implemented")
    }

    override fun invalidInput(message: String) {
        layoutEdtRegisterPhone.error = message
    }
}