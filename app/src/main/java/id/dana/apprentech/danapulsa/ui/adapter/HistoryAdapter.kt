package id.dana.apprentech.danapulsa.ui.adapter

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StrikethroughSpan
import android.view.View
import android.view.ViewGroup
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.adapter.BaseRecyclerAdapter
import id.dana.apprentech.danapulsa.base.adapter.BaseViewHolder
import id.dana.apprentech.danapulsa.domain.history.model.History
import id.dana.apprentech.danapulsa.util.CommonUtils
import id.dana.apprentech.danapulsa.util.asset.TransactionStatus
import id.dana.apprentech.danapulsa.util.capitalizeEachWords
import kotlinx.android.synthetic.main.item_history.view.*

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

class HistoryAdapter(context: Context, data: MutableList<History>) :
    BaseRecyclerAdapter<History, HistoryAdapter.HistoryViewHolder>(context, data) {

    override fun getItemResourceLayout(viewType: Int): Int = R.layout.item_history

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        return HistoryViewHolder(
            context,
            getView(parent, viewType),
            itemClickListener,
            longItemClickListener
        )
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.bind(data[position])
    }

    inner class HistoryViewHolder(
        context: Context,
        itemView: View,
        itemClickListener: OnItemClickListener?,
        itemLongItemClickListener: OnLongItemClickListener?
    ) : BaseViewHolder<History>(context, itemView, itemClickListener, itemLongItemClickListener) {

        override fun bind(data: History) {
            itemView.apply {
                val price = SpannableString(CommonUtils.formatCurrency(data.price))
                val voucher = SpannableString(CommonUtils.formatCurrency(data.price - data.voucher))
                price.setSpan(
                    StrikethroughSpan(),
                    0,
                    price.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                voucher.setSpan(
                    ForegroundColorSpan(context.resources.getColor(R.color.colorComplementary)),
                    0,
                    voucher.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                val discountedPrice = SpannableStringBuilder().append(price, " ",  voucher)
                tvHistoryItemProduct.text = context.getString(
                    R.string.item_history_product,
                    CommonUtils.formatPhoneNumber(data.phoneNumber)
                )

                tvHistoryItemPrice.text = if (data.voucher > 0) {
                    discountedPrice
                } else {
                    CommonUtils.formatCurrency(data.price)
                }

                tvHistoryItemDate.text = CommonUtils.getHumanReadableDateTime(data.createdAt)
                tvHistoryItemStatus.text = data.status.capitalizeEachWords()

                when (data.status) {
                    TransactionStatus.Completed.type -> tvHistoryItemStatus
                        .setTextColor(context.resources.getColor(R.color.colorSuccess))

                    TransactionStatus.Canceled.type, TransactionStatus.Failed.type -> tvHistoryItemStatus
                        .setTextColor(context.resources.getColor(R.color.colorFailed))

                    else -> tvHistoryItemStatus
                        .setTextColor(context.resources.getColor(R.color.colorTextPrimary))
                }
            }
        }
    }
}