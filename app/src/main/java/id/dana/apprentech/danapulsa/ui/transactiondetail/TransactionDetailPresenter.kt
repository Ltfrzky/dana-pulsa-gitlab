package id.dana.apprentech.danapulsa.ui.transactiondetail

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.handleSubscriberError
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.history.interactor.GetHistoryDetail
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

class TransactionDetailPresenter<V : TransactionDetailContract>(
    private val context: Context,
    private val getHistoryDetail: GetHistoryDetail
) : BasePresenter<V>(), TransactionDetailPresenterContract<V> {
    override fun getTransactionDetail(transactionId: String) {
        if (isOnline(context)) {
            getHistoryDetail.executeSingle(GetTransactionDetailSubscriber(), GetHistoryDetail.Request(transactionId))
        } else {
            getView()?.getTransactionFailed(noNetworkConnectivityError())
        }
    }

    inner class GetTransactionDetailSubscriber<T : DataResult<BaseResponse<Transaction>>> :
        BaseSingleSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.getTransactionLoading()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when(response){
                is DataResult.Success<*> ->{
                    getView()?.getTransactionSuccess(response.successData as BaseResponse<Transaction>)
                }

                is DataResult.Error -> {
                    getView()?.getTransactionFailed(response)
                }
            }
            getView()?.getTransactionFinished()
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getTransactionFailed(noNetworkConnectivityError())
            getView()?.getTransactionFinished()
        }
    }
}