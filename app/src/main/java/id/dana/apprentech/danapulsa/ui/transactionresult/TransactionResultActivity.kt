package id.dana.apprentech.danapulsa.ui.transactionresult

import android.content.Context
import android.content.Intent
import com.bumptech.glide.Glide
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.CatalogOrder
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Payment
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Provider
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.ui.main.MainActivity
import id.dana.apprentech.danapulsa.util.BundleKeys
import id.dana.apprentech.danapulsa.util.CommonUtils
import id.dana.apprentech.danapulsa.util.MessageUtils
import id.dana.apprentech.danapulsa.util.asset.TransactionStatus
import id.dana.apprentech.danapulsa.util.capitalizeEachWords
import kotlinx.android.synthetic.main.activity_transaction_result.*

class TransactionResultActivity : BaseActivity() {

    companion object {
        @JvmStatic
        fun start(context: Context, payment: Payment) {
            val starter = Intent(context, TransactionResultActivity::class.java)
            starter.putExtra(BundleKeys.KEY_PAYMENT, payment)
            context.startActivity(starter)
        }
    }

    lateinit var payment: Payment

    override fun getLayoutResource(): Int = R.layout.activity_transaction_result

    override fun setupLib() {}

    override fun setupIntent() {
        payment = intent.getParcelableExtra(BundleKeys.KEY_PAYMENT)!!
    }

    override fun setupUI() {
        decideResult(payment)
    }

    override fun setupAction() {
        btnBackToHome.setOnClickListener {
            MainActivity.start(this)
            finish()
        }
    }

    override fun setupProcess() {}

    private fun decideResult(payment: Payment) {
        val transaction = payment.transaction

        if (payment.rewardVoucher) {
            MessageUtils.showAlertDialog(
                supportFragmentManager, getString(R.string.dialog_get_voucher_title), getString(
                    R.string.dialog_get_voucher_content
                )
            )
        }

        tvTransactionResult.text =
            getString(R.string.result_payment_any, transaction.status.capitalizeEachWords())
        cardTransactionInformation.apply {
            setIdText(
                getString(
                    R.string.transaction_info_id,
                    transaction.transactionId.toString()
                )
            )
            setProductText(
                getString(
                    R.string.transaction_info_product,
                    transaction.catalog.provider.name,
                    CommonUtils.formatNumber(transaction.catalog.value)
                )
            )
            setPhoneText(CommonUtils.formatPhoneNumber(transaction.phoneNumber))
            setDateText(CommonUtils.getHumanReadableDateTime(transaction.createdAt))
            setStatusText(transaction.status.capitalizeEachWords())
        }

        when (transaction.status) {
            TransactionStatus.Completed.type -> {
                layoutTransactionResult.setBackgroundResource(R.color.colorPrimary)
                Glide.with(this).load(resources.getDrawable(R.drawable.ic_check))
                    .into(ivTransactionIcon)
                cardTransactionInformation.setStatusTextColor(resources.getColor(R.color.colorSuccess))
            }

            TransactionStatus.Verifying.type -> {
                layoutTransactionResult.setBackgroundResource(R.color.colorTextSecondary)
                Glide.with(this).load(resources.getDrawable(R.drawable.ic_check))
                    .into(ivTransactionIcon)
                tvTransactionResult.text = getString(R.string.transaction_result_payment_verifying)
                cardTransactionInformation.setStatusTextColor(resources.getColor(R.color.colorTextSecondary))
                btnBackToHome.setTextColor(resources.getColor(R.color.colorTextSecondary))
            }

            else -> {
                layoutTransactionResult.setBackgroundResource(R.color.colorFailed)
                Glide.with(this).load(resources.getDrawable(R.drawable.ic_failed))
                    .into(ivTransactionIcon)
                cardTransactionInformation.setStatusTextColor(resources.getColor(R.color.colorFailed))
                btnBackToHome.setTextColor(resources.getColor(R.color.colorFailed))
            }
        }
    }
}