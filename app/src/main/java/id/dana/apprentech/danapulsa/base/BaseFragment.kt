package id.dana.apprentech.danapulsa.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import id.dana.apprentech.danapulsa.custom.DialogAlert
import id.dana.apprentech.danapulsa.custom.DialogAsk

/**
 *created by Lutfi Rizky Ramadhan on 03/06/20
 */

abstract class BaseFragment : Fragment(), BaseContract {
    private var baseActivity: BaseActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayoutResource(), container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupLib()
        setupIntent(savedInstanceState)
        setupUI()
        setupAction()
        setupProcess()
    }

    override fun setupToolbar(toolbar: Toolbar, isBack: Boolean) {
        baseActivity?.setupToolbar(toolbar, isBack)
    }

    override fun setupToolbar(toolbar: Toolbar, title: String, isBack: Boolean) {
        baseActivity?.setupToolbar(toolbar, title, isBack)
    }

    override fun hideKeyboard() {
        baseActivity?.hideKeyboard()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) {
            val activity = context as BaseActivity?
            this.baseActivity = activity
        }
    }

    protected abstract fun getLayoutResource(): Int

    protected abstract fun setupLib()

    protected open fun setupIntent(savedInstanceState: Bundle?){}

    protected abstract fun setupUI()

    protected abstract fun setupAction()

    protected abstract fun setupProcess()
}