package id.dana.apprentech.danapulsa.ui.promotion.detail

import android.view.View
import com.bumptech.glide.Glide
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.custom.RequirementView
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Voucher
import id.dana.apprentech.danapulsa.ui.mobilerecharge.MobileRechargeActivity
import id.dana.apprentech.danapulsa.util.BundleKeys
import id.dana.apprentech.danapulsa.util.CommonUtils
import id.dana.apprentech.danapulsa.util.MessageUtils
import kotlinx.android.synthetic.main.activity_promotion_detail.*
import kotlinx.android.synthetic.main.base_toolbar.*
import kotlinx.android.synthetic.main.progress_view.*
import javax.inject.Inject

class PromotionDetailActivity : BaseActivity(), PromotionDetailContract {

    @Inject
    lateinit var presenter: PromotionDetailPresenterContract<PromotionDetailContract>
    private var voucherId: Int? = null

    override fun getLayoutResource() = R.layout.activity_promotion_detail

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupIntent() {
        val extras = intent.extras
        voucherId = extras?.getInt(BundleKeys.KEY_VOUCHER_ID)
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, "Promotion Detail", true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun setupAction() {
        btnBuyPulsa.setOnClickListener {
            MobileRechargeActivity.start(this)
            finish()
        }
    }

    override fun setupProcess() {
        presenter.getPromotionDetail(voucherId!!)
    }

    override fun onLoadingPromotion() {
        progressLayout.visibility = View.VISIBLE
        btnBuyPulsa.isEnabled = false
    }

    override fun onGetPromotionDetailSuccess(response: BaseResponse<Voucher>) {
        progressLayout.visibility = View.GONE
        btnBuyPulsa.isEnabled = true
        setDetail(response.data)
    }

    private fun setDetail(data: Voucher){
        Glide.with(this).load(data.filePath).into(ivPromoImage)
        tvPromoTitle.text = data.name
        viewPromoMinimumPurchase.setValueText(CommonUtils.formatCurrency(data.minPurchase))
        viewPromoMaximumBenefit.setValueText(CommonUtils.formatCurrency(data.value))
        viewPromoPeriod.setValueText(CommonUtils.getHumanReadableDateTime(data.expiryDate))
        viewPromoPaymentMethod.setValueText(data.paymentMethod)
    }

    override fun onGetPromotionDetailFailed(response: DataResult.Error) {
        progressLayout.visibility = View.GONE
        MessageUtils.showAlertDialog(supportFragmentManager, getString(R.string.whoops), response.message)
    }
}