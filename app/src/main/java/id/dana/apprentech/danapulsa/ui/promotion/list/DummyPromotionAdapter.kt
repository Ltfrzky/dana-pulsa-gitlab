package id.dana.apprentech.danapulsa.ui.promotion.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.domain.sample.entity.PromotionSample

class DummyPromotionAdapter(
    private val promotionSamples: List<PromotionSample>,
    private val listener: (PromotionSample) -> Unit
) : RecyclerView.Adapter<DummyPromotionAdapter.PromotionHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PromotionHolder {
        return PromotionHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_promotion, parent, false)
        )
    }

    override fun getItemCount(): Int{
        return 4
    }

    override fun onBindViewHolder(holder: PromotionHolder, position: Int) {
        holder.bind(promotionSamples.get(0), listener)
    }

    inner class PromotionHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(promotionSample: PromotionSample, listener: (PromotionSample) -> Unit) {
//            Glide.with(view.context).load(promotion.imageLink).into(view.ivPromotionItemImage)


//            view.tvPromotionItemTitle.text = promotion.promotionTitle
//            view.tvPromotionItemValid.text = promotion.promotionInfo
//            view.tvPromotionType.text = promotion.promotionType

            view.setOnClickListener {
                listener(promotionSample)
            }
        }
    }

}