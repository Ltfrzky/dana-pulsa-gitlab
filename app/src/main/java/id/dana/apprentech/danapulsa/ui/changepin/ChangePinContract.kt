package id.dana.apprentech.danapulsa.ui.changepin

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult

interface ChangePinContract: BaseContract {
    fun onChangePinStart()

    fun onChangePinSuccess(response: DataResult<NonBodyResponse>)

    fun onChangePinFailed(result: DataResult.Error)
}