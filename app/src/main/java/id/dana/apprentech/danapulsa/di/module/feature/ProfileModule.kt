package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.domain.profile.interactors.DeleteUserSession
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfile
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfileLocal
import id.dana.apprentech.danapulsa.domain.profile.repository.ProfileRepository
import id.dana.apprentech.danapulsa.ui.profile.ProfileContract
import id.dana.apprentech.danapulsa.ui.profile.ProfilePresenter
import id.dana.apprentech.danapulsa.ui.profile.ProfilePresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable
import javax.inject.Singleton

@Module
class ProfileModule {

    @Provides
    @Singleton
    fun provideDeleteUserSession(
        repository: ProfileRepository,
        compositeDisposable: CompositeDisposable
    ): DeleteUserSession = DeleteUserSession(repository, compositeDisposable)

    @Provides
    @Singleton
    fun provideProfilePresenter(
        context: Context,
        getUserProfile: GetUserProfile,
        getUserProfileLocal: GetUserProfileLocal,
        deleteUserSession: DeleteUserSession
    ): ProfilePresenterContract<ProfileContract> = ProfilePresenter(context, getUserProfile, getUserProfileLocal, deleteUserSession)
}