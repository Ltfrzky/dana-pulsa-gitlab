package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.history.HistoryRepositoryImpl
import id.dana.apprentech.danapulsa.data.history.network.HistoryApi
import id.dana.apprentech.danapulsa.domain.history.interactor.GetHistoryCompleted
import id.dana.apprentech.danapulsa.domain.history.interactor.GetHistoryDetail
import id.dana.apprentech.danapulsa.domain.history.interactor.GetHistoryInProgress
import id.dana.apprentech.danapulsa.domain.history.repository.HistoryRepository
import id.dana.apprentech.danapulsa.ui.history.HistoryContract
import id.dana.apprentech.danapulsa.ui.history.HistoryPresenter
import id.dana.apprentech.danapulsa.ui.history.HistoryPresenterContract
import id.dana.apprentech.danapulsa.ui.transactiondetail.TransactionDetailContract
import id.dana.apprentech.danapulsa.ui.transactiondetail.TransactionDetailPresenter
import id.dana.apprentech.danapulsa.ui.transactiondetail.TransactionDetailPresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */
@Module
class HistoryModule {

    @Provides
    @Singleton
    fun provideHistoryApi(retrofit: Retrofit): HistoryApi = retrofit.create(HistoryApi::class.java)

    @Provides
    fun provideHistoryRepository(historyApi: HistoryApi): HistoryRepository =
        HistoryRepositoryImpl(historyApi)

    @Provides
    fun provideGetHistoryInProgress(
        repository: HistoryRepository,
        compositeDisposable: CompositeDisposable
    ): GetHistoryInProgress = GetHistoryInProgress(repository, compositeDisposable)

    @Provides
    fun provideGetHistoryCompleted(
        repository: HistoryRepository,
        compositeDisposable: CompositeDisposable
    ): GetHistoryCompleted = GetHistoryCompleted(repository, compositeDisposable)

    @Provides
    fun provideGetHistoryDetail(
        repository: HistoryRepository,
        compositeDisposable: CompositeDisposable
    ): GetHistoryDetail = GetHistoryDetail(repository, compositeDisposable)

    @Provides
    fun provideHistoryPresenter(
        context: Context,
        getHistoryInProgress: GetHistoryInProgress,
        getHistoryCompleted: GetHistoryCompleted
    ): HistoryPresenterContract<HistoryContract> =
        HistoryPresenter(context, getHistoryInProgress, getHistoryCompleted)

    @Provides
    fun provideHistoryDetailPresenter(
        context: Context,
        getHistoryDetail: GetHistoryDetail
    ): TransactionDetailPresenterContract<TransactionDetailContract> =
        TransactionDetailPresenter(context, getHistoryDetail)
}