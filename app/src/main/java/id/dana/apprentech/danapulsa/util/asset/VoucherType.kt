package id.dana.apprentech.danapulsa.util.asset

/**
 *created by Lutfi Rizky Ramadhan on 17/06/20
 */

enum class VoucherType(val type: String) {
    Discount("discount"),
    Cashback("cashback")
}