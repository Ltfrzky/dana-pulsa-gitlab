package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import id.dana.apprentech.danapulsa.domain.sample.repository.SampleRepository
import id.dana.apprentech.danapulsa.data.sample.SampleRepositoryImpl
import id.dana.apprentech.danapulsa.data.sample.network.SampleApi
import id.dana.apprentech.danapulsa.ui.sample.SamplePresenter
import id.dana.apprentech.danapulsa.ui.sample.SamplePresenterContract
import id.dana.apprentech.danapulsa.ui.sample.SampleContract
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.domain.sample.interactor.LoginInteractor
import id.dana.apprentech.danapulsa.domain.sample.interactor.UserDataInteractor
import io.reactivex.rxjava3.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 *created by Lutfi Rizky Ramadhan on 05/06/20
 */

@Module
class SampleModule {

    @Provides
    @Singleton
    fun provideSampleApi(retrofit: Retrofit): SampleApi =
        retrofit.create(SampleApi::class.java)

    @Provides
    fun provideSampleRepository(
        sampleApi: SampleApi,
        preferencesManager: PreferencesManager
    ): SampleRepository =
        SampleRepositoryImpl(sampleApi, preferencesManager)

    @Provides
    fun provideLoginInteractor(
        sampleRepository: SampleRepository,
        compositeDisposable: CompositeDisposable
    ): LoginInteractor = LoginInteractor(sampleRepository, compositeDisposable)

    @Provides
    fun provideUserDataInteractor(
        sampleRepository: SampleRepository,
        compositeDisposable: CompositeDisposable
    ): UserDataInteractor = UserDataInteractor(sampleRepository, compositeDisposable)

    @Provides
    fun provideSamplePresenter(
        context: Context,
        loginInteractor: LoginInteractor,
        userDataInteractor: UserDataInteractor
    ): SamplePresenterContract<SampleContract> {
        return SamplePresenter(
            context,
            loginInteractor,
            userDataInteractor
        )
    }
}