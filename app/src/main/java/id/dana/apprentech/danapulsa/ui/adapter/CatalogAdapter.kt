package id.dana.apprentech.danapulsa.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.adapter.BaseRecyclerAdapter
import id.dana.apprentech.danapulsa.base.adapter.BaseViewHolder
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Catalog
import id.dana.apprentech.danapulsa.util.CommonUtils
import kotlinx.android.synthetic.main.item_catalog_pulsa.view.*

/**
 *created by Lutfi Rizky Ramadhan on 15/06/20
 */

class CatalogAdapter(context: Context, data: MutableList<Catalog>) :
    BaseRecyclerAdapter<Catalog, CatalogAdapter.CatalogViewHolder>(context, data) {

    override fun getItemResourceLayout(viewType: Int): Int = R.layout.item_catalog_pulsa

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatalogViewHolder {
        return CatalogViewHolder(
            context,
            getView(parent, viewType), itemClickListener, longItemClickListener
        )
    }

    override fun onBindViewHolder(holder: CatalogViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.bind(data[position])
    }

    class CatalogViewHolder(
        context: Context,
        itemView: View,
        itemClickListener: OnItemClickListener?,
        longItemClickListener: OnLongItemClickListener?
    ) : BaseViewHolder<Catalog>(context, itemView, itemClickListener, longItemClickListener) {
        override fun bind(data: Catalog) {
            itemView.tvPulsaValue.text = CommonUtils.formatNumber(data.value)
            itemView.tvPulsaPrice.text = CommonUtils.formatCurrency(data.price)
        }
    }
}