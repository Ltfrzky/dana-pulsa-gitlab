package id.dana.apprentech.danapulsa.di.module.feature

import android.content.Context
import dagger.Module
import dagger.Provides
import id.dana.apprentech.danapulsa.data.preference.PreferencesManager
import id.dana.apprentech.danapulsa.data.profile.ProfileRepositoryImpl
import id.dana.apprentech.danapulsa.data.profile.network.ProfileApi
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserBalance
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfile
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfileLocal
import id.dana.apprentech.danapulsa.domain.profile.repository.ProfileRepository
import id.dana.apprentech.danapulsa.ui.splash.SplashContract
import id.dana.apprentech.danapulsa.ui.splash.SplashPresenter
import id.dana.apprentech.danapulsa.ui.splash.SplashPresenterContract
import io.reactivex.rxjava3.disposables.CompositeDisposable
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */
@Module
class SplashModule {

    @Provides
    @Singleton
    fun provideProfileApi(retrofit: Retrofit): ProfileApi = retrofit.create(ProfileApi::class.java)

    @Provides
    fun provideProfileRepository(
        profileApi: ProfileApi,
        preferencesManager: PreferencesManager
    ): ProfileRepository = ProfileRepositoryImpl(profileApi, preferencesManager)

    @Provides
    fun provideGetUserProfileInteractor(
        repository: ProfileRepository,
        compositeDisposable: CompositeDisposable
    ): GetUserProfile = GetUserProfile(repository, compositeDisposable)

    @Provides
    fun provideGetUserProfileLocalInteractor(
        repository: ProfileRepository,
        compositeDisposable: CompositeDisposable
    ): GetUserProfileLocal = GetUserProfileLocal(repository, compositeDisposable)

    @Provides
    fun provideSplashPresenter(
        context: Context,
        getUserProfile: GetUserProfile,
        getUserProfileLocal: GetUserProfileLocal
    ): SplashPresenterContract<SplashContract> = SplashPresenter(context, getUserProfile, getUserProfileLocal)
}