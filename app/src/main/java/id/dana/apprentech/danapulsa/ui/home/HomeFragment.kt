package id.dana.apprentech.danapulsa.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseFragment
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.ui.promotion.list.PromotionActivity
import id.dana.apprentech.danapulsa.ui.promotion.list.DummyPromotionAdapter
import id.dana.apprentech.danapulsa.ui.promotion.detail.PromotionDetailActivity
import id.dana.apprentech.danapulsa.domain.sample.entity.PromotionSample
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.mobilerecharge.MobileRechargeActivity
import id.dana.apprentech.danapulsa.util.CommonUtils
import id.dana.apprentech.danapulsa.util.MessageUtils
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : BaseFragment(), HomeContract {

    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()

            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }

    }

    @Inject
    lateinit var presenter: HomePresenterContract<HomeContract>

    private lateinit var adapterDummy: DummyPromotionAdapter

    private var promotions = mutableListOf(PromotionSample())

    override fun getLayoutResource(): Int = R.layout.fragment_home

    override fun setupLib() {
        Injector.obtain(requireContext())?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupIntent(savedInstanceState: Bundle?) {
//        TODO("Not yet implemented")
    }

    override fun setupUI() {

        adapterDummy =
            DummyPromotionAdapter(
                promotions
            ) {
                startActivity(
                    Intent(
                        activity,
                        PromotionDetailActivity::class.java
                    ).putExtra("id", it.id)
                )
            }

        rvPromotion.apply {
            adapter = this@HomeFragment.adapterDummy
            layoutManager = GridLayoutManager(activity, 2)
        }
    }

    override fun setupAction() {
        btnPromotionViewAll.setOnClickListener {
            startActivity(Intent(activity, PromotionActivity::class.java))
        }
        btnMobileRecharge.setOnClickListener {
            MobileRechargeActivity.start(requireActivity())
            requireActivity().finish()
        }

        srlHome.setOnRefreshListener {
            presenter.getUserBalance()
        }
    }

    override fun setupProcess() {
        presenter.getUserBalance()
    }

    override fun getBalanceSuccess(result: BaseResponse<Int>) {
        tvBalance?.text = CommonUtils.formatCurrency(result.data)
    }

    override fun getBalanceFailed(result: DataResult.Error) {
        MessageUtils.showMessage(requireContext(), "Balance Load Failed")
    }

    override fun getBalanceLoading() {
        pbUserBalance.visibility = View.VISIBLE
        tvBalance.text = null
    }

    override fun getBalanceFinished() {
        pbUserBalance?.visibility = View.GONE
        srlHome?.isRefreshing = false
    }

}