package id.dana.apprentech.danapulsa.ui.login

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.BaseSubscriber
import id.dana.apprentech.danapulsa.domain.login.interactors.Login
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult
import javax.inject.Inject

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

class LoginPresenter<V : LoginContract> @Inject constructor(
    private val context: Context,
    private val loginInteractor: Login
) : BasePresenter<V>(), LoginPresenterContract<V> {

    override fun login(username: String) {
        if (isOnline(context)) {
            loginInteractor.executeSingle(LoginSubscriber(), Login.Request(username))
        } else {
            getView()?.loginFailed(noNetworkConnectivityError())
        }
    }

    inner class LoginSubscriber<T : DataResult<BaseResponse<UserProfile>>> :
        BaseSingleSubscriber<T>() {
        override fun onStart() {
            super.onStart()
            getView()?.loginLoading()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.loginSuccess(response.successData as BaseResponse<UserProfile>)
                    getView()?.loginFinished()
                }
                is DataResult.Error -> {
                    getView()?.loginFailed(response)
                }
            }
        }

//        override fun onNext(response: T) {
//            super.onNext(response)
//            when (response) {
//                is DataResult.Success<*> -> {
//                    getView()?.loginSuccess(response.successData as BaseResponse<UserProfile>)
//                }
//
//                is DataResult.Error -> {
//                    getView()?.loginFailed(response)
//                }
//            }
//        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.loginFailed(noNetworkConnectivityError())
            getView()?.loginFinished()
        }

//        override fun onComplete() {
//            super.onComplete()
//            getView()?.loginFinished()
//        }
    }
}