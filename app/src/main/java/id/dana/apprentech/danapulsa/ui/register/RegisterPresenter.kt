package id.dana.apprentech.danapulsa.ui.register

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.data.util.ext.noNetworkConnectivityError
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.register.interactor.RegisterInteractor
import id.dana.apprentech.danapulsa.domain.util.DataResult
import javax.inject.Inject

class RegisterPresenter<V : RegisterContract> @Inject constructor(
    private val context: Context,
    private val registerInteractor: RegisterInteractor
) : BasePresenter<V>(), RegisterPresenterContract<V> {

    override fun postRegister(name: String, email: String, phone: String, pin: Int) {
        if (isOnline(context)) {
            registerInteractor.executeSingle(
                RegisterSubscriber(),
                RegisterInteractor.Request(name, email, phone, pin)
            )
        } else {
            getView()?.onRegisterFailed(noNetworkConnectivityError())
        }
    }

    inner class RegisterSubscriber<T : DataResult<NonBodyResponse>>
        : BaseSingleSubscriber<T>() {

        override fun onStart() {
            super.onStart()
            getView()?.registerLoading()
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.onRegisterSuccess(response.successData as NonBodyResponse)
                }
                is DataResult.Error -> {
                    getView()?.onRegisterFailed(response)

                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.onRegisterFailed(DataResult.Error(Exception(e?.message)))
        }
    }

}