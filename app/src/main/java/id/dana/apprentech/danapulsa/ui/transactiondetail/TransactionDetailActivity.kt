package id.dana.apprentech.danapulsa.ui.transactiondetail

import android.content.Context
import android.content.Intent
import android.view.MenuItem
import android.view.View
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.custom.DialogAlert
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.CatalogOrder
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Provider
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.VoucherTransaction
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.util.BundleKeys
import id.dana.apprentech.danapulsa.util.CommonUtils
import id.dana.apprentech.danapulsa.util.MessageUtils
import id.dana.apprentech.danapulsa.util.asset.TransactionStatus
import id.dana.apprentech.danapulsa.util.capitalizeEachWords
import kotlinx.android.synthetic.main.activity_transaction_detail.*
import kotlinx.android.synthetic.main.base_toolbar.*
import javax.inject.Inject

class TransactionDetailActivity : BaseActivity(), TransactionDetailContract {

    companion object {
        @JvmStatic
        fun start(context: Context, transactionId: String) {
            val starter = Intent(context, TransactionDetailActivity::class.java)
            starter.putExtra(BundleKeys.KEY_TRANSACTION_ID, transactionId)
            context.startActivity(starter)
        }
    }

    @Inject
    lateinit var presenter: TransactionDetailPresenterContract<TransactionDetailContract>

    private lateinit var transactionId: String

    override fun getLayoutResource(): Int = R.layout.activity_transaction_detail

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupIntent() {
        transactionId = intent.getStringExtra(BundleKeys.KEY_TRANSACTION_ID) ?: ""
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, getString(R.string.activity_transaction_detail), true)
    }

    override fun setupAction() {
        cardTransactionUsedVoucher.setOnClickListener {
            MessageUtils.showMessage(this, "Go to voucher detail")
        }
    }

    override fun setupProcess() {
        presenter.getTransactionDetail(transactionId)
    }

    override fun getTransactionLoading() {
        layoutTransactionDetail.visibility = View.GONE
        pbTransactionDetail.visibility = View.VISIBLE
    }

    override fun getTransactionFinished() {
        layoutTransactionDetail.visibility = View.VISIBLE
        pbTransactionDetail.visibility = View.GONE
    }

    override fun getTransactionSuccess(result: BaseResponse<Transaction>) {
        val transaction = result.data

        cardHistoryTransactionInformation.apply {
            setIdText(
                getString(
                    R.string.transaction_info_id,
                    transaction.transactionId.toString()
                )
            )
            setProductText(
                getString(
                    R.string.transaction_info_product,
                    transaction.catalog.provider.name,
                    CommonUtils.formatNumber(transaction.catalog.value)
                )
            )
            setPhoneText(CommonUtils.formatPhoneNumber(transaction.phoneNumber))
            setDateText(CommonUtils.getHumanReadableDateTime(transaction.createdAt))
            setStatusText(transaction.status.capitalizeEachWords())
            setStatusTextColor(
                when (transaction.status) {
                    TransactionStatus.Completed.type -> resources.getColor(R.color.colorSuccess)
                    else -> resources.getColor(R.color.colorFailed)
                }
            )
        }

        if (transaction.voucher == null) {
            tvTransactionVoucherLabel.visibility = View.GONE
            cardTransactionUsedVoucher.visibility = View.GONE
            tvTransactionTotalValue.text = CommonUtils.formatCurrency(transaction.catalog.price)
        } else {
            tvTransactionVoucherName.text = transaction.voucher?.name
            if (transaction.voucher?.deduction!! > 0) {
                tvTransactionVoucherInfo.text = getString(
                    R.string.item_voucher_max_deduction,
                    CommonUtils.formatCurrency(transaction.voucher?.maxDeduction!!)
                )
                tvTransactionPriceLabel.visibility = View.VISIBLE
                tvTransactionPriceValue.visibility = View.VISIBLE
                tvTransactionPromoLabel.visibility = View.VISIBLE
                tvTransactionPromoValue.visibility = View.VISIBLE
                dividerCost.visibility = View.VISIBLE
                tvTransactionPriceValue.text = CommonUtils.formatCurrency(transaction.catalog.price)
                tvTransactionPromoValue.text =
                    CommonUtils.formatCurrency(transaction.voucher?.deduction!!)
                tvTransactionTotalValue.text =
                    CommonUtils.formatCurrency(transaction.catalog.price - transaction.voucher?.deduction!!)
            } else {
                tvTransactionVoucherInfo.text = getString(
                    R.string.item_voucher_cashback_amount,
                    CommonUtils.formatCurrency(transaction.voucher?.maxDeduction!!)
                )
                tvTransactionTotalValue.text = CommonUtils.formatCurrency(transaction.catalog.price)
            }
        }
    }

    override fun getTransactionFailed(result: DataResult.Error) {
        MessageUtils.showAlertDialog(
            supportFragmentManager,
            getString(R.string.dialog_error_title),
            result.message,
            object : DialogAlert.DialogListener {
                override fun onPositiveButtonClick() {
                    finish()
                }
            })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}