package id.dana.apprentech.danapulsa.ui.profile

import android.content.Intent
import android.view.View
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseFragment
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.otp.OtpActivity
import id.dana.apprentech.danapulsa.ui.login.LoginActivity
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_CHANGE_PIN
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_PHONE
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_USER_ID
import id.dana.apprentech.danapulsa.util.CommonUtils
import id.dana.apprentech.danapulsa.util.MessageUtils
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.progress_view.*
import javax.inject.Inject


class ProfileFragment : BaseFragment(), ProfileContract {

    override fun getLayoutResource(): Int = R.layout.fragment_profile

    private var userId: Int? = null
    private var userName: String? = null
    private var userPhone: String? = null
    private var userEmail: String? = null

    @Inject
    lateinit var presenter: ProfilePresenterContract<ProfileContract>
    override fun setupLib() {
        Injector.obtain(requireContext())?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupUI() {
        presenter.getLocalProfile()
        progressLayout.visibility = View.GONE
    }

    override fun setupAction() {
        btnUserSignOut.setOnClickListener {
            progressLayout.visibility = View.VISIBLE
            presenter.logOut()
        }

        btnUserChangePin.setOnClickListener {
            val intent = Intent(requireContext(), OtpActivity::class.java)
            intent.putExtra(KEY_CHANGE_PIN, true)
            intent.putExtra(KEY_USER_ID, userId)
            intent.putExtra(KEY_PHONE, userPhone)
            startActivity(intent)
        }
    }

    override fun setupProcess() {
        presenter.getProfile()
    }

    private fun setProfile(data: UserProfile){
        userName = data.name?.let { CommonUtils.capitalizeName(it) }
        userId = data.id
        userPhone = CommonUtils.formatDisplayPhone(data.phone)
        userEmail = data.email

        tvUserName.text = userName
        tvUserPhone.text = userPhone
        tvUserEmail.text = userEmail
    }

    override fun getProfileSuccess(response: BaseResponse<UserProfile>) {
        setProfile(response.data)
    }

    override fun getProfileSuccess(response: UserProfile) {
        setProfile(response)
    }

    override fun getProfileFailed(response: DataResult.Error) {
        presenter.getLocalProfile()
    }

    override fun getProfileLoading() {
    }

    override fun onFailedLogout(response: DataResult.Error) {
        MessageUtils.showMessage(requireContext(), response.message)
        progressLayout.visibility = View.GONE
    }

    override fun onLogout() {
        val next = Intent(requireContext(), LoginActivity::class.java)
        next.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(next)
        progressLayout.visibility = View.GONE
        requireActivity().finish()
    }

    override fun changePin() {
    }
}