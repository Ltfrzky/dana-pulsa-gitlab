package id.dana.apprentech.danapulsa.ui.splash

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 16/06/20
 */

interface SplashContract : BaseContract {
    fun getUserProfile(result: UserProfile)

    fun checkSessionSuccess(result: BaseResponse<UserProfile>)

    fun checkSessionFailed(result: DataResult.Error)
}