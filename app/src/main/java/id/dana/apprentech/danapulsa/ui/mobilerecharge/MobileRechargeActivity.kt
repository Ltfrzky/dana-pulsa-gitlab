package id.dana.apprentech.danapulsa.ui.mobilerecharge

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.provider.ContactsContract
import android.view.MenuItem
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.base.adapter.BaseRecyclerAdapter
import id.dana.apprentech.danapulsa.custom.DialogAsk
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Order
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.ProviderCatalog
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.RecentNumber
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.main.MainActivity
import id.dana.apprentech.danapulsa.ui.adapter.CatalogAdapter
import id.dana.apprentech.danapulsa.ui.adapter.RecentNumberAdapter
import id.dana.apprentech.danapulsa.ui.payment.PaymentActivity
import id.dana.apprentech.danapulsa.util.AppConstants.PICK_CONTACT_RESULT
import id.dana.apprentech.danapulsa.util.MessageUtils
import id.dana.apprentech.danapulsa.util.PatternRegex
import kotlinx.android.synthetic.main.activity_mobile_recharge.*
import kotlinx.android.synthetic.main.base_toolbar.*
import java.util.regex.Pattern
import javax.inject.Inject

class MobileRechargeActivity : BaseActivity(), MobileRechargeContract {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, MobileRechargeActivity::class.java)
            context.startActivity(starter)
        }
    }

    @Inject
    lateinit var presenter: MobileRechargePresenterContract<MobileRechargeContract>

    lateinit var recentNumberAdapter: RecentNumberAdapter

    lateinit var catalogAdapter: CatalogAdapter

    var isLoading = false

    override fun getLayoutResource(): Int = R.layout.activity_mobile_recharge

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, getString(R.string.activity_mobile_recharge), true)

        recentNumberAdapter = RecentNumberAdapter(this, ArrayList())
        rvRecentNumber.layoutManager = LinearLayoutManager(this)
        rvRecentNumber.setHasFixedSize(true)
        rvRecentNumber.adapter = recentNumberAdapter

        catalogAdapter = CatalogAdapter(this, ArrayList())
        rvPulsaCatalog.layoutManager = GridLayoutManager(this, 2)
        rvPulsaCatalog.setHasFixedSize(true)
        rvPulsaCatalog.adapter = catalogAdapter
    }

    override fun setupAction() {
        edtPhoneNumber.addTextChangedListener {
            var errorMessage: String?
            when {
                it.isNullOrEmpty() -> errorMessage = getString(R.string.error_mobile_recharge_phone)
                it.length < 5 -> {
                    errorMessage = null
                    showRecentNumber()
                }
                Pattern.matches(PatternRegex.PHONE_CATALOG_PATTERN, it) -> {
                    errorMessage = null
                    if (!isLoading && !layoutPulsaCatalog.isVisible) presenter.getProviderCatalog(
                        it.toString().substring(0..4)
                    )
                }
                else -> errorMessage = getString(R.string.error_mobile_recharge_phone)
            }
            tilPhoneNumber.error = errorMessage
            tilPhoneNumber.requestFocus()
        }

        tilPhoneNumber.setEndIconOnClickListener {
            edtPhoneNumber.text = null
            tilPhoneNumber.error = null
            showRecentNumber()
        }

        recentNumberAdapter.setOnItemClickListener(object :
            BaseRecyclerAdapter.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                pickPhoneNumber(recentNumberAdapter.data[position].number)
            }
        })

        catalogAdapter.setOnItemClickListener(object : BaseRecyclerAdapter.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                if (tilPhoneNumber.error == null && Pattern.matches(
                        PatternRegex.PHONE_PATTERN, edtPhoneNumber.text.toString()
                    )
                ) {
                    presenter.orderPulsa(
                        tilPhoneNumber.editText?.text.toString(),
                        catalogAdapter.data[position].catalogId.toString()
                    )
                } else {
                    tilPhoneNumber.error = getString(R.string.error_mobile_recharge_phone)
                }
            }
        })

        btnSelectContact.setOnClickListener {
            chooseFromContact()
        }
    }

    override fun setupProcess() {
        presenter.getRecentNumbers()
    }

    override fun getRecentNumberSuccess(result: BaseResponse<List<RecentNumber>>) {
        recentNumberAdapter.addOrUpdateItem(result.data)
    }

    override fun getRecentNumberFailed(result: DataResult.Error) {
        MessageUtils.showMessage(this, result.message)
    }

    override fun getRecentNumberFinished() {
        layoutRecentNumber.visibility = View.VISIBLE
        layoutPulsaCatalog.visibility = View.GONE
        pbMobileRecharge.visibility = View.GONE
        isLoading = false
    }

    override fun getCatalogSuccess(result: BaseResponse<ProviderCatalog>) {
        catalogAdapter.clearData()
        catalogAdapter.addOrUpdateItem(result.data.catalog)
        Glide.with(this).load(result.data.provider.image).into(ivProviderIcon)
        ivProviderIcon.visibility = View.VISIBLE
        isLoading = false
    }

    override fun getCatalogFailed(result: DataResult.Error) {
        catalogAdapter.clearData()
        if (result.code == 404) {
            tilPhoneNumber.error = getString(R.string.error_mobile_recharge_phone)
        }
    }

    override fun getCatalogFinished() {
        layoutRecentNumber.visibility = View.GONE
        layoutPulsaCatalog.visibility = View.VISIBLE
        pbMobileRecharge.visibility = View.GONE
        isLoading = false
    }

    override fun orderPulsaSuccess(result: BaseResponse<Order>) {
        PaymentActivity.start(this, result.data.orderId.toString())
        finish()
    }

    override fun orderPulsaFailed(result: DataResult.Error) {
        MessageUtils.showAlertDialog(
            supportFragmentManager,
            getString(R.string.dialog_error_title),
            result.message
        )
    }

    override fun orderPulsaFinished() {
        layoutRecentNumber.visibility = View.GONE
        layoutPulsaCatalog.visibility = View.VISIBLE
        pbMobileRecharge.visibility = View.GONE
        isLoading = false
    }

    override fun showLoading() {
        layoutRecentNumber.visibility = View.GONE
        layoutPulsaCatalog.visibility = View.GONE
        pbMobileRecharge.visibility = View.VISIBLE
        isLoading = true
    }

    private fun showRecentNumber() {
        layoutRecentNumber.visibility = View.VISIBLE
        layoutPulsaCatalog.visibility = View.GONE
        ivProviderIcon.visibility = View.GONE
    }

    private fun chooseFromContact() {
        val intent = Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
        startActivityForResult(intent, PICK_CONTACT_RESULT)
    }

    private fun pickPhoneNumber(phoneNumber: String) {
        isLoading = true
        edtPhoneNumber.setText(phoneNumber)
        presenter.getProviderCatalog(phoneNumber.substring(0..4))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                if (!isLoading) onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        MainActivity.start(this)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_CONTACT_RESULT && resultCode == Activity.RESULT_OK && data != null) {
            var cursor: Cursor? = null
            cursor = data.data?.let { contentResolver.query(it, null, null, null, null) }
            cursor?.moveToFirst()
            val selectedNumber =
                cursor?.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

            pickPhoneNumber(selectedNumber.toString())
        }
    }
}