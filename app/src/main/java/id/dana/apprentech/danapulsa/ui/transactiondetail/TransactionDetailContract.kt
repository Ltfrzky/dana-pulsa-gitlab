package id.dana.apprentech.danapulsa.ui.transactiondetail

import id.dana.apprentech.danapulsa.base.BaseContract
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.mobilerecharge.model.Transaction
import id.dana.apprentech.danapulsa.domain.util.DataResult

/**
 *created by Lutfi Rizky Ramadhan on 19/06/20
 */

interface TransactionDetailContract : BaseContract {
    fun getTransactionLoading()

    fun getTransactionFinished()

    fun getTransactionSuccess(result: BaseResponse<Transaction>)

    fun getTransactionFailed(result: DataResult.Error)
}