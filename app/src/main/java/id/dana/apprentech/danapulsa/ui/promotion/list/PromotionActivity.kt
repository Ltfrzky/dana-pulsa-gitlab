package id.dana.apprentech.danapulsa.ui.promotion.list

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.base.adapter.BaseRecyclerAdapter
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.sample.entity.PromotionSample
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.domain.voucher.model.Promotion
import id.dana.apprentech.danapulsa.ui.adapter.PromotionAdapter
import id.dana.apprentech.danapulsa.ui.promotion.detail.PromotionDetailActivity
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_VOUCHER_ID
import id.dana.apprentech.danapulsa.util.MessageUtils
import kotlinx.android.synthetic.main.activity_promotion.*
import kotlinx.android.synthetic.main.base_toolbar.*
import kotlinx.android.synthetic.main.progress_view.*
import javax.inject.Inject

class PromotionActivity : BaseActivity(),
    PromotionContract {

    private lateinit var adapterDummy: DummyPromotionAdapter
    private var promotions = mutableListOf(PromotionSample())

    @Inject
    lateinit var presenter: PromotionPresenterContract<PromotionContract>
    private lateinit var promotionAdapter: PromotionAdapter
    private var page = 1
    private var isLoadMore = false


    override fun getLayoutResource(): Int = R.layout.activity_promotion

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, "Promotion", true)

        promotionAdapter = PromotionAdapter(this, ArrayList())

        rvPromotion.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@PromotionActivity)
            adapter = promotionAdapter
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun setupAction() {
        promotionAdapter.setOnItemClickListener(object : BaseRecyclerAdapter.OnItemClickListener{
            override fun onItemClick(view: View?, position: Int) {
                val detail = Intent(this@PromotionActivity, PromotionDetailActivity::class.java)
                detail.putExtra(KEY_VOUCHER_ID, promotionAdapter.data[position].id)
                startActivity(detail)
            }
        })

        srlPromotion.setOnRefreshListener {
            page = 1
            isLoadMore = false
            promotionAdapter.clearData()
            presenter.getPromotions(page.toString())
        }
    }

    override fun setupProcess() {
        presenter.getPromotions(page.toString())
    }

    override fun onLoadPromotion() {
        progressLayout.visibility = View.VISIBLE
    }

    override fun getPromotionSuccess(response: BaseResponse<List<Promotion>>) {
        promotionAdapter.addItem(response.data)
    }

    override fun getPromotionFailed(response: DataResult.Error) {
        progressLayout.visibility = View.GONE
        srlPromotion.isRefreshing = false
        MessageUtils.showMessage(this, response.message)
    }

    override fun getPromotionFinished() {
        srlPromotion.isRefreshing = false
        progressLayout.visibility = View.GONE
    }

    private fun addDummy(){
        //        promotions.add(Promotion(1, "https://res.cloudinary.com/darwmcfjo/image/upload/v1591548482/WhatsApp_Image_2020-05-30_at_7.27.48_PM_nessez.jpg", "Cashback Pulsa", "Valid sampai 2021", "Cashback"))
//        promotions.add(Promotion(2, "https://res.cloudinary.com/darwmcfjo/image/upload/v1591548482/WhatsApp_Image_2020-05-30_at_7.27.48_PM_nessez.jpg", "Voucher Pulsa", "Valid sampai 2021", "Voucher"))

//        adapterDummy = DummyPromotionAdapter(promotions) {
//            startActivity(
//                Intent(
//                    this,
//                    PromotionDetailActivity::class.java
//                ).putExtra("id", it.id)
//            )
//        }
//
//        rvPromotion.apply {
//            adapter = this@PromotionActivity.adapterDummy
//            layoutManager = LinearLayoutManager(this@PromotionActivity)
//        }
    }
}