package id.dana.apprentech.danapulsa.ui.changepin

import android.content.Intent
import android.view.View
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.di.Injector
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.util.DataResult
import id.dana.apprentech.danapulsa.ui.login.LoginActivity
import id.dana.apprentech.danapulsa.ui.main.MainActivity
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_CHANGE_PIN
import id.dana.apprentech.danapulsa.util.BundleKeys.KEY_PIN
import id.dana.apprentech.danapulsa.util.MessageUtils.showAlertDialog
import kotlinx.android.synthetic.main.activity_confirm_new_pin.*
import kotlinx.android.synthetic.main.base_toolbar.*
import kotlinx.android.synthetic.main.progress_view.*
import javax.inject.Inject

class ConfirmNewPinActivity : BaseActivity(), ChangePinContract {

    override fun getLayoutResource(): Int = R.layout.activity_confirm_new_pin

    @Inject
    lateinit var presenter: ChangePinPresenterContract<ChangePinContract>

    private var newPin: String? = null
    private var changePinMode: Boolean = false

    override fun setupLib() {
        Injector.obtain(this)?.inject(this)
        presenter.onAttach(this, lifecycle)
    }

    override fun setupUI() {
        setupToolbar(baseToolbar, "Change PIN", true)
    }

    override fun setupAction() {
        var confirmPin: String? = null

        pinEntryConfirmPin.setOnPinEnteredListener {
            if (it.length == 6) {
                btnChangePinFinish.isEnabled = true
                confirmPin = it.toString()
                hideKeyboard()
            } else {
                confirmPin = null
                btnChangePinFinish.isEnabled = false
            }
        }

        btnChangePinFinish.setOnClickListener {
            if (confirmPin == newPin) {
                presenter.postNewPin(newPin!!.toInt())
            } else {
                showAlertDialog(
                    supportFragmentManager,
                    getString(R.string.whoops),
                    getString(R.string.pin_not_match)
                )
            }
        }
    }

    override fun setupIntent() {
        super.setupIntent()
        val extras = intent.extras
        if (extras != null) {
            newPin = extras.getString(KEY_PIN)
            changePinMode = extras.getBoolean(KEY_CHANGE_PIN)
        }
    }

    override fun setupProcess() {

    }

    override fun onChangePinStart() {
        progressLayout.visibility = View.VISIBLE
    }

    override fun onChangePinSuccess(response: DataResult<NonBodyResponse>) {
        progressLayout.visibility = View.GONE
        showAlertDialog(
            supportFragmentManager,
            getString(R.string.yeay),
            "Your pin successfully changed"
        )
        val nextActivity: Intent = if (changePinMode) {
            Intent(this, MainActivity::class.java)
        } else {
            Intent(this, LoginActivity::class.java)
        }
        nextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(nextActivity)
        finish()
    }

    override fun onChangePinFailed(result: DataResult.Error) {
        progressLayout.visibility = View.GONE
        showAlertDialog(supportFragmentManager, getString(R.string.whoops), result.message)
    }
}