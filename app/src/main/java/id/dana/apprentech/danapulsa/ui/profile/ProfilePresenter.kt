package id.dana.apprentech.danapulsa.ui.profile

import android.content.Context
import id.dana.apprentech.danapulsa.base.BasePresenter
import id.dana.apprentech.danapulsa.data.util.ext.isOnline
import id.dana.apprentech.danapulsa.domain.base.BaseResponse
import id.dana.apprentech.danapulsa.domain.base.BaseSingleSubscriber
import id.dana.apprentech.danapulsa.domain.base.Empty
import id.dana.apprentech.danapulsa.domain.base.NonBodyResponse
import id.dana.apprentech.danapulsa.domain.login.model.UserProfile
import id.dana.apprentech.danapulsa.domain.profile.interactors.DeleteUserSession
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfile
import id.dana.apprentech.danapulsa.domain.profile.interactors.GetUserProfileLocal
import id.dana.apprentech.danapulsa.domain.util.DataResult
import java.lang.Exception
import javax.inject.Inject

class ProfilePresenter<V : ProfileContract> @Inject constructor(
    private val context: Context,
    private val getUserProfile: GetUserProfile,
    private val getUserProfileLocal: GetUserProfileLocal,
    private val deleteUserSession: DeleteUserSession
) : BasePresenter<V>(), ProfilePresenterContract<V> {

    override fun getProfile() {
        if (isOnline(context)) {
            getUserProfile.executeSingle(ProfileSubscriber(), Empty())
        } else {
            getUserProfileLocal.executeSingle(ProfileLocalSubscriber(), Empty())
        }
    }

    override fun getLocalProfile() {
        getUserProfileLocal.executeSingle(ProfileLocalSubscriber(), Empty())
    }

    override fun logOut() {
        deleteUserSession.executeSingle(DeleteUserSubscriber(), Empty())
    }

    inner class DeleteUserSubscriber<T: DataResult<NonBodyResponse>>
        :BaseSingleSubscriber<T>(){

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when(response){
                is DataResult.Success<*> -> {
                    getView()?.onLogout()
                }
                is DataResult.Error -> {
                    getView()?.onFailedLogout(response)
                }
            }
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.onFailedLogout(DataResult.Error(Exception(e?.message)))
        }
    }

    inner class ProfileLocalSubscriber
        : BaseSingleSubscriber<UserProfile>() {
        override fun onStart() {
            super.onStart()
            getView()?.getProfileLoading()
        }

        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getProfileFailed(DataResult.Error(Exception(e?.message)))
        }

        override fun onSuccess(response: UserProfile) {
            super.onSuccess(response)
            getView()?.getProfileSuccess(response)
        }
    }

    inner class ProfileSubscriber<T : DataResult<BaseResponse<UserProfile>>>
        : BaseSingleSubscriber<T>() {
        override fun onError(e: Throwable?) {
            super.onError(e)
            getView()?.getProfileFailed(DataResult.Error(Exception(e?.message)))
        }

        override fun onSuccess(response: T) {
            super.onSuccess(response)
            when (response) {
                is DataResult.Success<*> -> {
                    getView()?.getProfileSuccess(response.successData as BaseResponse<UserProfile>)
                }
                is DataResult.Error -> {
                    getView()?.getProfileFailed(response)
                }
            }
        }

        override fun onStart() {
            super.onStart()
            getView()?.getProfileLoading()
        }
    }
}