package id.dana.apprentech.danapulsa.ui.promotion.detail

import id.dana.apprentech.danapulsa.base.BasePresenterContract

interface PromotionDetailPresenterContract<V: PromotionDetailContract>: BasePresenterContract<V> {
    fun getPromotionDetail(voucherId: Int)
}