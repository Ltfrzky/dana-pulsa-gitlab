package id.dana.apprentech.danapulsa.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import id.dana.apprentech.danapulsa.R
import id.dana.apprentech.danapulsa.base.BaseActivity
import id.dana.apprentech.danapulsa.custom.DialogAsk
import id.dana.apprentech.danapulsa.ui.mobilerecharge.MobileRechargeActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*

class MainActivity : BaseActivity(), DialogAsk.DialogListener {

    companion object{
        fun start(context: Context){
            val starter = Intent(context, MainActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun getLayoutResource(): Int = R.layout.activity_main

    override fun setupLib() {
//        TODO("Not yet implemented")
    }

    override fun setupIntent() {
//        TODO("Not yet implemented")
    }

    override fun setupUI() {

        val navController = findNavController(R.id.nhfMain)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.navigation_home,
            R.id.navigation_history,
            R.id.navigation_voucher,
            R.id.navigation_profile
        ))

//        setupActionBarWithNavController(navController, appBarConfiguration)
//        setSupportActionBar(baseToolbar)
        bnvMain.setupWithNavController(navController)
    }

    override fun setupAction() {
    }

    override fun setupProcess() {
//        TODO("Not yet implemented")
    }

    override fun onPositiveAskDialog() {
//        TODO("Not yet implemented")
    }
}