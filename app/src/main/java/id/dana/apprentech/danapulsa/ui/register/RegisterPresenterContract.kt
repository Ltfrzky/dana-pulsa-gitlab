package id.dana.apprentech.danapulsa.ui.register

import id.dana.apprentech.danapulsa.base.BasePresenterContract

interface RegisterPresenterContract<V: RegisterContract>: BasePresenterContract<V> {
    fun postRegister(name: String, email: String, phone: String, pin: Int)
}